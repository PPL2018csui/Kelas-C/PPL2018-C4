# *allocateam*'s Front-End Service
![coverage](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C4/badges/sit_uat/coverage.svg?job=test-peach)

This is the front-end service for allocateam, access on allocateam.com

## Getting Started

### Prerequisites
You need to install `node:8.9` first you can check the latest version [here](http://www.dropwizard.io/1.0.2/docs/) 

### Installing
Make sure the latest stable version of node and NPM already installed, then run this command:

```
npm install
npm run build
npm start
```

It will run the server on `http://localhost:3000 ` 

## Running the tests
To run all the test:
```
npm run test
```

To run on specific component/container:
```
npm test -- [ComponentName]
```

## Build With
Language: **JavaScript**

Framework: **ReactJS**

Architecture: **React Boilerplate**

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration

