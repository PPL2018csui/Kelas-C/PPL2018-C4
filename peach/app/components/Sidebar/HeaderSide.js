import styled from 'styled-components';

import theme from '../../theme';

const headerSide = styled.h1`
    font-size: 1.3em;
    color: ${theme.lightGreen};
    padding-top: 0.6em;
    font-family: ${theme.fontMenu};
`;

export default headerSide;
