import styled from 'styled-components';

import theme from '../../theme';

const A = styled.a`
  padding: 35px 8px 35px 6px;
  display: block;
  font-size: 1.2em;
  color: ${theme.mainGrey};
  font-family: ${theme.fontMenu};
  font-weight: bold;
  cursor: pointer;
  text-decoration:none;
  color: ${(props) => props.active ? 'white' : theme.mainGrey};
  background-color: ${(props) => props.active ? theme.lightGreen : 'white'};

  &:hover {
    background-color: ${theme.lightGreen};
    color: white;
    text-decoration: none;
  }
`;

export default A;
