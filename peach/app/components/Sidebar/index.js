import React from 'react';
import MdHome from 'react-icons/lib/fa/home';
import MdPeople from 'react-icons/lib/md/people';
import MdAssignment from 'react-icons/lib/md/assignment';
import MdSettings from 'react-icons/lib/md/settings';
import PropTypes from 'prop-types';
import Wrapper from './Wrapper';
import A from './A';
import HeaderSide from './HeaderSide';

function Sidebar(props) {
  let beranda = (<A href="/"><MdHome /><br />Beranda</A>);
  let penyuluh = (<A href="/penyuluh"><MdPeople /><br />Penyuluh</A>);
  let data = (<A href="/data"><MdAssignment /><br />Data</A>);
  let edit = (<A href="/edit-ka"><MdSettings /><br />Edit</A>);
  if (props.location === '/') {
    beranda = (
      <A active href="/"><MdHome /><br />Beranda</A>
    );
  } else if (props.location === '/penyuluh') {
    penyuluh = (
      <A active href="/penyuluh"><MdPeople /><br />Penyuluh</A>
    );
  } else if (props.location === '/data') {
    data = (
      <A active href="/data"><MdAssignment /><br />Data</A>
    );
  } else if (props.location === '/edit-ka') {
    edit = (
      <A active href="/edit-ka"><MdSettings /><br />Edit</A>
    );
  }
  const sidebar = (
    <div>
      <HeaderSide> Allocateam </HeaderSide>
      {beranda}
      {penyuluh}
      {data}
      {edit}
    </div>
  );

  return (
    <Wrapper>
      {sidebar}
    </Wrapper>
  );
}

Sidebar.propTypes = {
  location: PropTypes.string,
};

export default Sidebar;
