import React from 'react';
import { shallow } from 'enzyme';

import Sidebar from '../index';
import headerSide from '../HeaderSide';
import A from '../A';


describe('<Sidebar />', () => {
  it('should render 1 Header Side', () => {
    const renderedComponent = shallow(
      <Sidebar />
    );
    expect(renderedComponent.find(headerSide).length).toBe(1);
  });

  it('should render 4 <A> tags', () => {
    const renderedComponent = shallow(
      <Sidebar />
    );
    expect(renderedComponent.find(A).length).toBe(4);
  });
});
