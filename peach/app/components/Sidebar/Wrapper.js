import styled from 'styled-components';
import theme from '../../theme';

const Wrapper = styled.div`
    height: 150%;
    width: 8.3em;
    position: fixed;
    top: 0;
    margin-top: -1em;
    left: 0;
    background-color: white;
    overflow-x: hidden;
    box-shadow: 1px 0px 3px ${theme.lineGrey};
    text-align: center;
`;

export default Wrapper;

