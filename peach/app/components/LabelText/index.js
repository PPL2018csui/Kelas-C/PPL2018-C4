import styled from 'styled-components';
import theme from '../../theme';
const LabelText = styled.h3`
  font-size: 0.98em;
  color: ${theme.mainGrey};
  font-family: ${theme.fontContent};
`;
export default LabelText;
