import React from 'react';
import { shallow } from 'enzyme';

import LabelText from '../index';

describe('<LabelText />', () => {
  it('should render its text', () => {
    const title = 'Text';
    const renderedComponent = shallow(
      <LabelText>{title}</LabelText>
    );
    expect(renderedComponent.contains(title)).toBe(true);
  });
});
