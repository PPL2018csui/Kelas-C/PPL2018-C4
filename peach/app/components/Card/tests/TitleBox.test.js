import React from 'react';
import { shallow } from 'enzyme';
import TitleBox from '../TitleBox';

describe('<TitleBox />', () => {
  it('should render an <div> tag', () => {
    const renderedComponent = shallow(<TitleBox />);
    expect(renderedComponent.type()).toEqual('div');
  });

  it('should have a className attribute', () => {
    const renderedComponent = shallow(<TitleBox />);
    expect(renderedComponent.prop('className')).toBeDefined();
  });
});
