import React from 'react';
import { shallow } from 'enzyme';

import StyledCard from '../StyledCard';

describe('<StyledCard />', () => {
  it('should render an <div> tag', () => {
    const renderedComponent = shallow(<StyledCard />);
    expect(renderedComponent.type()).toEqual('div');
  });

  it('should have a className attribute', () => {
    const renderedComponent = shallow(<StyledCard />);
    expect(renderedComponent.prop('className')).toBeDefined();
  });

  it('should adopt a valid attribute', () => {
    const id = 'test';
    const renderedComponent = shallow(<StyledCard id={id} />);
    expect(renderedComponent.prop('id')).toEqual(id);
  });
});
