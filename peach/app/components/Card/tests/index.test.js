import React from 'react';
import { mount } from 'enzyme';
import Card from '../index';

const children = (<h1>Test</h1>);

const title = 'Test';
const renderComponent = (props = {}) => mount(
  <Card {...props}>
    {children}
  </Card>
);

const renderComponentTitle = (props = { title }) => mount(
  <Card {...props}>
    {children}
  </Card>
);

describe('<Card />', () => {
  it('should render an <div> tag', () => {
    const renderedComponent = renderComponent();
    expect(renderedComponent.find('div').length).toEqual(1);
  });

  it('should have children', () => {
    const renderedComponent = renderComponent();
    expect(renderedComponent.contains(children)).toEqual(true);
  });

  it('should not adopt an invalid attribute', () => {
    const renderedComponent = renderComponent();
    expect(renderedComponent.prop('attribute')).toBeUndefined();
  });

  it('should render title', () => {
    const renderedComponent = renderComponentTitle();
    expect(renderedComponent.find('h3').prop('className')).toBeDefined();
  });
});
