import styled from 'styled-components';
import theme from '../../theme';
const TitleBox = styled.div`
  border-bottom: 1.5px solid ${theme.lineGrey};
  width: 100%;
`;
export default TitleBox;
