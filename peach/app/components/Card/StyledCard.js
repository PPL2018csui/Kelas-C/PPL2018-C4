import styled from 'styled-components';
import theme from '../../theme';

const StyledCard = styled.div`
    display: flex;
    flex-direction: column;
    min-height: 10em;
    color: ${theme.mainGrey};
    background:  ${theme.white};
    border-radius: 10px;
    box-shadow: 4px 4px 9px 0px ${theme.lineGrey};
    padding: 0.7em 1em 0.7em 1em;
    margin: 1em 0.7em 1em 0.7em;
    font-family: ${theme.fontContent};
    font-weight: 300;
`;

export default StyledCard;
