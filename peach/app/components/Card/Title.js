import styled from 'styled-components';
import theme from '../../theme';
const Title = styled.h3`
  color: ${theme.darkGreen};
  font-size: 1.4em;
  font-family: ${theme.fontContent};
  margin: 0.1em 0 0.2em 0.1em;
`;
export default Title;
