import React, { Children } from 'react';
import PropTypes from 'prop-types';
import StyledCard from './StyledCard';
import Title from './Title';
import TitleBox from './TitleBox';

function Card(props) {
  let card = (<div></div>);

  if (props.title) {
    card = (
      <StyledCard>
        <TitleBox>
          <Title>
            {props.title}
          </Title>
        </TitleBox>
        {Children.toArray(props.children)}
      </StyledCard>
    );
  } else {
    card = (
      <StyledCard>
        {Children.toArray(props.children)}
      </StyledCard>
    );
  }

  return (
     card
  );
}

Card.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default Card;
