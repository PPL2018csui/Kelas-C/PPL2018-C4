import { css } from 'styled-components';
import theme from '../../theme';

const buttonStyles = css`
  display: inline-block;
  box-sizing: border-box;
  padding: 0.25em 2em;
  text-decoration: none;
  background: ${theme.lightGreen};
  box-shadow: 3px 3px 3px 0px ${theme.darkGreen};
  border-radius: 10px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-family: ${theme.fontContent};
  font-size: 0.9em;
  border: 0.1em solid ${theme.lightGreen};
  color: ${theme.white};

  &:hover {
    background: transparent;
    color: ${theme.lightGreen};
  }
`;

export default buttonStyles;
