import React from 'react';
import PropTypes from 'prop-types';
import StyledButton from './StyledButton';
import theme from '../../theme';

function TheButton(props) {
  const status = props.status;
  if (status === 'TO_DO') {
    return <StyledButton style={{ color: theme.todoOrange, border: `0.1em solid ${theme.todoOrange}` }}>To Do</StyledButton>;
  } else if (status === 'CANCELLED') {
    return <StyledButton style={{ color: theme.todoRed, border: `0.1em solid ${theme.todoRed}` }}>Cancelled</StyledButton>;
  } return <StyledButton style={{ color: theme.lightGreen, border: `0.1em solid ${theme.lightGreen}` }}>Visited</StyledButton>;
}

TheButton.propTypes = {
  status: PropTypes.string.isRequired,
};

export default TheButton;
