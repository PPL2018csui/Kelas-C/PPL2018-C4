import React from 'react';
import { mount } from 'enzyme';
import TheButton from '../index';

const txt = (<h1>Test</h1>);
const todo = 'TO_DO';
const visited = 'VISITED';
const cancelled = 'CANCELLED';

const renderComponentTodo = () => mount(
  <TheButton status={todo}>
    {txt}
  </TheButton>
);

const renderComponentVisited = () => mount(
  <TheButton status={visited}>
    {txt}
  </TheButton>
);

const renderComponentCancelled = () => mount(
  <TheButton status={cancelled}>
    {txt}
  </TheButton>
);

describe('<TheButton />', () => {
  it('should render an <button> tag', () => {
    const renderedComponent = renderComponentTodo();
    expect(renderedComponent.find('button').length).toEqual(1);
  });

  it('should render an <button> tag', () => {
    const renderedComponent = renderComponentVisited();
    expect(renderedComponent.find('button').length).toEqual(1);
  });

  it('should render an <button> tag', () => {
    const renderedComponent = renderComponentCancelled();
    expect(renderedComponent.find('button').length).toEqual(1);
  });

  it('should not adopt an invalid attribute', () => {
    const renderedComponent = renderComponentTodo();
    expect(renderedComponent.prop('attribute')).toBeUndefined();
  });

  it('should render todo', () => {
    const renderedComponent = renderComponentTodo();
    expect(renderedComponent.prop('status')).toBe('TO_DO');
  });

  it('should not adopt an invalid attribute', () => {
    const renderedComponent = renderComponentVisited();
    expect(renderedComponent.prop('attribute')).toBeUndefined();
  });

  it('should render visited', () => {
    const renderedComponent = renderComponentVisited();
    expect(renderedComponent.prop('status')).toBe('VISITED');
  });

  it('should not adopt an invalid attribute', () => {
    const renderedComponent = renderComponentCancelled();
    expect(renderedComponent.prop('attribute')).toBeUndefined();
  });

  it('should render cancelled', () => {
    const renderedComponent = renderComponentCancelled();
    expect(renderedComponent.prop('status')).toBe('CANCELLED');
  });
});
