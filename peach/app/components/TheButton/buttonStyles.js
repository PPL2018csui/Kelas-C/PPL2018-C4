import { css } from 'styled-components';
import theme from '../../theme';

const buttonStyles = css`
    width: 8em;
    display: inline-block;
    box-sizing: border-box;
    padding: 0.25em 0;
    text-decoration: none;
    border-radius: 10px;
    user-select: none;
    cursor: none;
    outline: 0;
    font-family: ${theme.fontContent};
    font-size: 0.9em;
    background: transparent;
`;

export default buttonStyles;
