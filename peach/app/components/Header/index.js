import React from 'react';
import Wrapper from './Wrapper';
import Hello from './Hello';
import StyledButton from './StyledButton';

let url = '';
const locs = window.location.host;
if (locs === 'localhost:3000') {
  url = 'http://localhost:5000/logout';
} else if (locs === 'sit-uat.allocateam.com') {
  url = 'http://sit-uat.mario.allocateam.com/logout';
} else {
  url = 'http://mario.allocateam.com/logout';
}

function Header() {
  const header = (
    <div>
      <Hello style={{ display: 'inline-block' }}> Hi, allocater! </Hello>
      <a href={url}><StyledButton style={{ fontWeight: 'bold' }}> Logout </StyledButton></a>
    </div>
  );

  return (
    <Wrapper>
      {header}
    </Wrapper>
  );
}

export default Header;
