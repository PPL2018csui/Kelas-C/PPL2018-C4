import React from 'react';
import { shallow } from 'enzyme';
import Hello from '../Hello';
import StyledButton from '../StyledButton';
import Header from '../index';

describe('<Header />', () => {
  it('should render Hello', () => {
    const renderedComponent = shallow(
      <Header />
    );
    expect(renderedComponent.find(Hello).length).toBe(1);
  });

  it('should render logout button', () => {
    const renderedComponent = shallow(
      <Header />
    );
    expect(renderedComponent.find(StyledButton).length).toBe(1);
  });
});
