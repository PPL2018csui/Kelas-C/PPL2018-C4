import styled from 'styled-components';
import helloStyles from './helloStyles';

const Hello = styled.h1`${helloStyles}`;

export default Hello;
