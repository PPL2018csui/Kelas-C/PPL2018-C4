import styled from 'styled-components';
import theme from '../../theme';

const Wrapper = styled.div`
  position: fixed;
  top: 0px;
  right: 0px;
  width: 100%;
  height: 3.4em;
  background-color: white;
  box-shadow: 2.5px 2.5px 20px ${theme.lineGrey};
  text-align: right;
  padding-right: 1em;
  padding-bottom: 2em;
`;

export default Wrapper;
