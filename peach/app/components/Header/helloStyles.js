import { css } from 'styled-components';
import theme from '../../theme';

const helloStyles = css`
  display: inline-block;
  font-size: 1.3em;
  color: ${theme.todoOrange};
  font-family: ${theme.fontMenu};
  margin-right: 40px;
  margin-top: 0.5em;
  text-shadow: 5px 5px 1px ${theme.graphicYellow};
`;

export default helloStyles;
