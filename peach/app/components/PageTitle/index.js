import styled from 'styled-components';
import theme from '../../theme';

const PageTitle = styled.h1`
  font-size: 2em;
  margin-left: 0.25em;
  color: ${theme.darkGreen};
  font-family: ${theme.fontContent};
  margin-bottom: -4em;
  margin-left: 0.8em;
  padding-top: 0.3em;
`;

export default PageTitle;
