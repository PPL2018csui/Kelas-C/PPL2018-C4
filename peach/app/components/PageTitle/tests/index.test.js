import React from 'react';
import { shallow } from 'enzyme';

import PageTitle from '../index';

describe('<PageTitle />', () => {
  it('should render its text', () => {
    const title = 'Text';
    const renderedComponent = shallow(
      <PageTitle>{title}</PageTitle>
    );
    expect(renderedComponent.contains(title)).toBe(true);
  });
});
