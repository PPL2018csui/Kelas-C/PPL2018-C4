import styled from 'styled-components';
import cardStyles from './cardStyles';

/* istanbul ignore next */
const StyledCard = styled.div`${cardStyles}`;

export default StyledCard;
