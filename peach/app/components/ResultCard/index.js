/**
 *
 * Card.js
 *
 */

import React, { Children } from 'react';
import PropTypes from 'prop-types';
import StyledCard from './StyledCard';

function Card(props) {
  return (
    <StyledCard>
      {Children.toArray(props.children)}
    </StyledCard>
  );
}

Card.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Card;
