import { css } from 'styled-components';
import theme from '../../theme';

const cardStyles = css`
  display: flex;
  flex-direction: column;
  min-height: 5em;
  color: ${theme.mainGrey};
  background:  ${theme.white};
  border-radius: 10px;
  box-shadow: 4px 4px 9px 0px ${theme.lineGrey};
  padding: 0.7em 0 0.7em 0;
  margin: 1em 1em 1em 1em;
  font-family: ${theme.fontContent};
  font-weight: 300;
  width: 100%;
  padding-bottom: 1em;
`;

export default cardStyles;
