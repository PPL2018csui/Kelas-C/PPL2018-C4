import { injectGlobal } from 'styled-components';
import theme from './theme';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Montserrat', sans-serif;
    background-color: ${theme.groundGrey};
    font-size: 0.92rem;
  }

  body.fontLoaded {
    font-family: 'Lora', serif;
  }

  #app {
    background-color: ${theme.groundGrey};
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: 'Montserrat', sans-serif;
    line-height: 1.5em;
  }

  #react-paginate ul {
    display: inline-block;
    float: right;
    margin-top: -1.7em;
  }
  
  #react-paginate li {
    list-style-type: none;
    display: inline-block;
    margin-left: 8px;
    margin-right: 8px;
    box-sizing: border-box;
    padding: 0.3em 1em;
    text-decoration: none;
    background: transparent;
    box-shadow: 1px 1px 3px 0px ${theme.darkGreen};
    border-radius: 4px;
    -webkit-font-smoothing: antialiased;
    -webkit-touch-callout: none;
    user-select: none;
    cursor: pointer;
    outline: 0;
    font-family: ${theme.fontContent};
    font-size: 0.9em;
    font-weight: bold;
    border: 0.1em solid ${theme.lightGreen};
    color: ${theme.lightGreen};
    &:hover {
      background: ${theme.lightGreen};
      color: white;
    }
  }

  #react-paginate .active {
    background: ${theme.lightGreen};
    color: white;
  }
  
  #react-paginate .break a {
    cursor: pointer;
  }
  
  a {
    text-decoration: none;
  }
`;
