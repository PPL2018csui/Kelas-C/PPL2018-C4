import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { API_ROOT } from './api-config';
import theme from '../../theme';

const Konten = styled.ul`
  height: 2em;
  padding-top: 0.65em;
  padding-left: 0em;
  margin-bottom: 0em;
  border-top: 1px solid ${theme.lineGrey};
`;

const Nomor = styled.li`
  display: inline-block;
  text-align: center;
  padding-left: 2em;
  width: 5%;
`;

const Nama = styled.li`
  margin-left: 1em;
  display: inline-block;
  text-align: left;
  width: 37%;
`;

const KetuaArisan = styled.li`
  display: inline-block;
  text-align: center;
  width: 12.5%;
`;

const Status = styled.li`
  margin-left: 2.5em;
  text-align: center;
  display: inline-block;
  box-sizing: border-box;
  padding: 0.25em 2em;
  text-decoration: none;
  background: ${theme.todoOrange};
  border-radius: 10px;
  box-shadow: 3px 3px 10px 0px ${theme.graphicOrange};
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  margin-top: -1em;
  font-family: ${theme.todoOrange};
  font-size: 0.9em;
  border: 0.1em solid ${theme.todoOrange};
  color: ${theme.white};
  font-weight: bold;

  &:hover {
    background: transparent;
    color: ${theme.todoOrange};
  }
`;

/* eslint-disable */
export class PenyuluhResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const currentClass = this;
    fetch(`${API_ROOT}/branch`)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) { 
      currentClass.setState({
        data: data, 
      });
    });
  }

  handleClick(e) {
    const branchId = e.target.id;
    this.props.onChange(branchId);
  }

  render() {
    const { result = [] } = this.state.data;
    while (result.length <= 0) {
      return (
        <div>
          <Konten>
            <Nomor></Nomor>
            <Nama>loading...</Nama>
          </Konten>
        </div>
      );
    }
    return (
      <div>
        {
          Object.keys(result).map((key, index) => (
            <Konten key={key}>
              <Nomor>{index + 1}.</Nomor><Nomor />
              <Nama>{result[key].branch_name}</Nama>
              <KetuaArisan>{result[key].branch_id}</KetuaArisan>
              <Nomor /><Status id={result[key].branch_id} onClick={(key) => this.handleClick(key)}>Select Branch</Status>
            </Konten>
          ))
        }
      </div>
    );
  }
}

PenyuluhResult.propTypes = {
  onChange: PropTypes.func,
};

export default PenyuluhResult;
