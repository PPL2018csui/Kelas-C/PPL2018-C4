import React from 'react';
import { shallow } from 'enzyme';
import Card from 'components/ResultCard';
import PageTitle from 'components/PageTitle';
import Allocation from '../index';

describe('<Allocation />', () => {
  it('should render a name', () => {
    const renderedComponent = shallow(
      <Allocation />
    );
    expect(renderedComponent.find(PageTitle).length).toBe(1);
  });
  it('should render a card', () => {
    const renderedComponent = shallow(
      <Allocation />
    );
    expect(renderedComponent.find(Card).length).toBe(1);
  });
});
