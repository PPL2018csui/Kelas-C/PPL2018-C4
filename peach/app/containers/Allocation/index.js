import React from 'react';
import PageTitle from 'components/PageTitle';
import Card from 'components/ResultCard';
import styled from 'styled-components';
import ReactPaginate from 'react-paginate';
import AllocationResult from './AllocationResult';
import PenyuluhResult from './PenyuluhResult';
import { API_ROOT } from './api-config';
import theme from '../../theme';

const Row = styled.div`
  margin: 0 -1em;
  margin-left: 0.3em;
  display: flex;
  min-height: 100%;
  padding: 0 1em;
  flex-direction: row;
  flex-wrap: wrap;
`;

const Judul = styled.h3`
  height: 2.2em;
  padding-top: 0.8em;
  margin-top: -0.5em;  
  margin-bottom: 0em;
`;

const Nomor = styled.li`
  display: inline-block;
  text-align: center;
  padding-left: 2em;
  width: 5%;
`;

const Nama = styled.li`
  marginLeft: 0em;
  display: inline-block;
  text-align: center;
  width: 25%;
`;

const KetuaArisan = styled.li`
  display: inline-block;
  text-align: center;
  width: 12.5%;
`;

const Tempat = styled.li`
  display: inline-block;
  text-align: center;
  width: 20%;
`;

const Kelurahan = styled.li`
  display: inline-block;
  text-align: center;
  width: 12.5%;
`;

const Tanggal = styled.li`
  display: inline-block;
  text-align: center;
  width: 15%;
`;

const Status = styled.li`
  display: inline-block;
  text-align: center;
  width: 15%;
`;

const Back = styled.li`
  list-style-type: none;
  text-align: center;
  box-sizing: border-box;
  padding: 0.25em 2em;
  text-decoration: none;
  background: ${theme.todoOrange};
  border-radius: 10px;
  box-shadow: 3px 3px 10px 0px ${theme.graphicOrange};
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  margin-top: 1em;
  font-family: ${theme.todoOrange};
  font-size: 1em;
  border: 0.1em solid ${theme.todoOrange};
  color: ${theme.white};
  font-weight: bold;
  width: 10%;

  &:hover {
    background: transparent;
    color: ${theme.todoOrange};
  }
`;

/* eslint-disable */
export class Allocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      branchId: -1,
      max_page: 0,
    };
    this.handlePageClick = this.handlePageClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handlePageClick = (data) => {
    const selected = data.selected;
    this.setState({
      offset: selected * 20,
    });
  };

  handleChange(branchId) {
    this.setState({
      branchId: branchId,
    });
    const currentClass = this;
    fetch(`${API_ROOT}/workloads?branch_id=${branchId}`)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      currentClass.setState({
        max_page: data['meta']['max_page']+1,
      });
    });
  }

  render() {
    if (this.state.branchId === -1) {
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Pilih Branch</PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '1.5em', marginRight: '0em', width: '70%' }}>
            <Card>
              <Judul>
                <Nomor></Nomor>
                <Nama style={{ width: '40%', marginLeft: '-1em' }}>Nama Branch</Nama>
                <KetuaArisan style={{ marginLeft: '2.7em' }}>Branch ID</KetuaArisan>
              </Judul>
              <PenyuluhResult onChange={this.handleChange} />
            </Card>
          </Row>
        </div>
      );
    } else {
      return (
        <div>
          <PageTitle style={{  }}>Jadwal Mingguan</PageTitle>
          <a href="/jadwal" onClick={this.handleClick}><Back style={{ marginTop: '8.5em', display: 'inline-block', marginLeft: '5em' }}>Back</Back></a>
          <div id="react-paginate">
            <ReactPaginate
              previousLabel={'previous'}
              pageRangeDisplayed={3}
              marginPageDisplayed={3}
              nextLabel={'next'}
              pageCount={this.state.max_page}
              onPageChange={this.handlePageClick}
              containerClassName={'pagination'}
              subContainerClassName={'pages pagination'}
              activeClassName={'active'}
            /><br />
          </div>
          <Row style={{ marginLeft: '1.5em', marginRight: '0em'}}>
            <Card>
              <Judul>
                <Nama>Nama PY</Nama>
                <KetuaArisan>Nama KA</KetuaArisan>
                <Tempat>Tempat</Tempat>
                <Kelurahan>Kelurahan</Kelurahan>
                <Tanggal>Tanggal</Tanggal>
                <Status>Status</Status>
              </Judul>
              <AllocationResult offset={this.state.offset} branchId={this.state.branchId}/>
            </Card>
          </Row>
        </div>
      );
    }
  }
}

export default Allocation;
