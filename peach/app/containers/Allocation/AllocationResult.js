import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import TheButton from 'components/TheButton';
import { API_ROOT } from './api-config';
import theme from '../../theme';

const Konten = styled.ul`
  height: 2em;
  padding-top: 0.65em;
  padding-left: 0em;
  margin-bottom: 0em;
  border-top: 1px solid ${theme.lineGrey};
`;

const Nomor = styled.li`
  display: inline-block;
  text-align: center;
  width: 5%;
`;

const Nama = styled.li`
  marginLeft: 0em;
  display: inline-block;
  text-align: left;
  width: 20%;
`;

const KetuaArisan = styled.li`
  display: inline-block;
  text-align: left;
  width: 12.5%;
`;

const Tempat = styled.li`
  display: inline-block;
  text-align: left;
  width: 20%;
`;

const Kelurahan = styled.li`
  display: inline-block;
  text-align: center;
  width: 12.5%;
`;

const Tanggal = styled.li`
  display: inline-block;
  text-align: center;
  width: 15%;
`;

const Status = styled.li`
  display: inline-block;
  text-align: center;
  width: 15%;
`;

/* eslint-disable */
class AllocationResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      prp: 0,
      branchId: this.props.branchId,
    };
  }

  componentDidMount() {
    const currentClass = this;
    fetch(`${API_ROOT}/workloads?offset=${this.state.prp}&limit=${20}&branch_id=${this.state.branchId}`)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      currentClass.setState({
        data: data,
      });
    });
  }

  componentWillReceiveProps(nextProps) {
    const currentClass = this;
    if (currentClass.state.prp !== nextProps.offset) {
      currentClass.setState({
        prp: nextProps.offset,
      });
      fetch(`${API_ROOT}/workloads?offset=${nextProps.offset}&limit=${20}&branch_id=${this.state.branchId}`)
      .then(function (response) { 
        return response.json();
      })
      .then(function (data) { 
        currentClass.setState({
          data: data, 
        });
      });
    }
  }

  render() {
    const { result = [] } = this.state.data;
    while (result.length <= 0) {
      return (
        <div>
          <Konten>
            <Nomor></Nomor>
            <Nama>loading...</Nama>
          </Konten>
        </div>
      );
    }
    return (
      <div>
        {
          Object.keys(result).map((key, index) => (
            <Konten key={key}>
              <Nomor>{index + (this.props.offset) + 1}.</Nomor>
              <Nama>{result[key].nama}</Nama>
              <KetuaArisan>{result[key].ketua_arisan}</KetuaArisan>
              <Tempat>{result[key].alamat}</Tempat>
              <Kelurahan>{result[key].kelurahan}</Kelurahan>
              <Tanggal>{result[key].date}</Tanggal>
              <Status><TheButton status={result[key].status}></TheButton></Status>
            </Konten>
          ))
        }
      </div>
    );
  }
}

AllocationResult.propTypes = {
  offset: PropTypes.number,
};

export default AllocationResult;
