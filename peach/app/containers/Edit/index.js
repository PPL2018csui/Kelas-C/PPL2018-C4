/**
 *
 * SubmitForm
 *
 */

import React from 'react';
import { post } from 'axios';
import styled from 'styled-components';

import StyledButton from 'components/Button/StyledButton';
import LabelText from 'components/LabelText';
import PageTitle from 'components/PageTitle';
import Card from 'components/Card';

import { EDIT_ENDPOINT } from './constants';

const Row = styled.div`
  margin: 0 -1em;
  margin-left: 0.3em;
  display: flex;
  min-height: 100%;
  padding: 0 1em;
  flex-direction: row;
  flex-wrap: wrap;
`;

const TextInput = styled.input`
  width: 20%;
  padding: 4px 4px;
  margin: 2px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
`;

/* eslint-disable */
export class Edit extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      ka_id: -1,
      field: '',
      new_value: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit = (event) => {
    console.log(this.state); // eslint-disable-line no-console
    event.preventDefault();

    const formData = new FormData();
    formData.set('ka_id', this.state.ka_id);
    formData.set('field', this.state.field);
    formData.set('new_value', this.state.new_value);

    const config = {
      mode: 'no-cors',
    };

    post(EDIT_ENDPOINT, formData, config)
    .then((response) => {
      const data = response.data;
      const resp = data['http-status'].message;
      console.log(response); // eslint-disable-line no-console
    })
    .catch((error) => {
      const data = error.response.data;
      const resp = data['http-status'].message;
    });
  }

  render() {
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Perbaharui Data </PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '2em', marginRight: '0em' }}>
            <div style={{ width: '25em' }}>
              <Card title="Data PY dan KA">
                <form onSubmit={this.handleSubmit}>
                  <LabelText> Ketua Arisan [ID] </LabelText>
                  <TextInput
                    type="text"
                    name="ka_id"
                    onChange={this.handleInputChange}
                    required/>
                  <br />
                  <LabelText> Field </LabelText>
                  Pilih di antara 'is_signed', 'is_registered', 'last_visited', 'visit_count':
                  <br />
                  <TextInput style={{ width: '50%' }}
                    type="text"
                    name="field"
                    value={this.state.field}
                    onChange={this.handleInputChange}
                    required/>
                  <LabelText> Value </LabelText>
                    Boolean: True or False
                    <br />
                    Date: dd/mm/yyyy
                    <br />
                    Count: Integer
                    <br />
                    <TextInput style={{ width: '50%' }}
                      type="text"
                      name="new_value"
                      value={this.state.new_value}
                      onChange={this.handleInputChange}
                      required/>
                    <br />
                  <br />
                  <StyledButton type="submit">Submit</StyledButton>
                </form>
              </Card>
            </div>
          </Row>
        </div>
      );
  }
}

export default Edit;
