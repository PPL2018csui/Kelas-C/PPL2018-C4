/**
 *
 * SubmitForm
 *
 */

import React from 'react';
import { post } from 'axios';
import styled from 'styled-components';

import StyledButton from 'components/Button/StyledButton';
import LabelText from 'components/LabelText';
import PageTitle from 'components/PageTitle';
import Card from 'components/Card';

import { EDIT_ENDPOINT } from './constants';

const Row = styled.div`
  margin: 0 -1em;
  margin-left: 0.3em;
  display: flex;
  min-height: 100%;
  padding: 0 1em;
  flex-direction: row;
  flex-wrap: wrap;
`;

const TextInput = styled.input`
  width: 20%;
  padding: 4px 4px;
  margin: 2px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
`;

/* eslint-disable */
export class EditKetua extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      ka_id: -1,
      isSignup: false,
      isRegister: false,
      visit_count: 0,
      date: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit = (event) => {
    console.log(this.state); // eslint-disable-line no-console
    event.preventDefault();
    
    const [year, month, day] = this.state.date.split('-');
    const serverDate = `${day}/${month}/${year}`;
    console.log(serverDate)

    // let field = {};
    // field.is_signed = this.state.isSignup;
    // field.is_registered = this.state.isRegister;
    // field.last_visited = serverDate;
    // field.visit_count = this.state.visit_count;
    
    // let field_final = JSON.stringify(field)
    // console.log(field_final); // eslint-disable-line no-console
    // is_signed', 'is_registered', 'last_visited', 'visit_count'

    const formData = new FormData();
    formData.set('ka_id', this.state.ka_id);
    formData.set('is_signed', this.state.isSignup);
    formData.set('is_registered', this.state.isRegister);
    formData.set('last_visited', serverDate);
    formData.set('visit_count', this.state.visit_count);
    
    console.log(formData); // eslint-disable-line no-console
    console.log(EDIT_ENDPOINT);

    const config = {
      mode: 'no-cors',
    };

    post(EDIT_ENDPOINT, formData, config)
    .then((response) => {
      const data = response.data;
      const resp = data['http-status'].message;
      console.log(response); // eslint-disable-line no-console
      alert(resp)
    })
    .catch((error) => {
      const data = error.response.data;
      const resp = data['http-status'].message;
    });
  }

  render() {
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Perbaharui Data </PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '2em', marginRight: '0em' }}>
            <div style={{ width: '25em' }}>
              <Card title="Data PY dan KA">
                <form onSubmit={this.handleSubmit}>
                  <LabelText> Ketua Arisan [ID] </LabelText>
                  <TextInput 
                    type="text" 
                    name="ka_id" 
                    onChange={this.handleInputChange} 
                    required/>
                  <br />
                  <LabelText> Sign-up? </LabelText>
                    <input
                      name="isSignup"
                      type="checkbox"
                      checked={this.state.isSignup}
                      onChange={this.handleInputChange} />
                  <LabelText> Registered? </LabelText> 
                    <input
                      name="isRegister"
                      type="checkbox"
                      checked={this.state.isRegister}
                      onChange={this.handleInputChange} />
                  <LabelText> Visit count? </LabelText> 
                    <TextInput
                      name="visit_count"
                      type="number"
                      value={this.state.visit_count}
                      onChange={this.handleInputChange} />
                  <LabelText> Last visited date</LabelText>
                    <TextInput style={{ width: '50%' }}
                      type="date" 
                      name="date" 
                      value={this.state.date}
                      onChange={this.handleInputChange}
                      />
                  <br />
                  <br />
                  <StyledButton type="submit">Submit</StyledButton>
                </form>
              </Card>
            </div>
          </Row>
        </div>
      );
  }
}

export default EditKetua;
