import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { BarChart, Bar, XAxis, YAxis, ResponsiveContainer, CartesianGrid, Tooltip, Legend } from 'recharts';
import { API_ROOT } from './api-config';
import theme from '../../theme';

const StyledChart = styled.div`
  margin: 0 -1em;
  display: flex;
  min-height: 100%;
  padding: 0 16px;
  flex-direction: row;
  flex-wrap: wrap;
`;

/* eslint-disable */
class ChartResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const currentClass = this;
    fetch(`${API_ROOT}/graph-workloads/${this.props.branchId}`)
    .then(function (response) { // eslint-disable-line prefer-arrow-callback
      return response.json();
    })
    .then(function (data) { // eslint-disable-line prefer-arrow-callback
      currentClass.setState({
        data: data, // eslint-disable-line object-shorthand
      });
    });
  }

  render() {
    const { result = [] } = this.state.data;
    while (result.length <= 0) {
      return (
        <div>
          <h3>loading...</h3>
        </div>
      );
    }
    const key = Object.keys(result[0]);
    const color = [
      theme.graphicGreen,
      theme.graphicYellow,
      theme.graphicDGreen,
      theme.graphicLightPink,
      theme.graphicLightGreen,
      theme.graphicPurple,
      theme.graphicOrange,
      theme.graphicBlue,
      theme.graphicPink,
      theme.graphicGold,
    ];
    return (
      <ResponsiveContainer>
        <StyledChart>
          <BarChart
            width={600}
            height={300}
            data={result}
            margin={{ top: 35, right: 5, left: 5, bottom: 30 }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            {
              key.map((k, index) => {
                if (k !== 'name') {
                  return (<Bar dataKey={k} fill={color[index]} />);
                };
              })
            }
          </BarChart>
        </StyledChart>
      </ResponsiveContainer>
    );
  }
}

ChartResult.propTypes = {
  data: PropTypes.any,
  branchId: PropTypes.any,
};

export default ChartResult;
