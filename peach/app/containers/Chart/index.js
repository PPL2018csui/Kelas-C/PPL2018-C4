import React from 'react';
import styled from 'styled-components';
import PageTitle from 'components/PageTitle';
import Card from 'components/Card';
import ChartResult from './ChartResult';

const Row = styled.div`
  margin: 0 -1em;
  margin-left: 0.3em;
  display: flex;
  min-height: 100%;
  padding: 0 1em;
  flex-direction: row;
  flex-wrap: wrap;
`;

export function Chart() {
  return (
    <div>
      <PageTitle style={{ display: 'inline-block' }}>Grafik Penyuluh</PageTitle>
      <Row style={{ marginTop: '1em', marginLeft: '2.5em' }}>
        <Card>
          <ChartResult branchId={this.props.branchId} />
        </Card>
      </Row>
    </div>
  );
}

export default Chart;
