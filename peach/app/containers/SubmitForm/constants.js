/*
 *
 * SubmitForm constants
 *
 */

import { API_ROOT } from './api-config';

export const MARIO_POST_ENDPOINT1 = `${API_ROOT}/upload`;
export const MARIO_POST_ENDPOINT2 = `${API_ROOT}/upload2`;
