/**
 *
 * SubmitForm
 *
 */

import React from 'react';
import { post } from 'axios';
import styled from 'styled-components';

import StyledButton from 'components/Button/StyledButton';
import LabelText from 'components/LabelText';
import PageTitle from 'components/PageTitle';
import Card from 'components/Card';

import { API_ROOT } from './api-config';
import { MARIO_POST_ENDPOINT1, MARIO_POST_ENDPOINT2 } from './constants';

const Row = styled.div`
  margin: 0 -1em;
  margin-left: 0.3em;
  display: flex;
  min-height: 100%;
  padding: 0 1em;
  flex-direction: row;
  flex-wrap: wrap;
`;

/* eslint-disable */
export class SubmitForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      flag: 0,
      worker: "",
      status: "Worker is starting..",
      currentId: -1,
    };
    this.upload1 = this.upload1.bind(this);
    this.upload2 = this.upload2.bind(this);
    this.checkWorker = this.checkWorker.bind(this);
  }

  checkWorker() {
    console.log("Halo")
    const currentClass = this;
    const workerId = this.state.worker;
    fetch(`${API_ROOT}/upload2/status/${workerId}`)
    .then(function (response) { 
      return response.json();
    })
    .then(function (data) { 
      currentClass.setState({
        status: data.status,
        currentId: data.current,
      });
      if (data.status !== "Task completed!") {
        currentClass.checkWorker();
      } else {
        currentClass.setState({
          flag: 2,
        })
      }
    });
  }

  upload1 = (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append('branch', event.target.branch.files[0]);
    formData.append('ketua_arisan', event.target.ketua_arisan.files[0]);
    formData.append('penyuluh', event.target.penyuluh.files[0]);
    formData.append('territory', event.target.territory.files[0]);
    const config = {
      mode: 'no-cors',
      headers: {
        'content-type': 'multipart/form-data',
      },
    };

    post(MARIO_POST_ENDPOINT1, formData, config)
    .then((response) => {
      const data = response.data;
      const resp = data['http-status'].message;
      const worker = data['worker-status'];
      console.log(worker);
      console.log(response); // eslint-disable-line no-console
    })
    .catch((error) => {
      const data = error.response.data;
      const resp = data['http-status'].message;
    });
  }

  upload2 = (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append('ketua_arisan', event.target.ketua_arisan.files[0]);
    formData.append('penyuluh', event.target.penyuluh.files[0]);
    const config = {
      mode: 'no-cors',
      headers: {
        'content-type': 'multipart/form-data',
      },
    };

    post(MARIO_POST_ENDPOINT2, formData, config)
    .then((response) => {
      this.setState({
        flag: 1,
        worker: response.data['worker-status'],
      })
    })
    .catch((error) => {
      console.log(error.response); // eslint-disable-line no-console
    });
  }

  render() {
    if (this.state.flag === 0) {
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Perbaharui Data </PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '2em', marginRight: '0em' }}>
            <div style={{ width: '25em' }}>
              <Card title="Data PY dan KA">
                <form onSubmit={this.upload2}>
                  <LabelText> File Penyuluh [PY] </LabelText>
                  <input id="penyuluh-file2" type="file" name="penyuluh" />
                  <br />
                  <LabelText> File Ketua Arisan [KA] </LabelText>
                  <input id="ka-file2" type="file" name="ketua_arisan" />
                  <br />
                  <br />
                  <StyledButton type="submit">Submit</StyledButton>
                  <br />
                  <br />
                </form>
              </Card>
            </div>
          </Row>
        </div>
      );
    } else if (this.state.flag === 1) { 
      this.checkWorker();
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Perbaharui Data </PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '2em', marginRight: '0em' }}>
            <div style={{ width: '25em' }}>
              <Card title="Data PY dan KA">
                <form onSubmit={this.upload2}>
                  <LabelText> File Penyuluh [PY] </LabelText>
                  <input id="penyuluh-file2" type="file" name="penyuluh" />
                  <br />
                  <LabelText> File Ketua Arisan [KA] </LabelText>
                  <input id="ka-file2" type="file" name="ketua_arisan" />
                  <br />
                  <br />
                  <StyledButton style={{ paddingLeft: '0.85em', cursor: 'none', width: '30%', background: 'white', color: 'green' }}>Uploading...</StyledButton>
                  <br />
                  <br />
                  <h2 style={{ textAlign: 'center', color: 'orange' }}>{this.state.status}</h2>
                  <h2 style={{ textAlign: 'center', color: 'green' }}>ID: {this.state.currentId}</h2>
                  <br />
                </form>
              </Card>
            </div>
          </Row>
        </div>
      );
    } else {
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Perbaharui Data </PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '2em', marginRight: '0em' }}>
            <div style={{ width: '25em' }}>
              <Card title="Data PY dan KA">
                <form onSubmit={this.upload2}>
                  <LabelText> File Penyuluh [PY] </LabelText>
                  <input id="penyuluh-file2" type="file" name="penyuluh" />
                  <br />
                  <LabelText> File Ketua Arisan [KA] </LabelText>
                  <input id="ka-file2" type="file" name="ketua_arisan" />
                  <br />
                  <br />
                  <StyledButton style={{ paddingLeft: '1.35em', cursor: 'none', width: '30%', background: 'white', color: 'green' }}>Uploaded</StyledButton>
                  <br />
                  <br />
                  <h1 style={{ textAlign: 'center', color: 'orange' }}>{this.state.status}</h1>
                  <br />
                </form>
              </Card>
            </div>
          </Row>
        </div>
      );
    }
  }
}

export default SubmitForm;
