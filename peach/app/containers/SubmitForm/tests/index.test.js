import React from 'react';
import { shallow } from 'enzyme';
import Card from 'components/Card';
import PageTitle from 'components/PageTitle';
import Button from 'components/Button/StyledButton';
import SubmitForm from '../index';

describe('<SubmitForm />', () => {
  it('should render the card', () => {
    const renderedComponent = shallow(
      <SubmitForm />
    );
    expect(renderedComponent.find(Card).length).toBe(1);
  });

  it('should render a Page Title', () => {
    const renderedComponent = shallow(
      <SubmitForm />
    );
    expect(renderedComponent.find(PageTitle).length).toBe(1);
  });

  it('should render 1 Button', () => {
    const renderedComponent = shallow(
      <SubmitForm />
    );
    expect(renderedComponent.find(Button).length).toBe(1);
  });
});
