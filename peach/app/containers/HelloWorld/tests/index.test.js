import React from 'react';
import { shallow } from 'enzyme';
import PageTitle from 'components/PageTitle';
import HelloWorld from '../index';

describe('<HelloWorld />', () => {
  it('should render the hello', () => {
    const renderedComponent = shallow(
      <HelloWorld />
    );
    expect(renderedComponent.find(PageTitle).length).toBe(1);
  });
});
