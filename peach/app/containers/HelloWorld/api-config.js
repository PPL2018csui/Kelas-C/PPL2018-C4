let backendHost;
const apiVersion = 'v1';
const hostname = window && window.location && window.location.hostname;

if (hostname === 'allocateam.com') {
  backendHost = 'http://mario.allocateam.com';
} else if (hostname === 'sit-uat.allocateam.com') {
  backendHost = 'http://sit-uat.mario.allocateam.com';
} else {
  backendHost = 'http://localhost:5000';
}

export const API_ROOT = `${backendHost}/api/${apiVersion}`;
