import React from 'react';
import PageTitle from 'components/PageTitle';

/* eslint-disable */
export class HelloWorld extends React.Component {
  render() {
    return (
      <div>
        <PageTitle style={{ display: 'inline-block' }}>The Best Allocator in 2018</PageTitle>
      </div>
    );
  }
}

export default HelloWorld;
