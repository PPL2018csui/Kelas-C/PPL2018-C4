import React from 'react';
import { shallow } from 'enzyme';
import Button from 'components/Button';
import Login from '../index';

describe('<Login />', () => {
  it('should render the h1', () => {
    const renderedComponent = shallow(
      <Login />
    );
    expect(renderedComponent.find('h1').length).toBe(1);
  });
  it('should render the button', () => {
    const renderedComponent = shallow(
      <Login />
    );
    expect(renderedComponent.find(Button).length).toBe(1);
  });
});
