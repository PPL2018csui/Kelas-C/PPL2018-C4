import React from 'react';
import styled from 'styled-components';
import Button from 'components/Button';
import theme from '../../theme';

const Row = styled.div`
  margin: 0 -1em;
  display: flex;
  min-height: 100%;
  padding: 0 16px;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
   height: 300px;
`;

let url = '';
const locs = window.location.host;
if (locs === 'localhost:3000') {
  url = 'http://localhost:5000/login';
} else if (locs === 'sit-uat.allocateam.com') {
  url = 'http://sit-uat.mario.allocateam.com/login';
} else {
  url = 'http://mario.allocateam.com/login';
}

export default function Login() {
  return (
    <div>
      <Row style={{ marginLeft: '-12.5em', marginTop: '7em' }}>
        <h1 style={{ marginTop: '-3em', color: theme.darkGreen }}>Welcome Back!</h1>
        <div style={{ fontWeight: 'bold', marginLeft: '-12.3em' }}>
          <a href={url}><Button>
            Login with Google
          </Button></a>
        </div>
      </Row>
    </div>
  );
}
