/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import Header from 'components/Header';
import Sidebar from 'components/Sidebar';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import SubmitForm from 'containers/SubmitForm/Loadable';
import HelloWorld from 'containers/HelloWorld/Loadable';
import Penyuluh from 'containers/Penyuluh/Loadable';
import Edit from 'containers/Edit/Loadable';
import Login from 'containers/Login/Loadable';
import EditKetua from 'containers/EditKetua/Loadable';
import { Switch, Route } from 'react-router-dom';

const AppWrapper = styled.div`
  display: flex;
  min-height: 100%;
  padding: 4em 2.5em 2em 9.5em;
  flex-direction: column;
  overflow-x: hidden;
`;

const pathname = window.location.pathname;

export default function App() {
  if (pathname !== '/login') {
    return (
      <div>
        <AppWrapper>
          <div>
            <Header />
            <Sidebar location={pathname} />
          </div>
          <Helmet
            titleTemplate="%s - allocateam | PPL-C4"
            defaultTitle="allocateam | PPL-C4"
          >
            <meta name="description" content="A React.js Boilerplate application" />
          </Helmet>
          <Switch>
            <Route exact path="/data" component={SubmitForm} />
            <Route exact path="/" component={HelloWorld} />
            <Route path="/login" component={Login} />
            <Route path="/penyuluh" component={Penyuluh} />
            <Route path="/edit" component={Edit} />
            <Route path="/edit-ka" component={EditKetua} />
            <Route path="" component={NotFoundPage} />
          </Switch>
        </AppWrapper>/
      </div>
    );
  }
  return (
    <div>
      <AppWrapper>
        <Helmet
          titleTemplate="%s - allocateam | PPL-C4"
          defaultTitle="allocateam | PPL-C4"
        >
          <meta name="description" content="A React.js Boilerplate application" />
        </Helmet>
        <Switch>
          <Route exact path="/data" component={SubmitForm} />
          <Route exact path="/" component={HelloWorld} />
          <Route path="/login" component={Login} />
          <Route path="/penyuluh" component={Penyuluh} />
          <Route path="/edit" component={Edit} />
          <Route path="/edit-ka" component={EditKetua} />
          <Route path="" component={NotFoundPage} />
        </Switch>
      </AppWrapper>/
    </div>
  );
}
