/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';

import PageTitle from 'components/PageTitle';
import messages from './messages';

export default function NotFound() {
  return (
    <div>
      <article>
        <PageTitle style={{ display: 'inline-block' }}>
          <FormattedMessage {...messages.header} />
        </PageTitle>
      </article>
    </div>
  );
}
