import React from 'react';
import PageTitle from 'components/PageTitle';
import Card from 'components/ResultCard';
import styled from 'styled-components';
import AllocationResult from './AllocationResult';
import PenyuluhResult from './PenyuluhResult';
import ListPenyuluh from './ListPenyuluh';
import theme from '../../theme';
import { API_ROOT } from './api-config';

const Row = styled.div`
  margin: 0 -1em;
  margin-left: 0.3em;
  display: flex;
  min-height: 100%;
  padding: 0 1em;
  flex-direction: row;
  flex-wrap: wrap;
`;

const Judul = styled.h3`
  height: 2.2em;
  padding-top: 0.8em;
  margin-top: -0.5em;  
  margin-bottom: 0em;
`;

const Nama = styled.li`
  marginLeft: 0em;
  display: inline-block;
  text-align: center;
`;

const Nomor = styled.li`
  display: inline-block;
  text-align: center;
  padding-left: 2em;
  width: 5%;
`;

const Status = styled.li`
  text-align: center;
  display: inline-block;
  box-sizing: border-box;
  padding: 0.25em 2em;
  text-decoration: none;
  background: ${theme.lightGreen};
  border-radius: 10px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  margin-top: -1em;
  font-family: ${theme.darkGreen};
  font-size: 1em;
  border: 0.1em solid ${theme.darkGreen};
  color: ${theme.white};
  font-weight: bold;

  &:hover {
    background: transparent;
    color: ${theme.darkGreen};
  }
`;

/* eslint-disable */
export class Penyuluh extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flag: 0,
      branchId: 1,
      penyuluhId: 0,
      data: [],
      isFetch: 1,
      worker: "",
      status: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleRecommend = this.handleRecommend.bind(this);
    this.checkWorker = this.checkWorker.bind(this);
    this.recommend = this.recommend.bind(this);
  }

  recommend(branchId) {
    const currentClass = this;
    if (currentClass.state.isFetch === 1) {
      fetch(`${API_ROOT}/recommendation/${branchId}`)
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        currentClass.setState({
          worker: data['worker-status'],
          status: "Worker is starting...",
          isFetch: 0,
        });
      });
    }
  }

  checkWorker() {
    const currentClass = this;
    const workerId = this.state.worker;
    fetch(`${API_ROOT}/upload2/status/${workerId}`)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      currentClass.setState({
        status: data.status,
      })
      if (data.status !== "Task completed!") {
        currentClass.checkWorker();
      } else {
        currentClass.setState({
          data: data.result,
          flag: 3,
        })
      }
    });
  }

  handleChange(branchId) {
    this.recommend(branchId);
    this.setState({
      flag: 1,
      branchId: branchId,
    });
  }

  handleRecommend(penyuluhId) {
    this.setState({
      flag: 2,
      penyuluhId: penyuluhId,
    })
  }

  render() {
    if (this.state.flag === 0) {
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Pilih Branch</PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '1.5em', marginRight: '0em', width: '60%' }}>
            <Card>
              <Judul>
                <Nomor style={{ marginLeft: '-0.5em' }}>ID</Nomor>
                <Nama style={{ width: '40%', marginLeft: '1.1em' }}>Nama Branch</Nama>
              </Judul>
              <PenyuluhResult onChange={this.handleChange} />
            </Card>
          </Row>
        </div>
      );
    } else if (this.state.flag === 1) {
      this.checkWorker();
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>{this.state.status}</PageTitle>
        </div>
      );
    } else if (this.state.flag === 3) {
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Daftar Penyuluh</PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '1.5em', marginRight: '-5em', width: '40%' }}>
            <Card style={{ paddingRight: '-10em' }} >
              <Judul style={{ textAlign: 'center' }}>
                <Nama style={{ width: '50%' }}>BRANCH ID: {this.state.branchId}</Nama>
              </Judul>
              <ListPenyuluh onChange={this.handleRecommend} branchId={this.state.branchId} />
            </Card>
          </Row>
          <a href="/penyuluh"><Status style={{ margin: '1em 1em 1em 3.5em'}}>Back</Status></a>
        </div>
      );
    } else {
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Daftar Penyuluh</PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '1.5em', marginRight: '-5em', width: '33%' }}>
            <Card style={{ paddingRight: '-10em' }} >
              <Judul style={{ textAlign: 'center' }}>
                <Nama style={{ width: '50%' }}>BRANCH ID: {this.state.branchId}</Nama>
              </Judul>
              <ListPenyuluh onChange={this.handleRecommend} branchId={this.state.branchId} />
            </Card>
          </Row>
          <a href="/penyuluh"><Status style={{ margin: '1em 1em 1em 3.5em'}}>Back</Status></a>
          <AllocationResult branchId={this.state.branchId} penyuluhId={this.state.penyuluhId} flag={this.state.flag} data={this.state.data}/>
        </div>
      );
    }
  }
}

export default Penyuluh;
