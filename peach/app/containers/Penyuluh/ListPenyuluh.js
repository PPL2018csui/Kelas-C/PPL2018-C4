import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { API_ROOT } from './api-config';
import theme from '../../theme';

const Konten = styled.ul`
  height: 2em;
  padding-top: 0.65em;
  padding-left: 0em;
  margin-bottom: 0em;
  border-top: 1px solid ${theme.lineGrey};
`;

const Nomor = styled.li`
  display: inline-block;
  text-align: center;
  padding-left: 2em;
  width: 5%;
`;

const Nama = styled.li`
  marginLeft: 0em;
  display: inline-block;
  text-align: left;
  width: 16%;
`;

const Status = styled.li`
  float: right;
  margin-right: 1em;
  text-align: center;
  display: inline-block;
  box-sizing: border-box;
  padding: 0.25em 1.5em;
  text-decoration: none;
  background: ${theme.todoOrange};
  border-radius: 10px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-family: ${theme.todoOrange};
  font-size: 0.8em;
  border: 0.1em solid ${theme.todoOrange};
  color: ${theme.white};
  font-weight: bold;

  &:hover {
    background: transparent;
    color: ${theme.todoOrange};
  }
`;

/* eslint-disable */
export class ListPenyuluh extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      branchId: -1,
    };
  }

  componentDidMount() {
    const currentClass = this;
    const branchId = this.props.branchId;
    fetch(`${API_ROOT}/penyuluh?branch_id=${this.props.branchId}`)
    .then(function (response) { 
      return response.json();
    })
    .then(function (data) { 
      currentClass.setState({
        data: data, 
        branchId: branchId,
      });
    });
  }

  handleClick(e) {
    const penyuluhId = e.target.id;
    this.props.onChange(penyuluhId);
  }

  render() {
    const { result = [] } = this.state.data;
    while (result.length <= 0) {
      return (
        <div>
          <Konten>
            <Nomor></Nomor>
            <Nama>loading...</Nama>
          </Konten>
        </div>
      );
    }
    return (
      <div>
        {
          Object.keys(result).map((key, index) => (
            <Konten key={key}>
              <Nomor>{index + 1}.</Nomor><Nomor />
              <Nama>{result[key].first_name}</Nama>
              <Status id={result[key].penyuluh_id} onClick={(key) => this.handleClick(key)}>Lihat Rekomendasi</Status>
            </Konten>
          ))
        }
      </div>
    );
  }
}

ListPenyuluh.propTypes = {
  branchId: PropTypes.any,
};

export default ListPenyuluh;
