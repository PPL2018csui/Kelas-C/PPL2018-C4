import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import PageTitle from 'components/PageTitle';
import Card from 'components/ResultCard';
import theme from '../../theme';

const Row = styled.div`
  margin: 0 -1em;
  margin-left: 0.3em;
  display: flex;
  min-height: 100%;
  padding: 0 1em;
  flex-direction: row;
  flex-wrap: wrap;
`;

const Judul = styled.h3`
  height: 2.2em;
  padding-top: 0.8em;
  margin-top: -0.5em;  
  margin-bottom: 0em;
`;

const Konten = styled.ul`
  height: 2em;
  padding-top: 0.65em;
  padding-left: 0em;
  margin-bottom: 0em;
  border-top: 1px solid ${theme.lineGrey};
`;

const Nomor = styled.li`
  display: inline-block;
  text-align: center;
  padding-left: 1em;
  width: 5%;
`;

const KetuaArisan = styled.li`
  display: inline-block;
  padding-left: 1em;
  text-align: left;
  padding-left: 1em;
  width: 7.5%;
`;

const Tempat = styled.li`
  display: inline-block;
  text-align: center;
  width: 15%;
`;

const Kelurahan = styled.li`
  display: inline-block;
  text-align: center;
  width: 23%;
`;

const RT = styled.li`
  display: inline-block;
  text-align: center;
  width: 7.5%;
`;

/* eslint-disable */
class AllocationResult extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.flag === 2) {
      const py_id = this.props.penyuluhId;
      const result = this.props.data;
      while (result.length <= 0) {
        return (
          <div></div>
        );
      }
      return (
        <div>
          <PageTitle style={{ display: 'inline-block' }}>Rekomendasi Alokasi Hari Ini</PageTitle>
          <Row style={{ marginTop: '1em', marginLeft: '1.5em', marginRight: '0em'}}>
            <Card>
              <Judul>
                <Nomor>No.</Nomor>
                <KetuaArisan style={{ marginLeft: '1em'}}>KA</KetuaArisan>
                <Tempat style={{ marginLeft: '-1em'}}>Provinsi</Tempat>
                <Tempat>Kabupaten</Tempat>
                <Tempat style={{ width: '17.5%'}} >Kecamatan</Tempat>
                <Kelurahan>Kelurahan</Kelurahan>
                <RT>RT</RT>
                <RT>RW</RT>
              </Judul>
              <div>
                {
                  Object.keys(result[py_id]).map((key, index) => (
                    <Konten key={key}>
                      <Nomor>{index + 1}.</Nomor>
                      <KetuaArisan>{result[py_id][key].nama}</KetuaArisan>
                      <Tempat>{result[py_id][key].provinsi}</Tempat>
                      <Tempat>{result[py_id][key].kabupaten}</Tempat>
                      <Tempat style={{ width: '17.5%'}}>{result[py_id][key].kecamatan}</Tempat>
                      <Kelurahan>{result[py_id][key].kelurahan}</Kelurahan>
                      <RT>{result[py_id][key].rt}</RT>
                      <RT>{result[py_id][key].rw}</RT>
                    </Konten>
                  ))
                }
              </div>
            </Card>
          </Row>
        </div>
      );
    } else {
      return <div></div>;
    }
  }
}

AllocationResult.propTypes = {
  offset: PropTypes.number,
};

export default AllocationResult;
