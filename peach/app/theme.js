const theme = {
  white: '#FFFFFF',
  graphicGreen: '#83B692',
  graphicYellow: '#ffeb7f',
  graphicDGreen: '#3C787E',
  graphicLightGreen: '#83F692',
  graphicPurple: '#654F6F',
  graphicOrange: '#F78764',
  graphicGold: '#c69502',
  graphicBlue: '#56CCF2',
  graphicPink: '#f4d5bf',
  darkGreen: '#658F41',
  lightGreen: '#A0C03C',
  mainGrey: '#808080',
  groundGrey: '#F0F3F3',
  todoOrange: '#F2A30F',
  todoRed: '#E00B27',
  lineGrey: 'rgba(0, 0, 0, 0.2)',
  fontContent: '\'Montserrat\', sans-serif',
  fontMenu: '\'Lora\', serif',
  modalBG: 'rgba(255, 255, 255, 0) @ 100%',
};

export default theme;
