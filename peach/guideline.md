# Guidelines

Hello, my friends!
I know that repeating all the hex-code for so many component with the same color is kinda frustating, thus I put all the colors you need in one file with a readable name. AND This is the documentation for color and how to use all the component that has been made 🎉🎉🎉. I'll update it once in a while, stay tune! ;) 


## CSS

### Color in theme.js
You can find the file in app/theme.js, it should contain all the color you need, if not please contact me.
```javascript
const  theme  = {
	white: '#FFFFFF',
	graphicGreen: '#83B692', // Only use for graphic
	graphicDGreen: '#3C787E', // Only use for graphic
	graphicPurple: '#654F6F', // Only use for graphic
	graphicOrange: '#F78764', // Only use for graphic
	graphicYellow: '#F4D35E', // Only use for graphic
	darkGreen: '#658F41', // For any other green color
	lightGreen: '#A0C03C', // Menu green
	mainGrey: '#808080', // Text, icon, ornament
	groundGrey: '#F0F3F3', // Background
	todoOrange: '#F2A30F', // Only for TODO
	todoRed: '#E00B27', // Only for TODO
	lineGrey: 'rgba(0, 0, 0, 0.2)', // Divider
	fontContent: '"Montserrat", sans-serif', // Montserrat, default font
	fontMenu: '"Lora", serif', // Lora, title and menu font
	modalBG: 'rgba(255, 255, 255, 0) @ 100%', // Shadow
};

export  default  theme;
```

#### How to use
Just import theme.js to your javascript. Example (asuming that you already used to the folder structure), imagine that we code at file called Card.js in app/components, then:
```javascript
import { css } from  'styled-components';
import  theme  from  '../../theme';

const  cardStyles  =  css`
	min-height: 10em;
	color: ${theme.mainGrey};
	background: ${theme.white};
	border-radius: 10px;
	box-shadow: 2px 2px ${theme.lineGrey};
	font-family: ${theme.fontContent};
	font-weight: 300;
`;

export  default  cardStyles;
```

#### Warning
Do not copy the hex code to our file, please, it's going to hard to maintain when we want to change the color scheme. Thank you very much ❤️

## Components

It includes the common components we usually use, like button, card, and page title. You need to import the .js file if you want to use it.

```javascript
import  Header  from  'components/Header';
```

### Button `(app/components/Button)`

You got button and it definitely has something to do, either a submit form or routing the URL (for a while, there's only the routing function. What you need to do is specify the url like `href="/hello"` (pssst, it's a relative route so use `'/[route]'` and react will handle the server/local url), **and** the button name `<Button> Hello </Button>`

#### How to use
```javascript
import  Button  from  'components/Button';

...

export  default  function  App() {
	return (
		<Button href="/hello"> Hello </Button>
	);
}
```

### Card `(app/components/Card)`
There are to type of card:
1. Where it doesn't need a title, that we only want the empty card
2. When we want a title (that has the same design in every card.

And, thanks to react an all its props we doesn't need to specify to different thing, we just need to add a little attribute when we want to render title (`title="[Title]"`)

#### How to use
```javascript
import  Card  from  'components/Card';

...

export  default  function  App() {
	return (
		<div>
			<Card title="Hello"> // Delete the title = "" if you don't need it
				<p>LOREM IPSUM DOLOR SIT AMET</p>
			</Card>
		</div>
	);

}
```

### PageTitle `(app/components/PageTitle)`

The simplest thing, you only need to write the page title (obviously?)

```javascript
import  PageTitle  from  'components/PageTitle';

function  HelloWorld() {
	return (
		<PageTitle>Hello World</PageTitle> //Here 'Hello World' is the title
	);
}
```