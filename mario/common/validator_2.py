import pandas as pd
import sys

from error import (
    NoneValueError,
    InvalidFileFormatError,
    MissingColumnError
)

sys.path.append("..")

TABLE_COLUMN = {
    'penyuluh': [
        'Nama', 'Branch', 'Ter'
    ],
    'ketua_arisan': [
        'KA Name', 'Longitude', 'Latitude', 'Branch',
        'Provinsi', 'Kabupaten', 'Kecamatan', 'Kelurahan',
        'Signed_Up', 'Registered', 'Last_Visited', 'Visit_Count',
        'RT', 'RW'
    ]
}


def validate_2(py_file, ka_file):

    try:
        py = pd.read_csv(py_file)
    except UnicodeDecodeError as e:
        raise InvalidFileFormatError("branch")

    try:
        ka = pd.read_csv(ka_file)
    except UnicodeDecodeError as e:
        raise InvalidFileFormatError("ketua_arisan")

    if not set(TABLE_COLUMN['penyuluh']).issubset(set(py)):
        raise MissingColumnError("Branch")

    if not set(TABLE_COLUMN['ketua_arisan']).issubset(set(ka)):
        raise MissingColumnError("ketua_arisan")

    if str(py.isnull().any().any()) is 'True':
        raise NoneValueError('penyuluh')

    if str(ka.isnull().any().any()) is 'True':
        raise NoneValueError('ketua_arisan')

    return True
