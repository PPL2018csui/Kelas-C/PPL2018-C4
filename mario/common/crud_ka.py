from models import (
    KetuaArisan,
    Profile,
    db
)
from datetime import datetime as dt

MULTIPLIER = 100


def create_ka(ka_attr, p_attr):
    new_id = KetuaArisan.query.join(KetuaArisan.profile).order_by(
        Profile.ketua_arisan_id.desc()).first().id
    ka = KetuaArisan(
        id=int(new_id)+1,
        visit_count=ka_attr[0],
        latitude=ka_attr[1],
        longitude=ka_attr[2],
        is_registered=bool(ka_attr[3]),
        is_signed=bool(ka_attr[4]),
        is_dimas=bool(ka_attr[6]),
        is_golden_age=bool(ka_attr[7]),
        is_wmt=bool(ka_attr[8])
    )
    p = Profile(
        first_name=p_attr[0],
        phone_number=p_attr[1],
        role_id=5,
        ketua_arisan_id=ka.id
    )
    db.session.add(ka)
    db.session.add(p)
    db.session.commit()
    return [ka.id]


def read_ka(ka_id):
    try:
        ka = KetuaArisan.query.join(KetuaArisan.profile).filter(
            Profile.ketua_arisan_id == ka_id
        ).first()
        return ka
    except IndexError as e:
        raise e


def update_ka(ka_id, field, new_value):
    ka = read_ka(ka_id)
    print(ka_id, field, new_value)
    if(field == 'visit_count'):
        ka.visit_count = int(new_value)
    elif(field == 'is_registered'):
        if (new_value == "True" or new_value == "true"):
            ka.is_registered = True
        else:
            ka.is_registered = False
    elif(field == 'is_signed'):
        if (new_value == "True" or new_value == "true"):
            ka.is_signed = True
        else:
            ka.is_signed = False
    elif(field == 'last_visited'):
        ka.last_visited = dt.strptime(
            new_value,
            '%d/%m/%Y'
        )
    else:
        raise AttributeError
    db.session.commit()

    return [field, new_value, ka]


def delete_ka(ka_id):
    ka = read_ka(ka_id)
    db.session.delete(ka)
    db.session.commit()
    return [ka.id]
