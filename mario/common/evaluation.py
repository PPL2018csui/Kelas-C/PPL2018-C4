from error import (
    NotAllocatedError,
    RequiredUploadError
)
from models import (
    Penyuluh,
    Assignment,
    AssignmentRevision
)


def get_py_per_branch(b_id):
    data_per_branch = Assignment.query.join(
        AssignmentRevision,
        AssignmentRevision.id == Assignment.assignment_revision_id
    ).filter(
        AssignmentRevision.branch_id == b_id,
    )
    return data_per_branch


def get_each_py(p_id, b_data):
    data_per_py = b_data.filter(
        Assignment.penyuluh_id == p_id
    )
    return data_per_py


def error_per_date(p_data, the_date):
    data_per_date = p_data.filter(
        Assignment.visit_date == the_date
    ).all()
    ka_id = []
    error = 0
    len_data = len(data_per_date)
    for data in data_per_date:
        ka_id_tmp = data.ketua_arisan_id
        if ka_id_tmp not in ka_id:
            ka_id.append(ka_id_tmp)
        else:
            error += 1
    return [error, len_data]


def get_daily_task_evaluation_score(err, total):
    return float(err) / float(total) * 100


def evaluate_daily_task():
    check_py = Penyuluh.query.all()
    if len(check_py) <= 0:
        raise RequiredUploadError()

    branch_id_raw = AssignmentRevision.query.with_entities(
        AssignmentRevision.branch_id
    ).all()
    data_branch_id = set(branch_id_raw)
    if len(list(data_branch_id)) <= 0:
        raise NotAllocatedError()

    data_date_raw = Assignment.query.with_entities(
        Assignment.visit_date
    ).all()
    data_date = set(data_date_raw)

    daily_task_error = 0
    total_task = 0
    for br_id in data_branch_id:
        py_per_branch = get_py_per_branch(br_id[0])
        py_id = set(py_per_branch.with_entities(Assignment.penyuluh_id).all())
        for pyid in py_id:
            py_data = get_each_py(pyid[0], py_per_branch)
            for ddate in data_date:
                err_per_date = error_per_date(py_data, ddate[0])
                daily_task_error += err_per_date[0]
                total_task += err_per_date[1]

    return get_daily_task_evaluation_score(daily_task_error, total_task)
