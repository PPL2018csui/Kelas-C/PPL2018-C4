import pandas as pd
import sys

from error import (
    NoneValueError,
    InvalidFileFormatError,
    MissingColumnError
)

sys.path.append("..")

TABLE_COLUMN = {
    'branch':       ['id', 'name', 'address', 'bm_name',
                     'bm_phone', 'territory_ids', 'penyuluh_ids'],
    'penyuluh':     ['id', 'first_name', 'last_name', 'phone'],
    'territory':    ['id', 'province', 'city',
                     'subdistrict', 'village'],
    'ketua_arisan': ['first_name', 'last_name', 'address',
                     'territory_id', 'latitude', 'longitude',
                     'visited_count', 'last_visited', 'signed_up',
                     'registered']
}


def validate(file_branch, file_ketua_arisan, file_penyuluh, file_territory):
    try:
        branch = pd.read_csv(file_branch)
        branch_must_have = TABLE_COLUMN['branch']
    except UnicodeDecodeError as e:
        raise InvalidFileFormatError("branch")

    try:
        penyuluh = pd.read_csv(file_penyuluh)
        penyuluh_must_have = TABLE_COLUMN['penyuluh']
    except UnicodeDecodeError as e:
        raise InvalidFileFormatError("penyuluh")

    try:
        territory = pd.read_csv(file_territory)
        territory_must_have = TABLE_COLUMN['territory']
    except UnicodeDecodeError as e:
        raise InvalidFileFormatError("territory")

    try:
        ka = pd.read_csv(file_ketua_arisan)
        ka_must_have = TABLE_COLUMN['ketua_arisan']
    except UnicodeDecodeError as e:
        raise InvalidFileFormatError("ketua arisan")

    if not set(branch_must_have).issubset(set(branch)):
        raise MissingColumnError("branch")
    if not (set(penyuluh_must_have).issubset(set(penyuluh))):
        raise MissingColumnError("penyuluh")
    if not (set(territory_must_have).issubset(set(territory))):
        raise MissingColumnError("territory")
    if not (set(ka_must_have).issubset(set(ka))):
        raise MissingColumnError("ketua arisan")
    if str(ka.isnull().any().any()) is 'True':
        raise NoneValueError("ketua arisan")
    if str(branch.isnull().any().any()) is 'True':
        raise NoneValueError("branch")
    if str(penyuluh.isnull().any().any()) is 'True':
        raise NoneValueError("penyuluh")
    if str(territory.isnull().any().any()) is 'True':
        raise NoneValueError("territory")

    return True
