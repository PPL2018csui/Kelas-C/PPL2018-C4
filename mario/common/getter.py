from models import (
    Penyuluh,
    Profile,
    Branch
)
from error import (
    NoContentError,
    NotFoundError
)


def get_branch():
    b = Branch.query.with_entities(
        Branch.id,
        Branch.name,
        Branch.address
        ).all()
    if(len(b) <= 0):
        raise NoContentError
    return b


def get_penyuluh(branch_id):
    p = Penyuluh.query.join(Penyuluh.profile).filter(
        Profile.penyuluh_branch_id == branch_id
        ).with_entities(
            Profile.penyuluh_id,
            Profile.first_name,
            Profile.last_name).all()
    if(len(p) <= 0):
        raise NotFoundError
    return p
