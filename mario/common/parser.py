import pandas as pd
import json

from models import db
from datetime import datetime as dt
from error import DuplicateTerritoryError
from luigi import celery
from io import BytesIO

from models import (
    Profile,
    Territory,
    Branch,
    KetuaArisan,
    Penyuluh,
)


@celery.task(bind=True)
def parse(self, branch_file, ka_file, py_file, territory_file):

    self.update_state(state='PROGRESS',
                      meta={'current': 1,
                            'status': 'Parsing input data.'})

    branch_file = BytesIO(branch_file)
    ka_file = BytesIO(ka_file)
    py_file = BytesIO(py_file)
    territory_file = BytesIO(territory_file)

    branch = pd.read_csv(branch_file, dtype={'bm_phone': str})
    ka = pd.read_csv(ka_file, dtype={
        'latitude': str,
        'longitude': str
    })

    py = pd.read_csv(py_file, dtype={'phone': str})
    territory = pd.read_csv(territory_file)

    counter = 0
    for _, row in branch.iterrows():

        self.update_state(state='PROGRESS',
                          meta={'current': counter,
                                'status': 'Saving branch data into database.'})
        counter = counter+1

        territory_list = territory.loc[
            territory['id'].isin(
                json.loads(
                    row['territory_ids']
                )
            )
        ][['id', 'province', 'city', 'subdistrict', 'village']]
        territory_list = territory_list.to_dict('records')

        bm = Profile(
            first_name=row['bm_name'],
            phone_number=row['bm_phone'],
            role_id=3,
        )

        br = Branch(
            name=row['name'],
            address=row['address'],
            branch_manager=bm
        )

        for t in territory_list:

            ter = Territory.query.filter_by(
                code=t['id'],
            ).first()

            if ter is None:
                ter = Territory(
                    code=t['id'],
                    province=t['province'],
                    city=t['city'],
                    subdistrict=t['subdistrict'],
                    village=t['village'],
                )
            else:
                b_name = Branch.query.join(
                    Territory,
                    Territory.branch_id == Branch.id
                ).filter(
                    Branch.id == ter.branch_id
                ).first().name

                raise DuplicateTerritoryError(
                    br.name,
                    str((
                        t['province'],
                        t['city'],
                        t['subdistrict'],
                        t['village']
                    )),
                    b_name
                )

            br.territories.append(ter)

        py_list = py.loc[
            py['id'].isin(
                json.loads(
                    row['penyuluh_ids']
                )
            )
        ][['id', 'first_name', 'last_name', 'phone']]
        py_list = py_list.to_dict('records')

        for p in py_list:

            penyuluh = Penyuluh(
                workload=3
            )

            penyuluh_profile = Profile(
                first_name=p['first_name'],
                last_name=p['last_name'],
                phone_number=p['phone'],
                penyuluh=penyuluh,
                role_id=4
            )

            br.penyuluhs.append(penyuluh_profile)

        db.session.add(br)
    db.session.commit()

    counter = 0
    for _, row in ka.iterrows():

        self.update_state(
            state='PROGRESS',
            meta={'current': counter,
                  'status': 'Saving ketua_arisan data into database.'}
        )
        counter = counter+1

        ketua_arisan_profile = Profile(
            first_name=row['first_name'],
            last_name=row['last_name'],
            address=row['address'],
            role_id=5
        )

        ter = Territory.query.filter_by(
            code=row['territory_id']
        ).first()

        if ter is None:
            t = territory.loc[
                territory['id'] == row['territory_id']
            ][[
                'id',
                'province',
                'city',
                'subdistrict',
                'village'
            ]].to_dict('records')

            ter = Territory(
                code=t[0]['id'],
                province=t[0]['province'],
                city=t[0]['city'],
                subdistrict=t[0]['subdistrict'],
                village=t[0]['village']
            )
        ketua_arisan_profile.territory = ter

        ketua_arisan = KetuaArisan(
            latitude=row['latitude'],
            longitude=row['longitude'],
            visit_count=row['visited_count'],
            last_visited=dt.strptime(
                row['last_visited'],
                '%d/%m/%Y'
            ),
            is_signed=row['signed_up'],
            is_registered=row['registered'],
            profile=ketua_arisan_profile,
        )

        db.session.add(ketua_arisan)
    db.session.commit()

    return {'current': 100,
            'status': 'Task completed!',
            'result': 'Parse ok.'}
