import pandas as pd

from models import db
from luigi import celery
from io import BytesIO
from datetime import datetime as dt

from models import (
    Profile,
    Branch,
    KetuaArisan,
    Penyuluh,
    Territory,
    RukunTetangga
)

MULTIPLIER = 100


@celery.task(bind=True)
def parse_2(self, ka_file, py_file):
    self.update_state(state='PROGRESS',
                      meta={'current': 1,
                            'status': 'Parsing input data.'})

    ka_file = BytesIO(ka_file)
    py_file = BytesIO(py_file)

    ka = pd.read_csv(ka_file)
    py = pd.read_csv(py_file)

    for idx, row in ka.iterrows():

        self.update_state(state='PROGRESS',
                          meta={'current': idx,
                                'status': 'Saving ketua arisan '
                                          'data into database.'})

        ka_prof = Profile.query.filter_by(
            first_name=row['KA Name'],
            role_id=5
        ).first()

        if ka_prof is not None:
            continue

        territory = Territory.query.filter_by(
            province=row["Provinsi"],
            city=row["Kabupaten"],
            subdistrict=row["Kecamatan"],
            village=row["Kelurahan"]
        ).first()

        if territory is None:  # pragma: no cover

            br = Branch.query.filter_by(
                name=row['Kabupaten']
            ).first()
            territory = Territory(
                province=row["Provinsi"],
                city=row["Kabupaten"],
                subdistrict=row["Kecamatan"],
                village=row["Kelurahan"],
                branch_id=br.id
            )
            db.session.add(territory)

        rtrw = RukunTetangga.query.filter_by(
            rt=str(row['RT']),
            rw=str(row['RW'])
        ).first()

        if rtrw is None:  # pragma: no cover

            rtrw = RukunTetangga(
                rt=str(row['RT']),
                rw=str(row['RW']),
                territory_id=territory.id
            )

            db.session.add(rtrw)

        ka_profile = Profile(
            first_name=row['KA Name'],
            role_id=5,
            territory_id=territory.id,
            rtrw_id=rtrw.id
        )

        ketua_arisan = KetuaArisan(
            latitude=row['Latitude'],
            longitude=row['Longitude'],
            is_signed=row['Signed_Up'],
            is_registered=row['Registered'],
            last_visited=row['Last_Visited'],
            visit_count=row['Visit_Count'],
            profile=ka_profile
        )

        db.session.add(ketua_arisan)

    db.session.commit()

    for idx, row in py.iterrows():

        self.update_state(state='PROGRESS',
                          meta={'current': idx,
                                'status': 'Saving penyuluh '
                                          'data into database.'})

        py_prof = Profile.query.filter_by(
            first_name=row['Nama'],
            role_id=4
        ).first()

        if py_prof is not None:
            continue

        penyuluh = Penyuluh(
            workload=3
        )
        db.session.add(penyuluh)

        py_profile = Profile(
            first_name=row["Nama"],
            penyuluh=penyuluh,
            role_id=4
        )

        br = Branch.query.filter_by(
            name=row['Branch']
        ).first()

        py_profile.penyuluh_branch_id = br.id

        if row['Ter'] == 'Kosong':
            db.session.add(py_profile)
            continue

        territories = row['Ter'].split(';')
        for ter in territories:
            terr = tuple(ter[1:-1].split('::'))
            territory = Territory.query.filter_by(
                province=terr[0],
                city=terr[1],
                subdistrict=terr[2],
                village=terr[3]
            ).first()

            if territory is None:
                continue

            penyuluh.territories.append(territory)

        db.session.add(py_profile)

    db.session.commit()

    counter = 0
    ketua_arisan = KetuaArisan.query.join(KetuaArisan.profile)
    ketua_arisan = ketua_arisan.filter(Profile.role.has(name='KETUA_ARISAN'))
    ketua_arisan = ketua_arisan.all()
    for ka in ketua_arisan:
        self.update_state(
            state='PROGRESS',
            meta={'current': counter,
                  'status': 'Calculating rank for ketua_arisan.'}
        )
        rank_visit_count = -1 * ka.visit_count * MULTIPLIER
        if ka.last_visited:
            last_visited = ka.last_visited
        else:
            last_visited = dt.today() - dt.timedelta(84)
        delta_visit = dt.today() - last_visited
        rank_delta_visit = round(delta_visit.days * MULTIPLIER / 7)
        rank_singned_up = -1 * MULTIPLIER if ka.is_signed else 0
        rank_registered = -2 * MULTIPLIER if ka.is_registered else 0
        rank_dimas = 1 * MULTIPLIER if ka.is_dimas else 0
        rank_goldenage = 1 * MULTIPLIER if ka.is_golden_age else 0
        rank_wmt = 2 * MULTIPLIER if ka.is_wmt else 0
        rank = (rank_visit_count + rank_delta_visit +
                rank_singned_up + rank_registered +
                rank_dimas + rank_goldenage + rank_wmt)
        ka.rank = rank
        counter = counter + 1
    db.session.commit()

    return {'current': 100,
            'status': 'Task completed!',
            'result': 'Parse ok.'}
