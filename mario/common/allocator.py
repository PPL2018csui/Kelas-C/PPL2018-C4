import sys
import datetime

from models import db
from geopy.distance import vincenty
from luigi import celery

from models import (
    Profile,
    Branch,
    KetuaArisan,
    Penyuluh,
    Assignment,
    Status,
    AssignmentRevision,
    Territory,
    RukunTetangga
)

MULTIPLIER = 100


def set_rank():
    ketua_arisan = KetuaArisan.query.join(KetuaArisan.profile)
    ketua_arisan = ketua_arisan.filter(Profile.role.has(name='KETUA_ARISAN'))
    ketua_arisan = ketua_arisan.all()
    for ka in ketua_arisan:
        rank_visit_count = -1 * ka.visit_count * MULTIPLIER
        if ka.last_visited:
            last_visited = ka.last_visited
        else:
            last_visited = datetime.datetime.today() - datetime.timedelta(84)
        delta_visit = datetime.datetime.today() - last_visited
        rank_delta_visit = round(delta_visit.days * MULTIPLIER / 7)
        rank_singned_up = -1 * MULTIPLIER if ka.is_signed else 0
        rank_registered = -2 * MULTIPLIER if ka.is_registered else 0
        rank_dimas = 1 * MULTIPLIER if ka.is_dimas else 0
        rank_goldenage = 1 * MULTIPLIER if ka.is_golden_age else 0
        rank_wmt = 2 * MULTIPLIER if ka.is_wmt else 0
        rank = (rank_visit_count + rank_delta_visit +
                rank_singned_up + rank_registered +
                rank_dimas + rank_goldenage + rank_wmt)
        ka.rank = rank
    db.session.commit()


@celery.task(bind=True)
def recommendation(self, branch_id):  # pragma: no cover
    branch = Branch.query.filter_by(id=branch_id).first()

    self.update_state(
        state='PROGRESS',
        meta={
            'current': 1,
            'status': 'Getting all Penyuluhs in Branch {}'.format(branch.name)
        }
    )

    penyuluhs = branch.penyuluhs
    res = []

    list_of_py_territory = []
    for py in penyuluhs:
        py_id = Penyuluh.query.filter_by(profile=py).first().id
        territory_list = Territory.query.filter_by(penyuluh_id=py_id).all()

        if len(territory_list) == 0:
            res.append(
                {
                    'penyuluh': {
                        'profile_id': "-",
                        'py_id': py_id,
                        'nama': "-"
                    },
                    'kas': [{
                        'ka_id': "-",
                        'profile_id': "-",
                        'nama': "-",
                        'provinsi': "-",
                        'kabupaten': "-",
                        'kecamatan': "-",
                        'kelurahan': "-",
                        'rt': '-',
                        'rw': '-'
                    }]
                }
            )

        for territory in territory_list:
            list_of_py_territory.append(
                (py, territory)
            )

    self.update_state(
        state='PROGRESS',
        meta={
            'current': 1,
            'status': 'Allocating workload \
            for each Penyuluhs in Branch {}'.format(branch.name)
        }
    )

    for py_ter in list_of_py_territory:
        ketua_arisanss = KetuaArisan.query.join(KetuaArisan.profile)
        ketua_arisanss = ketua_arisanss.filter(
                Profile.role.has(name='KETUA_ARISAN')
            )
        ketua_arisanss = ketua_arisanss.filter_by(territory=py_ter[1])
        ketua_arisans = ketua_arisanss.order_by(KetuaArisan.rank.desc()).all()

        if len(ketua_arisans) == 1:
            ketuas = [ketua_arisans[0]]
        else:
            ketuas = []
            if len(ketua_arisans) == 0:
                continue
            ketuas.append(ketua_arisans[0])
            ketua_arisans.remove(ketua_arisans[0])

            while len(ketua_arisans) > 0:
                ketuas_tmp = []
                ka_now = ketuas[len(ketuas)-1]
                for i in range(len(ketua_arisans)):
                    distance = vincenty(
                        (
                            float(ka_now.latitude),
                            float(ka_now.longitude)
                        ),
                        (
                            float(ketua_arisans[i].latitude),
                            float(ketua_arisans[i].longitude)
                        )
                    ).km
                    ketuas_tmp.append(
                        (
                            i,
                            ketua_arisans[i].rank - (distance * 0.4)
                        )
                    )

                ketuas_tmp = sorted(
                    ketuas_tmp, key=lambda rank: float(rank[1])
                )
                ketuas.append(ketua_arisans[ketuas_tmp[0][0]])
                ketua_arisans.remove(ketua_arisans[ketuas_tmp[0][0]])

        data = {
            'penyuluh': {
                'profile_id': py_ter[0].id,
                'py_id': py_ter[0].penyuluh_id,
                'nama': py_ter[0].first_name
            },
            'kas': []
        }

        for ketua in ketuas:
            ka_profile = Profile.query.filter_by(
                ketua_arisan_id=ketua.id
            ).first()

            rtrw = RukunTetangga.query.filter_by(
                id=ka_profile.rtrw_id
            ).first()

            if rtrw is None:
                data['kas'].append(
                    {
                        'ka_id': ketua.id,
                        'profile_id': ka_profile.id,
                        'nama': ka_profile.first_name,
                        'provinsi': py_ter[1].province,
                        'kabupaten': py_ter[1].city,
                        'kecamatan': py_ter[1].subdistrict,
                        'kelurahan': py_ter[1].village,
                        'rt': 'unlisted',
                        'rw': 'unlisted'
                    }
                )

            else:
                data['kas'].append(
                    {
                        'ka_id': ketua.id,
                        'profile_id': ka_profile.id,
                        'nama': ka_profile.first_name,
                        'provinsi': py_ter[1].province,
                        'kabupaten': py_ter[1].city,
                        'kecamatan': py_ter[1].subdistrict,
                        'kelurahan': py_ter[1].village,
                        'rt': rtrw.rt,
                        'rw': rtrw.rw
                    }
                )

        res.append(data)

    self.update_state(
        state='PROGRESS',
        meta={
            'current': 1,
            'status': 'Constructing return value'
        }
    )

    newres = {}

    for datum in res:
        if datum['penyuluh']['py_id'] in newres:
            x = len(newres[datum['penyuluh']['py_id']])
            if x >= 15:
                continue
            elif x + len(datum['kas']) >= 15:
                newres[datum['penyuluh']['py_id']].extend(
                    datum['kas'][:(15-x):]
                )
            else:
                newres[datum['penyuluh']['py_id']].extend(datum['kas'])
        else:
            if len(datum['kas']) >= 15:
                newres[datum['penyuluh']['py_id']] = datum['kas'][:15:]
            else:
                newres[datum['penyuluh']['py_id']] = datum['kas']

    return {
        'current': 100,
        'status': 'Task completed!',
        'result': newres
    }


def allocate_kapys(ketua_arisans, penyuluhs, delta_range, date_start, ass_rev):

    if len(ketua_arisans) == 1:  # pragma: no cover
        ketuas = [ketua_arisans[0]]
    else:  # pragma: no cover
        ketuas = []
        ketuas.append(ketua_arisans[0])
        ketua_arisans.remove(ketua_arisans[0])

        while len(ketua_arisans) > 0:
            ketuas_tmp = []
            ka_now = ketuas[len(ketuas)-1]
            print(ka_now.id)
            for i in range(len(ketua_arisans)):
                distance = vincenty(
                    (
                        float(ka_now.latitude),
                        float(ka_now.longitude)
                    ),
                    (
                        float(ketua_arisans[i].latitude),
                        float(ketua_arisans[i].longitude)
                    )
                ).km
                ketuas_tmp.append(
                    (
                        i,
                        ketua_arisans[i].rank - (distance * 0.4)
                    )
                )

            ketuas_tmp = sorted(ketuas_tmp, key=lambda rank: float(rank[1]))
            ketuas.append(ketua_arisans[ketuas_tmp[0][0]])
            ketua_arisans.remove(ketua_arisans[ketuas_tmp[0][0]])

    for i in range(0, len(penyuluhs)):
        penyuluh = penyuluhs[i]
        index = i % len(ketuas)
        date = date_start
        if isinstance(penyuluh, Penyuluh):
            for day in range(0, delta_range):
                for workload in range(0, penyuluh.workload):
                    ketua_arisan = ketuas[index]
                    if isinstance(ketua_arisan, KetuaArisan):
                        status = Status.query.filter_by(name='TO_DO').first()
                        assignment = Assignment()
                        assignment.assignment_revision = ass_rev
                        assignment.penyuluh = penyuluh
                        assignment.ketua_arisan = ketua_arisan
                        assignment.visit_date = date + datetime.timedelta(day)
                        assignment.status = status
                        db.session.add(assignment)
                        index = (index + 1) % len(ketuas)
                    else:
                        raise TypeError('ketua_arisan is not an '
                                        'instance of KetuaArisan')
        else:
            raise TypeError('penyuluh is not an instance of Penyuluh')
        db.session.commit()


def allocate_branch(branch, delta_range, date_start):
    if isinstance(branch, Branch):
        assignment_revision = AssignmentRevision(branch=branch)
        territories = branch.territories
        ka = 0
        for territory in territories:
            for profile in territory.profiles:
                ka = ka + 1 if profile.role.name == 'KETUA_ARISAN' else ka

        py = Profile.query.filter_by(
            penyuluh_branch_id=branch.id,
            role_id=4
        ).all()
        index = 0
        for territory in territories:
            ka_kelurahan = KetuaArisan.query.join(KetuaArisan.profile)
            ka_kelurahan = ka_kelurahan.filter(
                Profile.role.has(name='KETUA_ARISAN')
            )
            ka_kelurahan = ka_kelurahan.filter_by(territory=territory)
            ka_kelurahan = ka_kelurahan.order_by(KetuaArisan.rank.desc()).all()
            if len(ka_kelurahan) == 0:  # pragma: no cover
                continue
            ka_ratio = len(ka_kelurahan) / (ka + sys.float_info.epsilon)
            py_ratio = round(ka_ratio * len(py))
            index = len(py) - 1 if index + py_ratio > len(py) else index
            py_kelurahan = py[index:(index + py_ratio - 1)]
            allocate_kapys(ka_kelurahan,
                           [py.penyuluh for py in py_kelurahan],
                           delta_range, date_start,
                           assignment_revision)
            index = index + py_ratio
            db.session.add(assignment_revision)
            db.session.commit()
    else:
        raise TypeError


@celery.task(bind=True)
def allocate(self, delta_range=7, date_start=datetime.datetime.now()):
    if delta_range < 0 or not isinstance(delta_range, int):
        raise ValueError('Wrong input on delta range.')

    self.update_state(state='PROGRESS',
                      meta={'current': 0,
                            'status': 'Start allocating data.'})

    self.update_state(state='PROGRESS',
                      meta={'current': 0,
                            'status': 'Getting all branches.'})
    branches = Branch.query.all()
    ketua_arisan = KetuaArisan.query.join(KetuaArisan.profile)
    ketua_arisan = ketua_arisan.filter(Profile.role.has(name='KETUA_ARISAN'))
    self.update_state(state='PROGRESS',
                      meta={'current': 0,
                            'status': 'Getting all ketua_arisan.'})
    ketua_arisan = ketua_arisan.all()
    '''
    counter = 0
    for ka in ketua_arisan:
        self.update_state(
            state='PROGRESS',
            meta={'current': counter,
                  'status': 'Calculating rank for ketua_arisan.'}
        )
        counter = counter + 1
        set_rank(ka)
    '''

    counter = 0
    for branch in branches:
        self.update_state(state='PROGRESS',
                          meta={'current': counter,
                                'status': 'Allocating branch assignments.'})
        counter = counter + 1
        allocate_branch(branch, delta_range, date_start)

    return {'current': 100,
            'status': 'Task completed!',
            'result': 'Allocate ok.'}


def set_vincenty():
    pass
