import os
from flask import Flask
from models import db

app = Flask(__name__)

REDIS_URL = "redis://{host}:{port}".format(
    host=os.getenv('REDIS_HOST', 'localhost'),
    port=os.getenv('REDIS_PORT', '6379')
)

app.config.from_object(os.environ['APP_SETTINGS'])

app.config.update(
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    CELERY_BROKER_URL=REDIS_URL,
    CELERY_RESULT_BACKEND=REDIS_URL,
    CELERY_ACCEPT_CONTENT=['pickle'],
    CELERY_TASK_SERIALIZER='pickle',
    CELERY_RESULT_SERIALIZER='pickle'
)

db.init_app(app)
