import click
import os

from flask import Flask
from models import db
from sqlalchemy import exc
from data_seed.branch import seed_branch
from data_seed.territory import seed_territory

from models import (
    Role,
    Status
)


@click.group(invoke_without_command=True)
@click.pass_context
def seed(ctx):
    app = Flask(__name__)
    app.config.from_object(os.environ['APP_SETTINGS'])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    with app.app_context():
        db.init_app(app)
        if ctx.invoked_subcommand is None:
            ctx.invoke(role)
            ctx.invoke(status)
            ctx.invoke(branch_territory)


@seed.command()
def role():
    roles = {
        1: 'SUPER_USER',
        2: 'ADMIN',
        3: 'BRANCH_MANAGER',
        4: 'PENYULUH',
        5: 'KETUA_ARISAN'
    }
    for key, val in roles.items():
        db.session.add(
            Role(
                id=key,
                name=val
            )
        )
    try:
        db.session.commit()
    except exc.IntegrityError:
        click.echo("Data role are already exist")
    except exc.InvalidRequestError:
        click.echo("Data role are already exist")


@seed.command()
def status():
    statuses = {
        1: 'TO_DO',
        2: 'DONE',
        3: 'CANCELLED'
    }

    for key, val in statuses.items():
        db.session.add(
            Status(
                id=key,
                name=val
            )
        )
    try:
        db.session.commit()
    except exc.IntegrityError:
        click.echo("Data status are already exist")
    except exc.InvalidRequestError:
        click.echo("Data status are already exist")


@seed.command()
def branch_territory():
    seed_branch()
    seed_territory()

    db.session.commit()
