import sys

from models import db

from models import (
    Profile,
    Branch,
    Territory
)

sys.path.append('..')


def seed_branch():

    bm = Profile(
        first_name="BM BOGOR",
        phone_number="81234567890",

        role_id=3
    )
    br = Branch(
            name="BOGOR",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM TASIKMALAYA",
        phone_number="81234567891",

        role_id=3
    )
    br = Branch(
            name="TASIKMALAYA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SEMARANG",
        phone_number="81234567892",

        role_id=3
    )
    br = Branch(
            name="SEMARANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM LEBAK",
        phone_number="81234567893",

        role_id=3
    )
    br = Branch(
            name="LEBAK",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM REMBANG",
        phone_number="81234567894",

        role_id=3
    )
    br = Branch(
            name="REMBANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BANYUWANGI",
        phone_number="81234567895",

        role_id=3
    )
    br = Branch(
            name="BANYUWANGI",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SIDOARJO",
        phone_number="81234567896",

        role_id=3
    )
    br = Branch(
            name="SIDOARJO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM KARANGANYAR",
        phone_number="81234567897",

        role_id=3
    )
    br = Branch(
            name="KARANGANYAR",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SERANG",
        phone_number="81234567898",

        role_id=3
    )
    br = Branch(
            name="SERANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BANYUMAS",
        phone_number="81234567899",

        role_id=3
    )
    br = Branch(
            name="BANYUMAS",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BATANG",
        phone_number="81234567900",

        role_id=3
    )
    br = Branch(
            name="BATANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM DEMAK",
        phone_number="81234567901",

        role_id=3
    )
    br = Branch(
            name="DEMAK",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM MAJALENGKA",
        phone_number="81234567902",

        role_id=3
    )
    br = Branch(
            name="MAJALENGKA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM GARUT",
        phone_number="81234567903",

        role_id=3
    )
    br = Branch(
            name="GARUT",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM INDRAMAYU",
        phone_number="81234567904",

        role_id=3
    )
    br = Branch(
            name="INDRAMAYU",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PEKALONGAN",
        phone_number="81234567905",

        role_id=3
    )
    br = Branch(
            name="PEKALONGAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SUKOHARJO",
        phone_number="81234567906",

        role_id=3
    )
    br = Branch(
            name="SUKOHARJO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM DEPOK",
        phone_number="81234567907",

        role_id=3
    )
    br = Branch(
            name="DEPOK",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SUKABUMI",
        phone_number="81234567908",

        role_id=3
    )
    br = Branch(
            name="SUKABUMI",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM MADIUN",
        phone_number="81234567909",

        role_id=3
    )
    br = Branch(
            name="MADIUN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM TULUNGAGUNG",
        phone_number="81234567910",

        role_id=3
    )
    br = Branch(
            name="TULUNGAGUNG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM JAKARTA TIMUR",
        phone_number="81234567911",

        role_id=3
    )
    br = Branch(
            name="JAKARTA TIMUR",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BANDUNG BARAT",
        phone_number="81234567912",

        role_id=3
    )
    br = Branch(
            name="BANDUNG BARAT",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SUMEDANG",
        phone_number="81234567913",

        role_id=3
    )
    br = Branch(
            name="SUMEDANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM CIREBON",
        phone_number="81234567914",

        role_id=3
    )
    br = Branch(
            name="CIREBON",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BANDUNG",
        phone_number="81234567915",

        role_id=3
    )
    br = Branch(
            name="BANDUNG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM KLATEN",
        phone_number="81234567916",

        role_id=3
    )
    br = Branch(
            name="KLATEN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM KUDUS",
        phone_number="81234567917",

        role_id=3
    )
    br = Branch(
            name="KUDUS",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM TEGAL",
        phone_number="81234567918",

        role_id=3
    )
    br = Branch(
            name="TEGAL",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM JAKARTA SELATAN",
        phone_number="81234567919",

        role_id=3
    )
    br = Branch(
            name="JAKARTA SELATAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BEKASI",
        phone_number="81234567920",

        role_id=3
    )
    br = Branch(
            name="BEKASI",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM CILACAP",
        phone_number="81234567921",

        role_id=3
    )
    br = Branch(
            name="CILACAP",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PURWOREJO",
        phone_number="81234567922",

        role_id=3
    )
    br = Branch(
            name="PURWOREJO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM MAGELANG",
        phone_number="81234567923",

        role_id=3
    )
    br = Branch(
            name="MAGELANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM TANGERANG",
        phone_number="81234567924",

        role_id=3
    )
    br = Branch(
            name="TANGERANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SLEMAN",
        phone_number="81234567925",

        role_id=3
    )
    br = Branch(
            name="SLEMAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM MAGETAN",
        phone_number="81234567926",

        role_id=3
    )
    br = Branch(
            name="MAGETAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM GRESIK",
        phone_number="81234567927",

        role_id=3
    )
    br = Branch(
            name="GRESIK",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BANGKALAN",
        phone_number="81234567928",

        role_id=3
    )
    br = Branch(
            name="BANGKALAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PANDEGLANG",
        phone_number="81234567929",

        role_id=3
    )
    br = Branch(
            name="PANDEGLANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SURABAYA",
        phone_number="81234567930",

        role_id=3
    )
    br = Branch(
            name="SURABAYA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM LUMAJANG",
        phone_number="81234567931",

        role_id=3
    )
    br = Branch(
            name="LUMAJANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM MALANG",
        phone_number="81234567932",

        role_id=3
    )
    br = Branch(
            name="MALANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM JAKARTA PUSAT",
        phone_number="81234567933",

        role_id=3
    )
    br = Branch(
            name="JAKARTA PUSAT",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM JAKARTA UTARA",
        phone_number="81234567934",

        role_id=3
    )
    br = Branch(
            name="JAKARTA UTARA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM TANGERANG SELATAN",
        phone_number="81234567935",

        role_id=3
    )
    br = Branch(
            name="TANGERANG SELATAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM TANGERANG SELATAN (BANTEN)",
        phone_number="81234567936",

        role_id=3
    )
    br = Branch(
            name="TANGERANG SELATAN (BANTEN)",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM TANGERANG (BANTEN)",
        phone_number="81234567937",

        role_id=3
    )
    br = Branch(
            name="TANGERANG (BANTEN)",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SERANG (BANTEN)",
        phone_number="81234567938",

        role_id=3
    )
    br = Branch(
            name="SERANG (BANTEN)",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM CILEGON",
        phone_number="81234567939",

        role_id=3
    )
    br = Branch(
            name="CILEGON",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM CILEGON (BANTEN)",
        phone_number="81234567940",

        role_id=3
    )
    br = Branch(
            name="CILEGON (BANTEN)",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PANDEGLANG (BANTEN)",
        phone_number="81234567941",

        role_id=3
    )
    br = Branch(
            name="PANDEGLANG (BANTEN)",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM CIMAHI",
        phone_number="81234567942",

        role_id=3
    )
    br = Branch(
            name="CIMAHI",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM CIAMIS",
        phone_number="81234567943",

        role_id=3
    )
    br = Branch(
            name="CIAMIS",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SALATIGA",
        phone_number="81234567944",

        role_id=3
    )
    br = Branch(
            name="SALATIGA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BOYOLALI",
        phone_number="81234567945",

        role_id=3
    )
    br = Branch(
            name="BOYOLALI",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BANTUL",
        phone_number="81234567946",

        role_id=3
    )
    br = Branch(
            name="BANTUL",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM YOGYAKARTA",
        phone_number="81234567947",

        role_id=3
    )
    br = Branch(
            name="YOGYAKARTA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM GUNUNG KIDUL",
        phone_number="81234567948",

        role_id=3
    )
    br = Branch(
            name="GUNUNG KIDUL",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM LEBAK (BANTEN)",
        phone_number="81234567949",

        role_id=3
    )
    br = Branch(
            name="LEBAK (BANTEN)",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM CIANJUR",
        phone_number="81234567950",

        role_id=3
    )
    br = Branch(
            name="CIANJUR",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM KENDAL",
        phone_number="81234567951",

        role_id=3
    )
    br = Branch(
            name="KENDAL",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PATI",
        phone_number="81234567952",

        role_id=3
    )
    br = Branch(
            name="PATI",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM JEPARA",
        phone_number="81234567953",

        role_id=3
    )
    br = Branch(
            name="JEPARA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM TEMANGGUNG",
        phone_number="81234567954",

        role_id=3
    )
    br = Branch(
            name="TEMANGGUNG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SURAKARTA",
        phone_number="81234567955",

        role_id=3
    )
    br = Branch(
            name="SURAKARTA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BREBES",
        phone_number="81234567956",

        role_id=3
    )
    br = Branch(
            name="BREBES",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PEMALANG",
        phone_number="81234567957",

        role_id=3
    )
    br = Branch(
            name="PEMALANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM JEMBER",
        phone_number="81234567958",

        role_id=3
    )
    br = Branch(
            name="JEMBER",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BATU",
        phone_number="81234567959",

        role_id=3
    )
    br = Branch(
            name="BATU",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SUMENEP",
        phone_number="81234567960",

        role_id=3
    )
    br = Branch(
            name="SUMENEP",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM KEDIRI",
        phone_number="81234567961",

        role_id=3
    )
    br = Branch(
            name="KEDIRI",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM NGANJUK",
        phone_number="81234567962",

        role_id=3
    )
    br = Branch(
            name="NGANJUK",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PONOROGO",
        phone_number="81234567963",

        role_id=3
    )
    br = Branch(
            name="PONOROGO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SITUBONDO",
        phone_number="81234567964",

        role_id=3
    )
    br = Branch(
            name="SITUBONDO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BANJARNEGARA",
        phone_number="81234567965",

        role_id=3
    )
    br = Branch(
            name="BANJARNEGARA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM JAKARTA BARAT",
        phone_number="81234567966",

        role_id=3
    )
    br = Branch(
            name="JAKARTA BARAT",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM JOMBANG",
        phone_number="81234567967",

        role_id=3
    )
    br = Branch(
            name="JOMBANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM WONOGIRI",
        phone_number="81234567968",

        role_id=3
    )
    br = Branch(
            name="WONOGIRI",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM WONOSOBO",
        phone_number="81234567969",

        role_id=3
    )
    br = Branch(
            name="WONOSOBO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SRAGEN",
        phone_number="81234567970",

        role_id=3
    )
    br = Branch(
            name="SRAGEN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SUBANG",
        phone_number="81234567971",

        role_id=3
    )
    br = Branch(
            name="SUBANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PAMEKASAN",
        phone_number="81234567972",

        role_id=3
    )
    br = Branch(
            name="PAMEKASAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BLITAR",
        phone_number="81234567973",

        role_id=3
    )
    br = Branch(
            name="BLITAR",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PASURUAN",
        phone_number="81234567974",

        role_id=3
    )
    br = Branch(
            name="PASURUAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PURWAKARTA",
        phone_number="81234567975",

        role_id=3
    )
    br = Branch(
            name="PURWAKARTA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PROBOLINGGO",
        phone_number="81234567976",

        role_id=3
    )
    br = Branch(
            name="PROBOLINGGO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM KARAWANG",
        phone_number="81234567977",

        role_id=3
    )
    br = Branch(
            name="KARAWANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM MOJOKERTO",
        phone_number="81234567978",

        role_id=3
    )
    br = Branch(
            name="MOJOKERTO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM KULON PROGO",
        phone_number="81234567979",

        role_id=3
    )
    br = Branch(
            name="KULON PROGO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PANGANDARAN",
        phone_number="81234567980",

        role_id=3
    )
    br = Branch(
            name="PANGANDARAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM LAMONGAN",
        phone_number="81234567981",

        role_id=3
    )
    br = Branch(
            name="LAMONGAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM GROBOGAN",
        phone_number="81234567982",

        role_id=3
    )
    br = Branch(
            name="GROBOGAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BONDOWOSO",
        phone_number="81234567983",

        role_id=3
    )
    br = Branch(
            name="BONDOWOSO",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM KUNINGAN",
        phone_number="81234567984",

        role_id=3
    )
    br = Branch(
            name="KUNINGAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM BLORA",
        phone_number="81234567985",

        role_id=3
    )
    br = Branch(
            name="BLORA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM TUBAN",
        phone_number="81234567986",

        role_id=3
    )
    br = Branch(
            name="TUBAN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM NGAWI",
        phone_number="81234567987",

        role_id=3
    )
    br = Branch(
            name="NGAWI",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM SAMPANG",
        phone_number="81234567988",

        role_id=3
    )
    br = Branch(
            name="SAMPANG",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM KEBUMEN",
        phone_number="81234567989",

        role_id=3
    )
    br = Branch(
            name="KEBUMEN",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)

    bm = Profile(
        first_name="BM PURBALINGGA",
        phone_number="81234567990",

        role_id=3
    )
    br = Branch(
            name="PURBALINGGA",
            branch_manager=bm,

    )
    ter = Territory.query.filter_by(city=br.name).all()
    br.territories = ter
    db.session.add(br)