from setup import app
from flask_restful import Api

from resources.api import (
    HelloWorld,
    Upload,
    Allocate,
    Workloads,
    UploadStatus,
    AllocateStatus,
    Upload_2,
    Evaluation,
    WorkloadsBranch,
    GetBranch,
    GetPenyuluh,
    SetVincenty,
    CreateKa,
    ReadKa,
    UpdateKa,
    Recommendation,
    Status,
    SetRank,
    UpdateKa2
)

api = Api(app)

api_path = '/api/v1'
api.add_resource(HelloWorld, '{}/hello'.format(api_path))
api.add_resource(Workloads, '{}/workloads'.format(api_path))
api.add_resource(Upload, '{}/upload'.format(api_path))
api.add_resource(Allocate, '{}/allocate'.format(api_path))
api.add_resource(UploadStatus, '{}/upload/status/<string:task_id>'.format(api_path))
api.add_resource(AllocateStatus, '{}/allocate/status/<string:task_id>'.format(api_path))
api.add_resource(Upload_2, '{}/upload2'.format(api_path))
api.add_resource(Evaluation, '{}/evaluation'.format(api_path))
api.add_resource(WorkloadsBranch, '{}/graph-workloads/<branch_id>'.format(api_path))
api.add_resource(GetBranch, '{}/branch'.format(api_path))
api.add_resource(GetPenyuluh, '{}/penyuluh'.format(api_path))
api.add_resource(SetVincenty, '{}/set_vincenty'.format(api_path))
api.add_resource(CreateKa, '{}/ketua_arisan'.format(api_path))
api.add_resource(ReadKa, '{}/ketua_arisan/<ka_id>'.format(api_path))
api.add_resource(UpdateKa, '{}/update_ketua_arisan'.format(api_path))
api.add_resource(UpdateKa2, '{}/update_ketua_arisan2'.format(api_path))
api.add_resource(Recommendation, '{}/recommendation/<branch_id>'.format(api_path))
api.add_resource(Status, '{}/upload2/status/<string:task_id>'.format(api_path))
api.add_resource(SetRank, '{}/setrank'.format(api_path))
