# *allocateam*'s Back-End Service
![coverage](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C4/badges/sit_uat/coverage.svg?job=test-mario)

This is the back-end service for allocateam, access on mario.allocateam.com

## Getting Started

### Prerequisites

- Python:3.6
- Pip:9.0.1
- Postgresql:Latest
- Docker:Latest

### Installing

Login to postgresql:

```
psql -U postgres
```

Create database allocateam & allocateam_test:

```
CREATE DATABASE allocateam;
CREATE DATABASE allocateam_test;
```

Installing python requirements:

```
pip install requiremets.txt
```

Create .env (please edit to match your configuration):

```
cp env.sample .env
```

Source environment variables:

```
source .env
```

Upgrade database migration:

```
flask db upgrade
```

Seed database:

```
flask seed
```

Start database on 0.0.0.0 port 5000:

```
flask run -h 0.0.0.0 -p 5000
```

## Running the tests

Run the test:

```
coverage run tests.py
```

Coverage report:

```
coverage report -m
```

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Containerize the file using Dockerfile:

```
docker build -t ${image_name}
    --build-arg SECRET_KEY=${secret_key}
    --build-arg FLASK_APP=${flask_app}
    --build-arg APP_SETTINGS=${app_settings}
    --build-arg DATABASE_URL=${database_url}
    ./mario
```

Run the container (Docker container will expose port 5000):

```
docker run -d -p ${target_port}:5000 ${image_name}
```

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration