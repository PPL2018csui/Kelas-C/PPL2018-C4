from celery import Celery
from setup import app


celery = Celery('luigi',
                backend=app.config['CELERY_RESULT_BACKEND'],
                broker=app.config['CELERY_BROKER_URL'],
                include=['common.allocator',
                         'common.parser',
                         'common.parser_2'
                         ])

celery.conf.update(app.config)
task_base = celery.Task


class ContextTask(task_base):
    abstract = True

    def __call__(self, *args, **kwargs):
        with app.app_context():
            return task_base.__call__(self, *args, **kwargs)


celery.Task = ContextTask
