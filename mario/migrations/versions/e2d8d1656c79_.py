"""empty message

Revision ID: e2d8d1656c79
Revises: 4194dc7ab409
Create Date: 2018-05-17 04:02:10.670636

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e2d8d1656c79'
down_revision = '4194dc7ab409'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('ketua_arisan', sa.Column('rt', sa.Integer(), nullable=True))
    op.add_column('ketua_arisan', sa.Column('rw', sa.Integer(), nullable=True))


def downgrade():
    op.drop_column('ketua_arisan', 'rt')
    op.drop_column('ketua_arisan', 'rw')
