"""empty message

Revision ID: 97e0b3afbe68
Revises: 1cd1eb80731e
Create Date: 2018-05-15 21:25:32.886745

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '97e0b3afbe68'
down_revision = '1cd1eb80731e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('rukun_tetangga',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('territory_id', sa.Integer(), nullable=True),
    sa.Column('rw', sa.Integer(), nullable=False),
    sa.Column('rt', sa.String(length=30), nullable=False),
    sa.Column('latitude', sa.String(length=20), nullable=True),
    sa.Column('longitude', sa.String(length=20), nullable=True),
    sa.ForeignKeyConstraint(['territory_id'], ['territory.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.add_column('profile', sa.Column('rtrw_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'profile', 'rukun_tetangga', ['rtrw_id'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'profile', type_='foreignkey')
    op.drop_column('profile', 'rtrw_id')
    op.drop_table('rukun_tetangga')
    # ### end Alembic commands ###
