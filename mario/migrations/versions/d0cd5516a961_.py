"""empty message

Revision ID: d0cd5516a961
Revises: f5710800da89
Create Date: 2018-03-28 22:56:43.324098

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd0cd5516a961'
down_revision = 'f5710800da89'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('ketua_arisan', sa.Column('last_visited', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('ketua_arisan', 'last_visited')
    # ### end Alembic commands ###
