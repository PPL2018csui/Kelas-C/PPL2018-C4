from flask import jsonify
from flask_restful import Resource, reqparse
from flask import Response, request
from sqlalchemy import Date, cast
import math
import json
from common.validator import validate
from common.parser import parse
from common.validator_2 import validate_2
from common.parser_2 import parse_2
from common.allocator import allocate, set_vincenty, recommendation, set_rank
from common.evaluation import evaluate_daily_task
from datetime import datetime as dt

from datetime import datetime, timedelta
from error import (
    MissingFileError,
    OffsetReachedMaxpageError,
    BranchIDNotExistError
)
from common.getter import (
    get_branch,
    get_penyuluh
)
from models import (
    Assignment,
    AssignmentRevision,
    Profile,
    db
)
from common.crud_ka import (
    create_ka,
    read_ka,
    update_ka
    # delete_ka
)

MULTIPLIER = 100


def getLimitOffsetBranch():
    parser = reqparse.RequestParser()

    parser.add_argument('offset')
    parser.add_argument('limit')
    parser.add_argument('branch_id')
    args = parser.parse_args()

    offset = args.offset if args.offset is not None else 0
    limit = args.limit if args.limit is not None else 20
    branch_id = args.branch_id if args.branch_id is not None else -1
    limit = int(limit)

    return [offset, limit, branch_id]


def getLimitOffset():
    parser = reqparse.RequestParser()

    parser.add_argument('offset')
    parser.add_argument('limit')
    args = parser.parse_args()

    offset = args.offset if args.offset is not None else 0
    limit = args.limit if args.limit is not None else 20
    limit = int(limit)

    return [offset, limit]


def getAssigmentBranch(branch_id):
    ass = AssignmentRevision.query\
        .filter(AssignmentRevision.branch_id == branch_id)\
        .order_by(AssignmentRevision.id.desc())\
        .first()

    if not ass:
        raise BranchIDNotExistError

    branch_pys = Profile.query.with_entities(Profile.penyuluh_id)\
        .filter(Profile.penyuluh_branch_id == branch_id)

    py_name = {}

    for py_id in branch_pys:
        first_name = Profile.query.with_entities(Profile.first_name)\
            .filter(Profile.penyuluh_id == py_id).first()

        last_name = Profile.query.with_entities(Profile.last_name)\
            .filter(Profile.penyuluh_id == py_id).first()

        name = '{} {}'\
            .format(first_name.first_name, last_name.last_name)\
            if last_name.last_name is not None else first_name.first_name

        py_name[py_id] = name

    parser = reqparse.RequestParser()
    parser.add_argument('start_date')
    parser.add_argument('end_date')
    args = parser.parse_args()

    if args.start_date is not None \
            and datetime.strptime(args.start_date, '%d-%m-%Y') \
            >= datetime.now():
        start_date = datetime.strptime(args.start_date, '%d-%m-%Y')
    else:
        start_date = datetime.now()

    if args.end_date is not None \
            and datetime.strptime(args.end_date, '%d-%m-%Y') >= start_date:
        end_date = datetime.strptime(args.end_date, '%d-%m-%Y')
    else:
        end_date = start_date + timedelta(days=7)

    data = []
    d = start_date
    delta = timedelta(days=1)

    while d < end_date:
        date_str = d.strftime("%B %d, %Y")
        py_workloads = {'name': date_str}
        d_date = d.date()
        for py_id in branch_pys:
            workloads = Assignment.query\
                .filter(
                    Assignment.penyuluh_id == py_id,
                    cast(Assignment.visit_date, Date) == d_date
                ).count()
            name = '{}'.format(py_name.get(py_id))
            py_workloads[name] = workloads

        data.append(py_workloads)
        d = d + delta

    return data


def add_cors(response):
    response.headers.add(
        'Access-Control-Allow-Origin',
        '*'
    )


def run_exception(e):
    if hasattr(e, 'code'):
        response = Response(
            json.dumps({
                'http-status': {
                    'message': str(e),
                    'code': e.code
                }
            }), status=e.code
        )
    else:
        response = Response(
            json.dumps({
                'http-status': {
                    'message': str(e),
                    'code': 500
                }
            }), status=500
        )
    add_cors(response)
    return response


def update_rank(ka):
    rank_visit_count = -1 * ka.visit_count * MULTIPLIER
    if ka.last_visited:
        last_visited = ka.last_visited
    else:
        last_visited = dt.today() - dt.timedelta(84)
    delta_visit = dt.today() - last_visited
    rank_delta_visit = round(delta_visit.days * MULTIPLIER / 7)
    rank_singned_up = -1 * MULTIPLIER if ka.is_signed else 0
    rank_registered = -2 * MULTIPLIER if ka.is_registered else 0
    rank_dimas = 1 * MULTIPLIER if ka.is_dimas else 0
    rank_goldenage = 1 * MULTIPLIER if ka.is_golden_age else 0
    rank_wmt = 2 * MULTIPLIER if ka.is_wmt else 0
    rank = (rank_visit_count + rank_delta_visit +
            rank_singned_up + rank_registered +
            rank_dimas + rank_goldenage + rank_wmt)
    ka.rank = rank
    db.session.commit()


class HelloWorld(Resource):
    def get(self):
        response = jsonify({'hello': 'world'})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response


class Upload(Resource):
    def post(self):
        files = request.files

        try:
            try:
                validate(
                    files['branch'],
                    files['ketua_arisan'],
                    files['penyuluh'],
                    files['territory']
                )

                for key, _ in files.items():
                    files[key].seek(0)

                parser = parse.apply_async(
                    args=[
                        files['branch'].read(),
                        files['ketua_arisan'].read(),
                        files['penyuluh'].read(),
                        files['territory'].read()
                    ]
                )
            except KeyError:
                raise MissingFileError

            response = Response(
                json.dumps({
                    'worker-status': parser.id,
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    }
                }), status=200
            )

            response.headers.add('Access-Control-Allow-Origin', '*')
            return response

        except Exception as e:
            if hasattr(e, 'code'):
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': e.code
                        }
                    }), status=e.code
                )
            else:
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': 500
                        }
                    }), status=500
                )

            response.headers.add('Access-Control-Allow-Origin', '*')
            return response


class Allocate(Resource):

    def post(self):
        try:
            allocation = allocate.apply_async()

            response = Response(
                json.dumps({
                    'worker-status': allocation.id,
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    }
                }), status=200
            )

            response.headers.add('Access-Control-Allow-Origin', '*')
            return response

        except TypeError as e:

            response = Response(
                json.dumps({
                    'http-status': {
                        'message': str(e),
                        'code': 400
                    }
                }), status=400
            )

            response.headers.add('Access-Control-Allow-Origin', '*')
            return response

        except Exception as e:

            response = Response(
                json.dumps({
                    'http-status': {
                        'message': str(e),
                        'code': 500
                    }
                }), status=500
            )

            response.headers.add('Access-Control-Allow-Origin', '*')
            return response


class Workloads(Resource):
    def get(self):
        try:
            offset, limit, branch_id = getLimitOffsetBranch()

            if(branch_id == -1):
                ass = AssignmentRevision.query.order_by(
                    AssignmentRevision.branch_id, AssignmentRevision.id.desc()
                ).distinct(AssignmentRevision.branch_id)
            else:
                ass = AssignmentRevision.query.filter_by(
                    branch_id=branch_id
                ).distinct(AssignmentRevision.branch_id)

            ass_id = [i.id for i in ass]
            assignments = Assignment.query.filter(
                Assignment.assignment_revision_id.in_(ass_id)
            )

            count = assignments.count()
            max_page = math.floor(count/limit)
            assignments = assignments.order_by(
                Assignment.penyuluh_id, Assignment.visit_date
            ).limit(limit).offset(offset)

            data = [i.serialize for i in assignments]

            if not data:
                raise OffsetReachedMaxpageError
            else:
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': 'OK',
                            'code': 200
                        },
                        'meta': {
                            'offset': offset,
                            'limit': limit,
                            'max_page': max_page,
                            'branch_id': branch_id
                        },
                        'result':
                            data
                    }), status=200
                )

            response.headers.add(
                'Access-Control-Allow-Origin', 'http://allocateam.com'
            )
            return response

        except Exception as e:
            if hasattr(e, 'code'):
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': e.code
                        }
                    }), status=e.code
                )
            else:  # pragma: no cover
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': 500
                        }
                    }), status=500
                )

            response.headers.add(
                'Access-Control-Allow-Origin', 'http://allocateam.com'
            )
            return response


class WorkloadsBranch(Resource):
    def get(self, branch_id=1):

        try:
            offset, limit = getLimitOffset()
            result = getAssigmentBranch(branch_id)

            response = Response(
                json.dumps({
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    },
                    'result':
                        result
                }), status=200
            )

            response.headers.add(
                'Access-Control-Allow-Origin', 'http://allocateam.com'
            )
            return response

        except Exception as e:
            if hasattr(e, 'code'):
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': e.code
                        }
                    }), status=e.code
                )
            else:  # pragma: no cover
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': 500
                        }
                    }), status=500
                )

            response.headers.add(
                'Access-Control-Allow-Origin', 'http://allocateam.com'
            )
            return response


class GetBranch(Resource):
    def get(self):

        try:
            b = get_branch()
            data = [{
                'branch_id': i[0],
                'branch_name': i[1],
                'branch_address': i[2]} for i in b]

            response = Response(
                json.dumps({
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    },
                    'result':
                        data
                }), status=200
            )
            response.headers.add(
                'Access-Control-Allow-Origin', 'http://allocateam.com'
            )
            return response

        except Exception as e:
            if hasattr(e, 'code'):
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': e.code
                        }
                    }), status=e.code
                )
            else:  # pragma: no cover
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': 500
                        }
                    }), status=500
                )

            response.headers.add(
                'Access-Control-Allow-Origin', 'http://allocateam.com'
            )
            return response


class GetPenyuluh(Resource):
    def get(self):

        try:
            parser = reqparse.RequestParser()

            parser.add_argument('branch_id')
            args = parser.parse_args()

            branch_id = args.branch_id

            p = get_penyuluh(branch_id)
            data = [{
                'penyuluh_id': i[0],
                'first_name': i[1],
                'last_name': i[2]
                } for i in p]

            response = Response(
                json.dumps({
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    },
                    'result':
                        data
                }), status=200
            )
            response.headers.add(
                'Access-Control-Allow-Origin', 'http://allocateam.com'
            )
            return response

        except Exception as e:
            if hasattr(e, 'code'):
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': e.code
                        }
                    }), status=e.code
                )
            else:  # pragma: no cover
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': 500
                        }
                    }), status=500
                )

            response.headers.add(
                'Access-Control-Allow-Origin', 'http://allocateam.com'
            )
            return response


class AllocateStatus(Resource):  # pragma: no cover
    def get(self, task_id):
        task = allocate.AsyncResult(task_id)
        print(task.result)
        if task.state == 'PENDING':
            # job did not start yet
            response = {
                'state': task.state,
                'current': 0,
                'total': 1,
                'status': 'Pending...'
            }
        elif task.state != 'FAILURE':
            response = {
                'state': task.state,
                'current': task.info.get('current', 0),
                'total': task.info.get('total', 1),
                'status': task.info.get('status', '')
            }
            if 'result' in task.info:
                response['result'] = task.info['result']
        else:
            # something went wrong in the background job
            response = {
                'state': task.state,
                'current': 1,
                'total': 1,
                'status': str(task.info),  # this is the exception raised
            }
        return jsonify(response)


class UploadStatus(Resource):  # pragma: no cover
    def get(self, task_id):
        task = parse.AsyncResult(task_id)
        if task.state == 'PENDING':
            # job did not start yet
            response = {
                'state': task.state,
                'current': 0,
                'total': 1,
                'status': 'Pending...'
            }
        elif task.state != 'FAILURE':
            response = {
                'state': task.state,
                'current': task.info.get('current', 0),
                'total': task.info.get('total', 1),
                'status': task.info.get('status', '')
            }
            if 'result' in task.info:
                response['result'] = task.info['result']
        else:
            # something went wrong in the background job
            response = {
                'state': task.state,
                'current': 1,
                'total': 1,
                'status': str(task.info),  # this is the exception raised
            }
        return jsonify(response)


class Upload_2(Resource):
    def post(self):
        files = request.files

        try:
            try:
                validate_2(
                    py_file=files['penyuluh'],
                    ka_file=files['ketua_arisan']
                )

                for key, _ in files.items():
                    files[key].seek(0)

                parser = parse_2.apply_async(
                    args=[
                        files['ketua_arisan'].read(),
                        files['penyuluh'].read()
                    ]
                )
            except KeyError:
                raise MissingFileError

            response = Response(
                json.dumps({
                    'worker-status': parser.id,
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    }
                }), status=200
            )

            response.headers.add('Access-Control-Allow-Origin', '*')

        except Exception as e:
            if hasattr(e, 'code'):
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': e.code
                        }
                    }), status=e.code
                )
            else:
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': 500
                        }
                    }), status=500
                )

            response.headers.add('Access-Control-Allow-Origin', '*')
        return response


class Evaluation(Resource):
    def get(self):
        try:
            ev = evaluate_daily_task()
            response = Response(
                json.dumps({
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    },
                    'result': ev
                }), status=200
            )
            response.headers.add(
                'Access-Control-Allow-Origin',
                'http://allocateam.com'
            )
            return response
        except Exception as e:
            if hasattr(e, 'code'):
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': e.code
                        }
                    }), status=e.code
                )
            else:
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': 500
                        }
                    }), status=500
                )

            response.headers.add(
                'Access-Control-Allow-Origin',
                'http://allocateam.com'
            )
            return response


class SetVincenty(Resource):
    def post(self):
        try:
            set_vincenty()
            response = Response(
                json.dumps({
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    },
                }), status=200
            )
            response.headers.add(
                'Access-Control-Allow-Origin',
                'http://allocateam.com'
            )
            return response
        except Exception as e:
            if hasattr(e, 'code'):
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': e.code
                        }
                    }), status=e.code
                )
            else:
                response = Response(
                    json.dumps({
                        'http-status': {
                            'message': str(e),
                            'code': 500
                        }
                    }), status=500
                )

            response.headers.add(
                'Access-Control-Allow-Origin',
                'http://allocateam.com'
            )
            return response


class CreateKa(Resource):
    def post(self):
        try:
            ka_attr = []
            ka_attr.append(request.form['visit_count'])
            ka_attr.append(request.form['latitude'])
            ka_attr.append(request.form['longitude'])
            ka_attr.append(request.form['is_registered'])
            ka_attr.append(request.form['is_signed'])
            ka_attr.append(request.form['last_visited'])
            ka_attr.append(request.form['is_dimas'])
            ka_attr.append(request.form['is_golden_age'])
            ka_attr.append(request.form['is_wmt'])
            p_attr = []
            p_attr.append(request.form['first_name'])
            p_attr.append(request.form['phone_number'])

            res = create_ka(ka_attr, p_attr)
            response = Response(
                json.dumps({
                    'http-status': {
                        'message': 'Successfully created a new ketua arisan \
with id {}'.format(res[0]),
                        'code': 200
                    },
                }), status=200
            )
            add_cors(response)
            return response
        except Exception as e:
            run_exception(e)


class ReadKa(Resource):
    def get(self, ka_id):
        try:
            res = read_ka(ka_id)
            response = Response(
                json.dumps({
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    }, 'ketua_arisan': {
                        'id': res.id,
                        'visit_count': res.visit_count,
                        'latitude': res.latitude,
                        'longitude': res.longitude,
                        'is_registered': res.is_registered,
                        'is_signed': res.is_signed,
                        'last_visited': str(res.last_visited),
                        'rank': res.rank,
                        'is_dimas': res.is_dimas,
                        'is_golden_age': res.is_golden_age,
                        'is_wmt': res.is_wmt
                    },
                }), status=200
            )
            add_cors(response)
            return response
        except Exception as e:
            run_exception(e)


class UpdateKa(Resource):
    def post(self):
        try:
            ka_id = request.form['ka_id']
            field = request.form['field']
            new_value = request.form['new_value']

            res = update_ka(ka_id, field, new_value)
            response = Response(
                json.dumps({
                    'http-status': {
                        'message': '{} of KA with ID {} \
is now {}'.format(field, ka_id, res[1]),
                        'code': 200
                    },
                }), status=200
            )
            add_cors(response)
            return response
        except Exception as e:
            run_exception(e)


class UpdateKa2(Resource):
    def post(self):
        try:
            ka_id = request.form['ka_id']
            # field = request.form['field']
            is_signed = request.form['is_signed']
            is_registered = request.form['is_registered']
            last_visited = request.form['last_visited']
            visit_count = request.form['visit_count']

            print('DONE')
            print(is_signed, is_registered, last_visited, visit_count)

            res1 = update_ka(ka_id, 'is_signed', is_signed)
            res2 = update_ka(ka_id, 'is_registered', is_registered)
            res3 = update_ka(ka_id, 'last_visited', last_visited)
            res4 = update_ka(ka_id, 'visit_count', visit_count)
            update_rank(res1[2])
            print(res1, res2, res3, res4)
            msg1 = '{} of KA with ID {} is now {}'.format(
                'is_signed', ka_id, res1[1]
                ) if not None else 'Update is_signed failed'
            msg2 = '{} of KA with ID {} is now {}'.format(
                'is_registered', ka_id, res2[1]
                ) if not None else 'Update is_registered failed'
            msg3 = '{} of KA with ID {} is now {}'.format(
                'last_visited', ka_id, res3[1]
                ) if not None else 'Update last_visited failed'
            msg4 = '{} of KA with ID {} is now {}'.format(
                'visit_count', ka_id, res4[1]
                ) if not None else 'Update visit_count failed'

            response = Response(
                json.dumps({
                    'http-status': {
                        'message': '{} \n {} \n {} \n {}'.format(
                            msg1, msg2, msg3, msg4
                        ),
                        'code': 200
                    },
                }), status=200
            )
            add_cors(response)
            return response
        except Exception as e:
            run_exception(e)


class Recommendation(Resource):  # pragma: no cover
    def get(self, branch_id):
        recommender = recommendation.apply_async(
            args=[
                branch_id
            ]
        )

        response = Response(
            json.dumps({
                    'http-status': {
                        'message': 'OK',
                        'code': 200
                    },
                    'worker-status': recommender.id
                }
            ), status=200
        )
        response.headers.add(
                'Access-Control-Allow-Origin',
                '*'
            )
        return response


class Status(Resource):  # pragma: no cover
    def get(self, task_id):
        task = parse.AsyncResult(task_id)
        if task.state == 'PENDING':
            # job did not start yet
            response = {
                'state': task.state,
                'current': 0,
                'total': 1,
                'status': 'Pending...'
            }
        elif task.state != 'FAILURE':
            response = {
                'state': task.state,
                'current': task.info.get('current', 0),
                'total': task.info.get('total', 1),
                'status': task.info.get('status', '')
            }
            if 'result' in task.info:
                response['result'] = task.info['result']
        else:
            # something went wrong in the background job
            response = {
                'state': task.state,
                'current': 1,
                'total': 1,
                'status': str(task.info),  # this is the exception raised
            }
        return jsonify(response)


class SetRank(Resource):  # pragma: no cover
    def post(self):
        set_rank()

        return Response(
            json.dumps(
                {
                    'status': 'OK',
                    'code': 200
                }
            )
        )
