from urls import app
from setup import db
from flask_migrate import Migrate, MigrateCommand
from seed import seed
from flask_cors import CORS
from flask import redirect, url_for, session
from common.flask_oauth import OAuth
from urllib.request import Request, urlopen, URLError

migrate = Migrate(app, db)
app.cli.add_command(seed)

cors = CORS(
    app,
    resources={r"/api/*": {"origins": r"allocateam\.com"}}
)

GOOGLE_CLIENT_ID = '546690677569-rara5a0lic4k1a2m6alvasn24bi8io4v.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = 'r7U29VW6QFryyiv4DxXqgLe4'
REDIRECT_URI = '/oauth2callback'
oauth = OAuth()
google = oauth.remote_app('google',
                          base_url='https://www.google.com/accounts/',
                          authorize_url='https://accounts.google.com/o/oauth2/auth',
                          request_token_url=None,
                          request_token_params={'scope': 'https://www.googleapis.com/auth/userinfo.email',
                                                'response_type': 'code'},
                          access_token_url='https://accounts.google.com/o/oauth2/token',
                          access_token_method='POST',
                          access_token_params={'grant_type': 'authorization_code'},
                          consumer_key=GOOGLE_CLIENT_ID,
                          consumer_secret=GOOGLE_CLIENT_SECRET)


@app.cli.command()
def db():
    return MigrateCommand


@app.route('/')
def home():
    access_token = session.get('access_token')
    if access_token is None:
        return redirect(url_for('login'))

    access_token = access_token[0]
    headers = {'Authorization': 'OAuth '+access_token}
    req = Request('https://www.googleapis.com/oauth2/v1/userinfo',
                  None, headers)
    try:
        res = urlopen(req)
    except URLError:
        if URLError == 401:
            # Unauthorized - bad token
            session.pop('access_token', None)
            return redirect(url_for('login'))
        return redirect(url_for('login'))

    session['userinfo'] = res.read()
    return redirect(
                        "http://localhost:3000/",
                        code=302
                        )


@app.route('/login')
def login():
    access_token = session.get('access_token')
    if access_token is None:
        callback = url_for('authorized', _external=True)
        return google.authorize(callback=callback)
    else:
        return redirect(
                    "http://localhost:3000/",
                    code=302
                    )


@app.route('/logout')
def logout():
    session.clear()
    return redirect(
                    "http://localhost:3000/login",
                    code=302
                    )


@google.tokengetter
def get_access_token():
    return session.get('access_token')


@app.route(REDIRECT_URI)
@google.authorized_handler
def authorized(resp):
    access_token = resp['access_token']
    session['access_token'] = access_token, ''
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
