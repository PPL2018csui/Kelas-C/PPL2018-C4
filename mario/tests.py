import unittest
import mario
import datetime
import csv
import common.parser as p
import sys
import json
from common import allocator
from common.getter import (
    get_branch,
    get_penyuluh
)
from io import StringIO, BytesIO
from common import validator
from copy import deepcopy
from PIL import Image
from flask import Flask
from flask_testing import TestCase
from models import db
from sqlalchemy import exc
from unittest.mock import patch
from datetime import date, timedelta


from models import (
    Profile,
    Territory,
    Branch,
    KetuaArisan,
    Role,
    Revision,
    Penyuluh,
    Assignment,
    Status,
    AssignmentRevision
)
from error import (
    NoneValueError,
    InvalidFileFormatError,
    MissingColumnError,
    DuplicateTerritoryError
)

sys.path.append('.')


class TestLogin(unittest.TestCase):

    def setUp(self):
        self.app = mario.app.test_client()
        self.app.testing = True

    def test_login_success(self):
        response = self.app.get('/login')
        self.assertEqual(response.status_code, 302)


class TestLogout(unittest.TestCase):

    def setUp(self):
        self.app = mario.app.test_client()
        self.app.testing = True

    def test_logout(self):
        response = self.app.get('/logout')
        self.assertEqual(response.status_code, 302)


class TestApi(TestCase):

    __api_path = '/api/v1'

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()
        self.app = mario.app.test_client()
        self.app.testing = True

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_workloads_ok(self):
        revision = Revision()
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role,
            revision=revision
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            revision=revision,
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role,
            revision=revision
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get('{}/workloads'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['meta']['max_page'] == 0 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_workloads_no_territory(self):
        revision = Revision()
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role,
            revision=revision
        )

        role = Role()
        role.name = 'ketua_arisan'

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            role=role,
            revision=revision
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get('{}/workloads'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_workloads_offset(self):
        revision = Revision()
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role,
            revision=revision
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            revision=revision,
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role,
            revision=revision
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get(
            '{}/workloads?offset=0'.format(self.__api_path)
        )
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_workloads_offset_max(self):
        revision = Revision()
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role,
            revision=revision
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            revision=revision,
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role,
            revision=revision
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get(
            '{}/workloads?offset=2'.format(self.__api_path)
        )
        assert (
            response.status_code == 416
        )

    def test_workloads_limit(self):
        revision = Revision()
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role,
            revision=revision
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            revision=revision,
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role,
            revision=revision
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get('{}/workloads?limit=1'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_workloads_branch_id(self):
        revision = Revision()
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role,
            revision=revision
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            revision=revision,
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role,
            revision=revision
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        branch = Branch(
            id=1,
            name='Test Name',
            address='Test Address',
            revision=revision
        )

        assignment_revision = AssignmentRevision(
            branch_id=branch.id
        )

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(territory)
        db.session.add(branch, assignment)
        db.session.commit()

        response = self.app.get('{}/workloads?branch_id=1'.format(
            self.__api_path))
        assert (
            len(json.loads(
                response.data.decode())['meta']['branch_id']
                ) > 0
        )

    def test_hello(self):
        response = self.app.get('{}/hello'.format(self.__api_path))
        self.assertEqual(json.loads(response.get_data().decode()), {'hello':
                                                                    'world'})

    def test_hello_ok(self):
        response = self.app.get('{}/hello'.format(self.__api_path))
        self.assertEqual(response.status_code, 200)

    def test_upload_ok(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        with patch('resources.api.parse'):
            resp = self.app.post(
                '/api/v1/upload',
                data=files,
                content_type="multipart/form-data"
            )

        self.assertEqual(
            resp.status_code, 200
        )

    def test_upload_invalid_file(self):
        TABLE_COLUMN = {
            'penyuluh':    ['id', 'name', 'address', 'bm_name',
                            'bm_phone', 'territory_ids', 'penyuluh_ids'],
            'branch':     ['id', 'first_name', 'last_name', 'phone'],
            'territory':    ['id', 'province', 'city',
                             'subdistrict', 'village'],
            'ketua_arisan': ['first_name', 'last_name', 'address',
                             'territory_id', 'latitude', 'longitude',
                             'visited_count', 'last_visited', 'signed_up',
                             'registered']
        }

        file_names = list(TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        resp = self.app.post(
            '/api/v1/upload',
            data=files,
            content_type="multipart/form-data"
        )

        self.assertEqual(
            resp.status_code, 413
        )

    def test_upload_none_row(self):
        TABLE_COLUMN = {
            'penyuluh':    ['id', 'name', 'address', 'bm_name',
                            'bm_phone', 'territory_ids', 'penyuluh_ids'],
            'branch':     ['id', 'first_name', 'last_name', 'phone'],
            'territory':    ['id', 'province', 'city',
                             'subdistrict', 'village'],
            'ketua_arisan': ['first_name', 'last_name', 'address',
                             'territory_id', 'latitude', 'longitude',
                             'visited_count', 'last_visited', 'signed_up',
                             'registered']
        }

        file_names = list(TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       None, [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        resp = self.app.post(
            '/api/v1/upload',
            data=files,
            content_type="multipart/form-data"
        )

        self.assertEqual(
            resp.status_code, 411
        )

    def test_upload_missing_file(self):
        TABLE_COLUMN = {
            'penyuluh':     ['id', 'first_name', 'last_name', 'phone'],
            'territory':    ['id', 'province', 'city',
                             'subdistrict', 'village'],
            'ketua_arisan': ['first_name', 'last_name', 'address',
                             'territory_id', 'latitude', 'longitude',
                             'visited_count', 'last_visited', 'signed_up',
                             'registered']
        }

        file_names = list(TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        resp = self.app.post(
            '/api/v1/upload',
            data=files,
            content_type="multipart/form-data"
        )

        self.assertEqual(
            resp.status_code, 413
        )

    def test_upload_not_csv(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        file_names.pop(1)
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya']
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        files['ketua_arisan'] = (file_img, file_img.name)

        resp = self.app.post(
            '/api/v1/upload',
            data=files,
            content_type="multipart/form-data"
        )

        self.assertEqual(
            resp.status_code, 412
        )

    def test_upload_500_error(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        with patch('resources.api.parse', side_effect=AttributeError):
            resp = self.app.post(
                '/api/v1/upload',
                data=files,
                content_type="multipart/form-data"
            )

        self.assertEqual(
            resp.status_code, 500
        )

    def test_allocate_ok(self):

        with patch('resources.api.allocate', return_value=None):
            resp = self.app.post(
                '/api/v1/allocate'
            )

        self.assertAlmostEquals(
            resp.status_code, 200
        )

    def test_allocate_type_error(self):

        with patch('resources.api.allocate', side_effect=TypeError):
            resp = self.app.post(
                '/api/v1/allocate'
            )

        self.assertAlmostEquals(
            resp.status_code, 400
        )

    def test_allocate_500_error(self):

        with patch('resources.api.allocate', side_effect=AttributeError):
            resp = self.app.post(
                '/api/v1/allocate'
            )

        self.assertAlmostEquals(
            resp.status_code, 500
        )

    def test_get_branch(self):

        revision = Revision()
        branch = Branch(
            name='Test Branch',
            revision=revision
        )
        db.session.add(branch)
        db.session.commit()

        res = get_branch()
        self.assertEqual(
            len(res), 1
        )

    def test_get_penyuluh(self):

        revision = Revision()
        branch = Branch(
            id=1,
            name='Test Branch',
            revision=revision
        )
        penyuluh = Penyuluh(
            profile=Profile(
                first_name='Test First',
                role=Role(
                    id=4,
                    name='Test Name'
                ),
                revision=revision,
                penyuluh_branch_id=branch.id
            )
        )
        db.session.add(branch, penyuluh)
        db.session.commit()

        res = get_penyuluh(1)
        self.assertEqual(
            len(res), 1
        )

    def test_api_get_branch(self):

        revision = Revision()
        branch = Branch(
            name='Test Branch',
            revision=revision
        )
        db.session.add(branch)
        db.session.commit()

        response = self.app.get('{}/branch'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_api_get_penyuluh(self):

        revision = Revision()
        branch = Branch(
            id=1,
            name='Test Branch',
            revision=revision
        )
        penyuluh = Penyuluh(
            profile=Profile(
                first_name='Test First',
                role=Role(
                    id=4,
                    name='Test Name'
                ),
                revision=revision,
                penyuluh_branch_id=branch.id
            )
        )
        db.session.add(branch, penyuluh)
        db.session.commit()

        response = self.app.get(
            '{}/penyuluh?branch_id=1'.format(
                self.__api_path
            )
        )
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )


class TestApiWorkloadsBranch(TestCase):

    __api_path = '/api/v1'

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()
        self.app = mario.app.test_client()
        self.app.testing = True

        revision = Revision()

        branch = Branch(
            id=1,
            name='BRANCH',
            revision=revision
        )

        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            penyuluh_branch=branch,
            role=role,
            revision=revision
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            revision=revision,
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role,
            revision=revision
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision(
            branch=branch
        )

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_workloads_branch_ok(self):
        db.session.commit()

        response = self.app.get('{}/graph-workloads/1'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_workloads_start_end_date(self):
        db.session.commit()

        start_date = date.today() + timedelta(days=1)
        start_res = start_date.strftime("%B %d, %Y")
        start_date = start_date.strftime('%d-%m-%Y')
        end_date = date.today() + timedelta(days=7)
        end_date = end_date.strftime('%d-%m-%Y')

        response = self.app.get(
            '{}/graph-workloads/1?start_date={}&end_date={}'.format(
                self.__api_path, start_date, end_date
                )
            )

        resp = json.loads(response.data.decode())
        assert (
            response.status_code == 200 and
            resp['http-status']['message'] == "OK" and
            resp['result'][0]['name'] == start_res
        )

    def test_workloads_start_end_date_false(self):
        db.session.commit()

        dated = date.today() - timedelta(days=7)
        dated = dated.strftime('%d-%m-%Y')

        response = self.app.get(
            '{}/graph-workloads/1?start_date={}&end_date={}'.format(
                self.__api_path, dated, dated
                )
            )

        resp = json.loads(response.data.decode())
        dated = datetime.datetime.now().strftime("%B %d, %Y")
        assert (
            response.status_code == 200 and
            resp['http-status']['message'] == "OK" and
            resp['result'][0]['name'] == dated
        )

    def test_workloads_branch_no_branch(self):
        db.session.commit()

        response = self.app.get('{}/graph-workloads/2'.format(self.__api_path))

        assert (
            response.status_code == 433 and
            json.loads(response.data.decode())['http-status']['message']
            == "Assignment for this branch doesn't exist"
        )


class TestAllocator(TestCase):
    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_allocate(self):
        date = datetime.datetime.now()
        allocator.allocate(7, date)

    def test_set_rank_no_last_visited(self):
        ketua_arisan = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )
        allocator.set_rank(ketua_arisan)

        self.assertEqual(ketua_arisan.rank, 700)

    def test_set_rank(self):
        ketua_arisan = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True,
            last_visited=datetime.datetime.now()
        )
        allocator.set_rank(ketua_arisan)

    def test_set_rank_wrong_data(self):
        role = Role()
        with self.assertRaises(TypeError):
            allocator.set_rank(role)

    def test_allocate_branch(self):
        revision = Revision()

        ka = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )

        ka_2 = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )

        role_ka = Role(
            name='KETUA_ARISAN'
        )

        profile_ka = Profile(
            first_name='Wisnu',
            role=role_ka,
            ketua_arisan=ka,
            revision=revision
        )

        profile_ka_2 = Profile(
            first_name='Teguh',
            role=role_ka,
            ketua_arisan=ka_2,
            revision=revision
        )

        territory = Territory(
            code=1,
            province='DKI Jakarta',
            city='Jakarta Timur',
            subdistrict='Duren Sawit',
            village='Malaka Jaya',
            revision=revision,
            profiles=[profile_ka]
        )

        territory_2 = Territory(
            code=1,
            province='DKI Jakarta',
            city='Jakarta Timur',
            subdistrict='Duren Sawit',
            village='Malaka Sari',
            revision=revision,
            profiles=[profile_ka_2]
        )

        role_py = Role(
            name='PENYULUH'
        )

        py = Penyuluh(
            workload=3
        )

        profile_py = Profile(
            first_name='Wicaksono',
            role=role_py,
            penyuluh=py,
            revision=revision
        )

        branch = Branch(
            name='BranchLala',
            territories=[territory, territory_2],
            penyuluhs=[profile_py],
            revision=revision
        )

        date = datetime.datetime.now()
        allocator.allocate_branch(branch, 3, date)

    def test_allocate_branch_wrong_data(self):
        role = Role()
        date = datetime.datetime.now()
        with self.assertRaises(TypeError):
            allocator.allocate_branch(role, 3, date)

    def test_allocate_kapys(self):
        ass_rev = AssignmentRevision()

        ka = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )
        py = Penyuluh(
            workload=3
        )
        date = datetime.datetime.now()
        allocator.allocate_kapys([ka], [py], 3, date, ass_rev)

    def test_allocate_kapys_wrong_py_data(self):
        ass_rev = AssignmentRevision()
        ka = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )
        role = Role()
        date = datetime.datetime.now()
        with self.assertRaises(TypeError):
            allocator.allocate_kapys([ka], [role], 3, date, ass_rev)

    def test_allocate_kapys_wrong_ka_data(self):
        ass_rev = AssignmentRevision()
        role = Role()
        py = Penyuluh(
            workload=3
        )
        date = datetime.datetime.now()
        with self.assertRaises(TypeError):
            allocator.allocate_kapys([role], [py], 3, date, ass_rev)


class TestConstructData(unittest.TestCase):

    def setUp(self):
        self.app = mario.app.test_client()
        self.app.testing = True

    def test_construct_data_redirect(self):
        response = self.app.get('/constructdata')
        self.assertEqual(response.status_code, 302)

    def test_construct_data_redirecterror(self):
        response = self.app.get('/construtdata')
        self.assertEqual(response.status_code, 404)


class TestConstructData2(unittest.TestCase):

    def setUp(self):
        self.app = mario.app.test_client()
        self.app.testing = True

    def test_construct_data_redirect(self):
        response = self.app.get('/constructdata2')
        self.assertEqual(response.status_code, 302)

    def test_construct_data_redirecterror(self):
        response = self.app.get('/construtdata2')
        self.assertEqual(response.status_code, 404)


class TestParser(TestCase):

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

        roles = {
            1: 'SUPER_USER',
            2: 'ADMIN',
            3: 'BRANCH_MANAGER',
            4: 'PENYULUH',
            5: 'KETUA_ARISAN'
        }

        for key, val in roles.items():
            db.session.add(
                Role(
                    id=key,
                    name=val
                )
            )

        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_parse(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        files = []

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])

            if filename == 'ketua_arisan':
                writer.writerow(
                    [
                        1, 'Zahra', 'Ule', 'Jl. Bunga Rampai', 2,
                        "-6.198495", "106.837306", 3, "08/03/2018",
                        False, False
                    ]
                )

            if filename == 'territory':
                writer.writerow(
                    [
                        2, 'Jawa Barat', 'Depok',
                        'Cimanggis', 'Harjamukti'
                    ]
                )

            file.seek(0)
            files.append(file)

        p.parse(
            branch_file=files[0],
            ka_file=files[1],
            py_file=files[2],
            territory_file=files[3]
        )

        br = Branch.query.filter_by(
            revision_id=1,
            name='Tebet',
            address='Jl. Tebet Raya'
        ).first()

        ter = Territory.query.filter_by(
            code=1,
            revision_id=1,
            province='DKI Jakarta',
            city='Jakarta Timur',
            subdistrict='Duren Sawit',
            village='Malaka Jaya'
        ).first()

        py = Profile.query.join(
            Penyuluh,
            Profile.penyuluh_id == Penyuluh.id
        ).filter(
            Profile.first_name == 'Wicaksono',
            Profile.last_name == 'Wisnu',
            Profile.phone_number == '08111710107',
            Profile.role_id == 4
        )

        ka = Profile.query.join(
            KetuaArisan,
            Profile.ketua_arisan_id == KetuaArisan.id
        ).filter(
            Profile.first_name == 'Bthari',
            Profile.last_name == 'Smart',
            Profile.address == 'Jl. Bunga Rampai',
            KetuaArisan.latitude == '-6.198495',
            KetuaArisan.longitude == '106.837306'
        )

        self.assertIsNotNone(br)
        self.assertIsNotNone(ter)
        self.assertIsNotNone(py)
        self.assertIsNotNone(ka)

    def test_parse_duplicate_territory(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        files = []

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])

            if filename == 'branch':
                writer.writerow(
                    [
                        1, 'Depok', 'Jl. Depok', 'Rubi Rafi',
                        '08131655633', [1], [1]
                    ]
                )

            file.seek(0)
            files.append(file)

        with self.assertRaises(DuplicateTerritoryError):
            p.parse(
                branch_file=files[0],
                ka_file=files[1],
                py_file=files[2],
                territory_file=files[3]
            )


class TestDatabase(TestCase):

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_profile_ok(self):
        revision = Revision()

        role = Role()
        role.name = 'Super Admin'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role
        profile.revision = revision

        db.session.add(profile)
        db.session.commit()

        self.assertIn(profile, db.session)

    def test_profile_no_name(self):
        role = Role()
        role.name = 'Super Admin'

        profile = Profile()
        profile.role = role
        db.session.add(profile)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_profile_no_role(self):
        profile = Profile()
        profile.first_name = 'Wicaksono'

        db.session.add(profile)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_branch_ok(self):
        revision = Revision()

        branch = Branch()
        branch.name = 'Branch_A'
        branch.revision = revision
        db.session.add(branch)
        db.session.commit()

        self.assertIn(branch, db.session)

    def test_branch_no_name(self):
        branch = Branch()
        db.session.add(branch)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_territory_ok(self):
        revision = Revision()

        territory = Territory()
        territory.code = 1
        territory.province = 'DKI Jakarta'
        territory.city = 'Jakarta Timur'
        territory.subdistrict = 'Duren Sawit'
        territory.village = 'Malaka Jaya'
        territory.revision = revision
        db.session.add(territory)
        db.session.commit()

        self.assertIn(territory, db.session)

    def test_territory_no_province(self):
        territory = Territory()
        territory.city = 'Jakarta Timur'
        territory.subdistrict = 'Duren Sawit'
        territory.village = 'Malaka Jaya'
        db.session.add(territory)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_territory_no_city(self):
        territory = Territory()
        territory.province = 'DKI Jakarta'
        territory.subdistrict = 'Duren Sawit'
        territory.village = 'Malaka Jaya'
        db.session.add(territory)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_territory_no_subdistrict(self):
        territory = Territory()
        territory.province = 'DKI Jakarta'
        territory.city = 'Jakarta Timur'
        territory.village = 'Malaka Jaya'
        db.session.add(territory)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_territory_no_village(self):
        territory = Territory()
        territory.province = 'DKI Jakarta'
        territory.city = 'Jakarta Timur'
        territory.subdistrict = 'Duren Sawit'
        db.session.add(territory)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_role_ok(self):
        role = Role()
        role.name = 'Super Admin'
        db.session.add(role)
        db.session.commit()

        self.assertIn(role, db.session)

    def test_role_no_name(self):
        role = Role()
        db.session.add(role)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_ketua_arisan_ok(self):
        ketua_arisan = KetuaArisan()
        ketua_arisan.visit_count = 5
        db.session.add(ketua_arisan)
        db.session.commit()

        self.assertIn(ketua_arisan, db.session)

    def test_revision_ok(self):
        role = Role()
        role.name = 'Super Admin'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role

        branch = Branch()
        branch.name = 'Branch_A'

        territory = Territory()
        territory.code = 1
        territory.province = 'DKI Jakarta'
        territory.city = 'Jakarta Timur'
        territory.subdistrict = 'Duren Sawit'
        territory.village = 'Malaka Jaya'

        revision = Revision()
        revision.branches = branch
        revision.profiles = profile
        revision.territories = territory

        db.session.add(revision)
        db.session.commit()

        self.assertIn(revision, db.session)

    def test_profile_no_revision(self):
        role = Role()
        role.name = 'Super Admin'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role

        db.session.add(profile)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_branch_no_revision(self):
        branch = Branch()
        branch.name = 'Branch_A'
        db.session.add(branch)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_territory_no_revision(self):
        territory = Territory()
        territory.province = 'DKI Jakarta'
        territory.city = 'Jakarta Timur'
        territory.subdistrict = 'Duren Sawit'
        territory.village = 'Malaka Jaya'
        db.session.add(territory)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_penyuluh_ok(self):
        revision = Revision()

        role = Role()
        role.name = 'Penyuluh'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role
        profile.revision = revision

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        db.session.add(penyuluh)
        db.session.commit()

        self.assertIn(penyuluh, db.session)

    def test_assignment_ok(self):
        revision = Revision()

        role = Role()
        role.name = 'Penyuluh'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role
        profile.revision = revision

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        role = Role()
        role.name = 'Ketua Arisan'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role
        profile.revision = revision

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        assignment_revision = AssignmentRevision()

        assignment = Assignment()
        assignment.ketua_arisan = ketua_arisan
        assignment.penyuluh = penyuluh
        assignment.assignment_revision = assignment_revision

        db.session.add(assignment)
        db.session.commit()

        self.assertIn(assignment, db.session)

    def test_status_ok(self):
        status = Status()
        status.name = 'cancelled'

        db.session.add(status)
        db.session.commit()

        self.assertIn(status, db.session)

    def test_assignment_revision_ok(self):
        revision = Revision()

        branch = Branch()
        branch.name = 'Branch_A'
        branch.revision = revision

        assignment_revision = AssignmentRevision()
        assignment_revision.branch = branch

        db.session.add(assignment_revision)
        db.session.commit()


class TestValidator(unittest.TestCase):
    mock_data = {
        'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                   '08131651104', '[1]', '[1]'],
        'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
        'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                      'Duren Sawit', 'Malaka Jaya'],
        'ketua_arisan': [1, 'Bthari Smart', 1, 'Jl. Bunga Rampai',
                         '-6.198495', '106.837306', 3, '08/03/2018',
                         False, False]
    }

    def setUp(self):
        self.app = mario.app.test_client()
        self.app.testing = True

    def test_validator_ok(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = validator.TABLE_COLUMN
        mock_data = self.mock_data
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)
        self.assertTrue(validator.validate(files[0], files[1],
                                           files[2], files[3]))

    def test_validator_no_important_column_on_branch(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        table_column['branch'].pop(1)
        mock_data['branch'].pop(1)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_no_important_column_on_penyuluh(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data)
        files = []

        table_column['penyuluh'].pop(1)
        mock_data['penyuluh'].pop(1)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_no_important_column_on_territory(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data)
        files = []

        table_column['territory'].pop(0)
        mock_data['territory'].pop(0)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_no_important_column_on_ketua_arisan(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data)
        files = []

        table_column['ketua_arisan'].pop(1)
        mock_data['ketua_arisan'].pop(1)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_none_value_on_branch(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['branch'][1] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_branch_none_value_on_penyuluh(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['penyuluh'][1] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_branch_none_value_on_territory(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['territory'][1] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_branch_none_value_on_ketua_arisan(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['ketua_arisan'][2] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_branch_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[0] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_ketua_arisan_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[1] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_penyuluh_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[2] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_territory_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[3] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator.validate(files[0], files[1], files[2], files[3])


if __name__ == "__main__":
    unittest.main()
