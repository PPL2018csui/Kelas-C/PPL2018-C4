class DuplicateTerritoryError(Exception):
    def __init__(self, branch, territory, branch_handled):
        self.message = "Branch {} can't handle territory {}, \
            because it is already handled by {} branch".format(
            branch,
            territory,
            branch_handled
        )
        self.code = 410

    def __str__(self):
        return self.message


class NoneValueError(Exception):
    def __init__(self, file_name):
        self.message = "There's a none value in {} file".format(file_name)
        self.code = 411

    def __str__(self):
        return self.message


class InvalidFileFormatError(Exception):
    def __init__(self, file_name):
        self.message = "File {} has to be in csv format".format(file_name)
        self.code = 412

    def __str__(self):
        return self.message


class MissingColumnError(Exception):
    def __init__(self, file_name):
        self.message = "There's missing column in file {}".format(
            file_name
        )
        self.code = 413

    def __str__(self):
        return self.message


class MissingFileError(Exception):
    def __init__(self):
        self.message = "Some file(s) are missing"
        self.code = 414

    def __str__(self):
        return self.message


class OffsetReachedMaxpageError(Exception):
    def __init__(self):
        self.message = "Offset has reached max_page"
        self.code = 416

    def __str__(self):
        return self.message


class RequiredUploadError(Exception):

    def __init__(self):
        self.message = "Data Penyuluh & Ketua Arisan Required To Be Uploaded"
        self.code = 425

    def __str__(self):
        return self.message


class NotAllocatedError(Exception):

    def __init__(self):
        self.message = "Allocation Algorithm Not Yet Executed"
        self.code = 426

    def __str__(self):
        return self.message


class InvalidFieldError(Exception):

    def __init__(self):
        self.message = "Invalid Field to Update"
        self.code = 427

    def __str__(self):
        return self.message


class BranchIDNotExistError(Exception):
    def __init__(self):
        self.message = "Assignment for this branch doesn't exist"
        self.code = 433

    def __str__(self):
        return self.message


# HTTP Error Code


class NoContentError(Exception):

    def __init__(self):
        self.message = "Error No Content"
        self.code = 204

    def __str__(self):
        return self.message


class NotFoundError(Exception):

    def __init__(self):
        self.message = "Error Not Found"
        self.code = 404

    def __str__(self):
        return self.message
