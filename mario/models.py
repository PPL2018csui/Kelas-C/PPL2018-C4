from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class AssignmentRevision(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    branch_id = db.Column(db.Integer,
                          db.ForeignKey('branch.id'))
    assignments = db.relationship('Assignment',
                                  backref='assignment_revision',
                                  lazy=True,
                                  foreign_keys='Assignment.assignment'
                                               '_revision_id')


class Branch(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(15), nullable=False)
    address = db.Column(db.String(30))
    address_territory_id = db.Column(db.Integer,
                                     db.ForeignKey('territory.id',
                                                   use_alter=True,
                                                   name='fk_address'
                                                        '_territory_id'))
    branch_manager = db.relationship('Profile', backref='manager_branch',
                                     lazy=True, uselist=False,
                                     foreign_keys='Profile.manager_branch_id')
    territories = db.relationship('Territory', backref="branch",
                                  foreign_keys='Territory.branch_id')
    penyuluhs = db.relationship('Profile', backref="penyuluh_branch",
                                foreign_keys='Profile.penyuluh_branch_id')
    assignment_revisions = db.relationship('AssignmentRevision',
                                           backref='branch',
                                           lazy=True,
                                           foreign_keys='Assignment'
                                                        'Revision.branch_id')


class Profile(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(15), nullable=False)
    last_name = db.Column(db.String(15))
    phone_number = db.Column(db.String(15))
    address = db.Column(db.String(30))
    territory_id = db.Column(db.Integer, db.ForeignKey('territory.id'))
    rtrw_id = db.Column(db.Integer, db.ForeignKey('rukun_tetangga.id'))
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'),
                        nullable=False)
    ketua_arisan_id = db.Column(db.Integer, db.ForeignKey('ketua_arisan.id'))
    penyuluh_id = db.Column(db.Integer, db.ForeignKey('penyuluh.id'))
    manager_branch_id = db.Column(db.Integer,
                                  db.ForeignKey('branch.id',
                                                name='fk_manager_branch_id'))
    penyuluh_branch_id = db.Column(db.Integer,
                                   db.ForeignKey('branch.id',
                                                 name='fk_penyuluh_branch_id'))


class Territory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Integer, nullable=False)
    province = db.Column(db.String(20), nullable=False)
    city = db.Column(db.String(20), nullable=False)
    subdistrict = db.Column(db.String(20), nullable=False)
    village = db.Column(db.String(20), nullable=False)
    profiles = db.relationship('Profile', backref="territory", lazy=True)
    branch_address_territory = db.relationship('Branch',
                                               backref="address_territory",
                                               lazy=True, uselist=False,
                                               foreign_keys='Branch.address'
                                                            '_territory_id')
    branch_id = db.Column(db.Integer,
                          db.ForeignKey('branch.id',
                                        name='fk_branch_id'))
    penyuluh_id = db.Column(db.Integer,
                            db.ForeignKey('penyuluh.id',
                                          name='fk_penyuluh_id'))
    rtrws = db.relationship('RukunTetangga', backref="territory", lazy=True)


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    profiles = db.relationship('Profile', backref="role", lazy=True)


class KetuaArisan(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    visit_count = db.Column(db.Integer, default=0)
    is_signed = db.Column(db.Boolean, default=False)
    is_registered = db.Column(db.Boolean, default=False)
    latitude = db.Column(db.String(20))
    longitude = db.Column(db.String(20))
    last_visited = db.Column(db.DateTime)
    is_wmt = db.Column(db.Boolean, default=False)
    is_dimas = db.Column(db.Boolean, default=False)
    is_golden_age = db.Column(db.Boolean, default=False)
    rank = db.Column(db.Integer, default=0)
    profile = db.relationship('Profile', backref="ketua_arisan",
                              lazy=True, uselist=False)
    assignments = db.relationship('Assignment',
                                  lazy=True, backref='ketua_arisan')


class Penyuluh(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    workload = db.Column(db.Integer)
    profile = db.relationship('Profile', backref='penyuluh',
                              lazy=True, uselist=False)
    territories = db.relationship('Territory', backref="penyuluh",
                                  foreign_keys='Territory.penyuluh_id')
    assignments = db.relationship('Assignment',
                                  lazy=True, backref='penyuluh')


class Assignment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    status_id = db.Column(db.Integer,
                          db.ForeignKey('status.id'))
    ketua_arisan_id = db.Column(db.Integer,
                                db.ForeignKey('ketua_arisan.id'))
    penyuluh_id = db.Column(db.Integer,
                            db.ForeignKey('penyuluh.id'))
    visit_date = db.Column(db.DateTime)
    assignment_revision_id = db.Column(db.Integer,
                                       db.ForeignKey('assignment_revision.id'),
                                       nullable=False)

    @property
    def serialize(self):
        ka = self.ketua_arisan.profile
        py = self.penyuluh.profile
        resp = {
            'status': self.status.name if self.status is not None else 'todo',
            'alamat': '{}'.format(
                    ka.address if ka.address is not None else 'unlisted'
                ),
            'ketua_arisan': '{}'.format(ka.first_name),
            'date': self.visit_date.strftime('%d %B %Y'),
            'nama': '{} {}'.format(
                py.first_name,
                py.last_name if py.last_name is not None else ''
            ),
        }

        if ka.territory is not None:
            resp.update({
                'kelurahan': '{}'.format(
                    ka.territory.subdistrict
                ),
            })
        else:
            resp.update({
                'kelurahan': 'undefined'
            })

        return resp


class Status(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    assignments = db.relationship('Assignment', backref='status',
                                  lazy=True)


class RukunTetangga(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rw = db.Column(db.String(5), nullable=False)
    rt = db.Column(db.String(5), nullable=False)
    latitude = db.Column(db.String(20))
    longitude = db.Column(db.String(20))
    territory_id = db.Column(db.Integer, db.ForeignKey('territory.id'))
    profiles = db.relationship('Profile', backref="rukun_tetangga", lazy=True)
    as_rt_awals = db.relationship('Jarak', backref="awal",
                                  lazy=True, foreign_keys='Jarak.rt_awal_id')
    as_rt_tujuans = db.relationship('Jarak', backref="tujuan",
                                    lazy=True,
                                    foreign_keys='Jarak.rt_tujuan_id')


class Jarak(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rt_awal_id = db.Column(db.Integer,
                           db.ForeignKey('rukun_tetangga.id',
                                         name='fk_rt_awal_id'),
                           nullable=False)
    rt_tujuan_id = db.Column(db.Integer,
                             db.ForeignKey('rukun_tetangga.id',
                                           name='fk_rt_tujuan_id'),
                             nullable=False)
    jarak = db.Column(db.Integer, default=0)
