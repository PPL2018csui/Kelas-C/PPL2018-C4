import datetime
import unittest

from flask import Flask
from models import db
from common import allocator
from flask_testing import TestCase
from unittest.mock import patch

from models import (
    Profile,
    Territory,
    Branch,
    KetuaArisan,
    Role,
    Penyuluh,
    AssignmentRevision
)


class TestAllocator(TestCase):
    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    @unittest.skip
    def test_allocate(self):

        ka = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )

        ka_2 = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )

        role_ka = Role(
            id=5,
            name='KETUA_ARISAN'
        )

        profile_ka = Profile(
            first_name='Wisnu',
            role=role_ka,
            ketua_arisan=ka,
        )

        profile_ka_2 = Profile(
            first_name='Teguh',
            role=role_ka,
            ketua_arisan=ka_2,
        )

        territory = Territory(
            code=1,
            province='DKI Jakarta',
            city='Jakarta Timur',
            subdistrict='Duren Sawit',
            village='Malaka Jaya',
            profiles=[profile_ka]
        )

        territory_2 = Territory(
            code=1,
            province='DKI Jakarta',
            city='Jakarta Timur',
            subdistrict='Duren Sawit',
            village='Malaka Sari',
            profiles=[profile_ka_2]
        )

        role_py = Role(
            name='PENYULUH'
        )

        py = Penyuluh(
            workload=3
        )

        profile_py = Profile(
            first_name='Wicaksono',
            role=role_py,
            penyuluh=py,
        )

        branch = Branch(
            name='Branch One',
            territories=[territory, territory_2],
            penyuluhs=[profile_py],
        )

        db.session.add(branch)
        db.session.commit()
        with patch('common.allocator.allocate.update_state'):
            date = datetime.datetime.now()
            allocator.allocate(7, date)

    def test_allocate_negative_delta_range(self):
        with patch('common.allocator.allocate.update_state'):
            date = datetime.datetime.now() + datetime.timedelta(days=10)
            with self.assertRaises(ValueError):
                allocator.allocate(-7, date)

    def test_allocate_float_delta_range(self):
        with patch('common.allocator.allocate.update_state'):
            date = datetime.datetime.now() + datetime.timedelta(days=10)
            with self.assertRaises(ValueError):
                allocator.allocate(0.5, date)

    '''
    def test_set_success(self):
        ketua_arisan = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True,
            last_visited=datetime.datetime.now()
        )
        allocator.set_rank()
        self.assertEqual(ketua_arisan.rank, 700)
    '''

    def test_set_rank(self):
        allocator.set_rank()

    def test_set_rank_wrong_data(self):
        role = Role()
        with self.assertRaises(TypeError):
            allocator.set_rank(role)

    @unittest.skip
    def test_endpoint_rank(self):
        self.assertEqual(self.state, 'SUCCESS')

    def test_allocate_branch(self):

        ka = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )

        ka_2 = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )

        role_ka = Role(
            name='KETUA_ARISAN'
        )

        profile_ka = Profile(
            first_name='Wisnu',
            role=role_ka,
            ketua_arisan=ka,
        )

        profile_ka_2 = Profile(
            first_name='Teguh',
            role=role_ka,
            ketua_arisan=ka_2,
        )

        territory = Territory(
            code=1,
            province='DKI Jakarta',
            city='Jakarta Timur',
            subdistrict='Duren Sawit',
            village='Malaka Jaya',
            profiles=[profile_ka]
        )

        territory_2 = Territory(
            code=1,
            province='DKI Jakarta',
            city='Jakarta Timur',
            subdistrict='Duren Sawit',
            village='Malaka Sari',
            profiles=[profile_ka_2]
        )

        role_py = Role(
            name='PENYULUH'
        )

        py = Penyuluh(
            workload=3
        )

        profile_py = Profile(
            first_name='Wicaksono',
            role=role_py,
            penyuluh=py,
        )

        branch = Branch(
            name='BranchLala',
            territories=[territory, territory_2],
            penyuluhs=[profile_py],
        )

        db.session.add(branch)
        db.session.commit()

        date = datetime.datetime.now()
        allocator.allocate_branch(branch, 3, date)

    def test_allocate_branch_wrong_data(self):
        role = Role()
        date = datetime.datetime.now()
        with self.assertRaises(TypeError):
            allocator.allocate_branch(role, 3, date)

    def test_allocate_kapys(self):
        ass_rev = AssignmentRevision()

        ka = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )
        py = Penyuluh(
            workload=3
        )
        date = datetime.datetime.now()
        allocator.allocate_kapys([ka], [py], 3, date, ass_rev)

    def test_allocate_kapys_wrong_py_data(self):
        ass_rev = AssignmentRevision()
        ka = KetuaArisan(
            visit_count=2,
            is_signed=True,
            is_registered=True
        )
        role = Role()
        date = datetime.datetime.now()
        with self.assertRaises(TypeError):
            allocator.allocate_kapys([ka], [role], 3, date, ass_rev)

    def test_allocate_kapys_wrong_ka_data(self):
        ass_rev = AssignmentRevision()
        role = Role()
        py = Penyuluh(
            workload=3
        )
        date = datetime.datetime.now()
        with self.assertRaises(TypeError):
            allocator.allocate_kapys([role], [py], 3, date, ass_rev)
