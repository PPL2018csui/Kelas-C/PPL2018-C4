import unittest
import mario
import csv

from io import StringIO, BytesIO
from common import validator_2
from PIL import Image
from copy import deepcopy

from error import (
    NoneValueError,
    InvalidFileFormatError,
    MissingColumnError
)


class TestValidator2(unittest.TestCase):
    mock_data = {
        'ketua_arisan': [
            'KA1',
            '106.94012865',
            '-5.91442471',
            'BOGOR',
            'JAWA BARAT',
            'BOGOR',
            'CIJERUK',
            'CIBALUNG',
            True,
            True,
            '02/02/2018',
            7,
            '2',
            '3'
        ],
        'penyuluh': [
            'PY1',
            'BOGOR',
            '(JAWA BARAT::BOGOR::CIJERUK::CIBALUNG)'
        ]
    }

    def setUp(self):
        self.app = mario.app.test_client()
        self.app.testing = True

    def test_validator_ok(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator_2.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        self.assertTrue(
            validator_2.validate_2(
                ka_file=files[0],
                py_file=files[1]
            )
        )

    def test_validator_no_important_column_on_penyuluh(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator_2.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        table_column['penyuluh'].pop(1)
        mock_data['penyuluh'].pop(1)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator_2.validate_2(ka_file=files[0], py_file=files[1])

    def test_validator_no_important_column_on_ka(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator_2.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        table_column['ketua_arisan'].pop(1)
        mock_data['ketua_arisan'].pop(1)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator_2.validate_2(ka_file=files[0], py_file=files[1])

    def test_validator_none_value_on_penyuluh(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator_2.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['penyuluh'][1] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator_2.validate_2(
                ka_file=files[0],
                py_file=files[1]
            )

    def test_validator_none_value_on_ka(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator_2.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['ketua_arisan'][1] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator_2.validate_2(
                ka_file=files[0],
                py_file=files[1]
            )

    def test_validator_ka_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator_2.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[0] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator_2.validate_2(ka_file=files[0], py_file=files[1])

    def test_validator_py_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator_2.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[1] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator_2.validate_2(ka_file=files[0], py_file=files[1])
