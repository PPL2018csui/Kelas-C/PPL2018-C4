import datetime
import json
import mario

from flask_testing import TestCase
from models import db
from datetime import date, timedelta
from flask import Flask

from models import (
    Profile,
    Territory,
    Branch,
    KetuaArisan,
    Role,
    Penyuluh,
    Assignment,
    Status,
    AssignmentRevision
)


class TestApiWorkloadsBranch(TestCase):

    __api_path = '/api/v1'

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()
        self.app = mario.app.test_client()
        self.app.testing = True

        branch = Branch(
            id=1,
            name='BRANCH',
        )

        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            penyuluh_branch=branch,
            role=role
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision(
            branch=branch
        )

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_workloads_branch_ok(self):
        db.session.commit()

        response = self.app.get('{}/graph-workloads/1'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_workloads_start_end_date(self):
        db.session.commit()

        start_date = date.today() + timedelta(days=1)
        start_res = start_date.strftime("%B %d, %Y")
        start_date = start_date.strftime('%d-%m-%Y')
        end_date = date.today() + timedelta(days=7)
        end_date = end_date.strftime('%d-%m-%Y')

        response = self.app.get(
            '{}/graph-workloads/1?start_date={}&end_date={}'.format(
                self.__api_path, start_date, end_date
                )
            )

        resp = json.loads(response.data.decode())
        assert (
            response.status_code == 200 and
            resp['http-status']['message'] == "OK" and
            resp['result'][0]['name'] == start_res
        )

    def test_workloads_start_end_date_false(self):
        db.session.commit()

        dated = date.today() - timedelta(days=7)
        dated = dated.strftime('%d-%m-%Y')

        response = self.app.get(
            '{}/graph-workloads/1?start_date={}&end_date={}'.format(
                self.__api_path, dated, dated
                )
            )

        resp = json.loads(response.data.decode())
        dated = datetime.datetime.now().strftime("%B %d, %Y")
        assert (
            response.status_code == 200 and
            resp['http-status']['message'] == "OK" and
            resp['result'][0]['name'] == dated
        )

    def test_workloads_branch_no_branch(self):
        db.session.commit()

        response = self.app.get('{}/graph-workloads/2'.format(self.__api_path))

        assert (
            response.status_code == 433 and
            json.loads(response.data.decode())['http-status']['message']
            == "Assignment for this branch doesn't exist"
        )
