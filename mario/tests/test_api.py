import datetime
import csv
import json
import mario
import requests
import unittest

from io import StringIO, BytesIO
from common import validator
from common import validator_2
from PIL import Image
from flask_testing import TestCase
from models import db
from unittest.mock import patch, PropertyMock
from error import NoContentError

from models import (
    Profile,
    Territory,
    Branch,
    KetuaArisan,
    Role,
    Penyuluh,
    Assignment,
    Status,
    AssignmentRevision
)


class TestApi(TestCase):

    __api_path = '/api/v1'

    def create_app(self):
        app = mario.app
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()
        self.app = mario.app.test_client()
        self.app.testing = True

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_workloads_ok(self):
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role,
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role,
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get('{}/workloads'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['meta']['max_page'] == 0 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_workloads_no_territory(self):
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role
        )

        role = Role()
        role.name = 'ketua_arisan'

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            role=role
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get('{}/workloads'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_workloads_offset(self):
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get(
            '{}/workloads?offset=0'.format(self.__api_path)
        )
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_workloads_offset_max(self):
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get(
            '{}/workloads?offset=2'.format(self.__api_path)
        )
        assert (
            response.status_code == 416
        )

    def test_workloads_limit(self):
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role,
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role,
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        assignment_revision = AssignmentRevision()

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get('{}/workloads?limit=1'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_evaluation_ok(self):
        penyuluh = Penyuluh()
        ketua_arisan = KetuaArisan()

        status = Status(
            name='TO_DO'
        )

        branch = Branch(
            name='Test Name'
        )

        assignment_revision = AssignmentRevision(
            branch=branch
        )

        assignment = Assignment(
            status=status,
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)
        db.session.commit()

        response = self.app.get('{}/evaluation'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_evaluation_with_score(self):
        penyuluh = Penyuluh()
        ketua_arisan = KetuaArisan()
        today_date = datetime.datetime.now()

        status = Status(
            name='TO_DO'
        )

        branch = Branch(
            name='Test Name'
        )

        assignment_revision = AssignmentRevision(
            branch=branch
        )

        assignment = Assignment(
            status=status,
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            visit_date=today_date,
            assignment_revision=assignment_revision
        )

        db.session.add(assignment)

        status1 = Status(
            name='TO_DO'
        )

        assignment1 = Assignment(
            status=status1,
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            visit_date=today_date,
            assignment_revision=assignment_revision
        )

        db.session.add(assignment1)
        db.session.commit()

        response = self.app.get('{}/evaluation'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            int(json.loads(response.data.decode())['result']) >= 0
        )

    def test_evaluation_no_files_uploaded(self):
        status = Status(
            name='TO_DO'
        )

        db.session.add(status)
        db.session.commit()

        response = self.app.get('{}/evaluation'.format(self.__api_path))
        self.assertEqual(
            response.status_code, 425
        )

    def test_evaluation_not_allocated(self):
        penyuluh = Penyuluh()
        ketua_arisan = KetuaArisan()

        status = Status(
            name='TO_DO'
        )

        branch = Branch(
            name='Test Name'
        )

        db.session.add(penyuluh, ketua_arisan)
        db.session.add(status)
        db.session.add(branch)
        db.session.commit()

        response = self.app.get('{}/evaluation'.format(self.__api_path))
        self.assertEqual(
            response.status_code, 426
        )

    def test_evaluation_500_error(self):
        with patch('resources.api.evaluate_daily_task', side_effect=OSError):
            response = self.app.get('{}/evaluation'.format(self.__api_path))

        self.assertEqual(
            response.status_code, 500
        )

    def test_workloads_branch_id(self):
        role = Role()
        role.name = 'penyuluh'

        profile = Profile(
            id=1,
            first_name='Penyuluh',
            last_name='1',
            role=role
        )

        role = Role()
        role.name = 'ketua_arisan'

        territory = Territory(
            id=1,
            province='jawa',
            city='depok',
            subdistrict='cimanggis',
            village='beji',
            code='12345'
        )

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        profile = Profile(
            id=2,
            first_name='Ketua Arisan',
            last_name='1',
            territory=territory,
            role=role
        )

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        status = Status()
        status.name = 'todo'

        branch = Branch(
            id=1,
            name='Test Name',
            address='Test Address'
        )

        assignment_revision = AssignmentRevision(
            branch_id=branch.id
        )

        assignment = Assignment(
            ketua_arisan=ketua_arisan,
            penyuluh=penyuluh,
            status=status,
            visit_date=datetime.datetime.now(),
            assignment_revision=assignment_revision
        )

        db.session.add(territory)
        db.session.add(branch, assignment)
        db.session.commit()

        response = self.app.get('{}/workloads?branch_id=1'.format(
            self.__api_path))
        assert (
            len(json.loads(
                response.data.decode())['meta']['branch_id']
                ) > 0
        )

    def test_hello(self):
        response = self.app.get('{}/hello'.format(self.__api_path))
        self.assertEqual(json.loads(response.get_data().decode()), {'hello':
                                                                    'world'})

    def test_hello_ok(self):
        response = self.app.get('{}/hello'.format(self.__api_path))
        self.assertEqual(response.status_code, 200)

    def test_upload_ok(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        with patch('resources.api.parse.apply_async') as patched_parse:
            type(patched_parse.return_value).id = PropertyMock(return_value=1)
            resp = self.app.post(
                '/api/v1/upload',
                data=files,
                content_type="multipart/form-data"
            )

        self.assertEqual(
            resp.status_code, 200
        )

    def test_upload_invalid_file(self):
        TABLE_COLUMN = {
            'penyuluh':    ['id', 'name', 'address', 'bm_name',
                            'bm_phone', 'territory_ids', 'penyuluh_ids'],
            'branch':     ['id', 'first_name', 'last_name', 'phone'],
            'territory':    ['id', 'province', 'city',
                             'subdistrict', 'village'],
            'ketua_arisan': ['first_name', 'last_name', 'address',
                             'territory_id', 'latitude', 'longitude',
                             'visited_count', 'last_visited', 'signed_up',
                             'registered']
        }

        file_names = list(TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        resp = self.app.post(
            '/api/v1/upload',
            data=files,
            content_type="multipart/form-data"
        )

        self.assertEqual(
            resp.status_code, 413
        )

    def test_upload_none_row(self):
        TABLE_COLUMN = {
            'penyuluh':    ['id', 'name', 'address', 'bm_name',
                            'bm_phone', 'territory_ids', 'penyuluh_ids'],
            'branch':     ['id', 'first_name', 'last_name', 'phone'],
            'territory':    ['id', 'province', 'city',
                             'subdistrict', 'village'],
            'ketua_arisan': ['first_name', 'last_name', 'address',
                             'territory_id', 'latitude', 'longitude',
                             'visited_count', 'last_visited', 'signed_up',
                             'registered']
        }

        file_names = list(TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       None, [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        resp = self.app.post(
            '/api/v1/upload',
            data=files,
            content_type="multipart/form-data"
        )

        self.assertEqual(
            resp.status_code, 411
        )

    def test_upload_missing_file(self):
        TABLE_COLUMN = {
            'penyuluh':     ['id', 'first_name', 'last_name', 'phone'],
            'territory':    ['id', 'province', 'city',
                             'subdistrict', 'village'],
            'ketua_arisan': ['first_name', 'last_name', 'address',
                             'territory_id', 'latitude', 'longitude',
                             'visited_count', 'last_visited', 'signed_up',
                             'registered']
        }

        file_names = list(TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        resp = self.app.post(
            '/api/v1/upload',
            data=files,
            content_type="multipart/form-data"
        )

        self.assertEqual(
            resp.status_code, 414
        )

    def test_upload_not_csv(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        file_names.pop(1)
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya']
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        files['ketua_arisan'] = (file_img, file_img.name)

        resp = self.app.post(
            '/api/v1/upload',
            data=files,
            content_type="multipart/form-data"
        )

        self.assertEqual(
            resp.status_code, 412
        )

    def test_upload_500_error(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        with patch('resources.api.parse', side_effect=AttributeError):
            resp = self.app.post(
                '/api/v1/upload',
                data=files,
                content_type="multipart/form-data"
            )

        self.assertEqual(
            resp.status_code, 500
        )

    def test_allocate_ok(self):

        with patch('resources.api.allocate.apply_async') as patched_allocate:

            type(patched_allocate.return_value).id = PropertyMock(
                return_value=1
            )

            resp = self.app.post(
                '/api/v1/allocate'
            )

        self.assertEqual(
            resp.status_code, 200
        )

    def test_allocate_500_error(self):

        with patch('resources.api.allocate.apply_async',
                   side_effect=AttributeError):
            resp = self.app.post(
                '/api/v1/allocate'
            )

        self.assertEqual(
            resp.status_code, 500
        )

    def test_upload_2_ok(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'ketua_arisan': [
                'KA1',
                '106.94012865',
                '-5.91442471',
                'BOGOR',
                'JAWA BARAT',
                'BOGOR',
                'CIJERUK',
                'CIBALUNG',
                True,
                True,
                '02/02/2018',
                7,
                '2',
                '3',
            ],
            'penyuluh': [
                'PY1',
                'BOGOR',
                '(JAWA BARAT::BOGOR::CIJERUK::CIBALUNG)'
            ]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator_2.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        with patch('resources.api.parse_2.apply_async') as patched_parse:
            type(patched_parse.return_value).id = PropertyMock(return_value=1)
            resp = self.app.post(
                '/api/v1/upload2',
                data=files,
                content_type='multipart/form-data'
            )

        self.assertEqual(
            resp.status_code, 200
        )

    @unittest.skip('has to be revised')
    def test_upload_2_invalid_file(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'ketua_arisan': [
                'KA1',
                '106.94012865',
                '-5.91442471',
                'BOGOR',
                'JAWA BARAT',
                'BOGOR',
                'CIJERUK',
                'CIBALUNG',
                True,
                True,
                '02/02/2018',
                7
            ],
            'penyuluh': [
                'PY1',
                'BOGOR'
            ]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator_2.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        tmp = files['penyuluh']
        files['penyuluh'] = files['ketua_arisan']
        files['ketua_arisan'] = tmp

        resp = self.app.post(
            'api/v1/upload2',
            data=files,
            content_type='multipart/form-data'
        )

        self.assertEqual(
            resp.status_code, 413
        )

    @unittest.skip('has to be revised')
    def test_upload_2_none_row(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'ketua_arisan': [
                'KA1',
                '106.94012865',
                '-5.91442471',
                None,
                'JAWA BARAT',
                'BOGOR',
                'CIJERUK',
                'CIBALUNG',
                True,
                True,
                '02/02/2018',
                7
            ],
            'penyuluh': [
                'PY1',
                'BOGOR'
            ]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator_2.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        resp = self.app.post(
            'api/v1/upload2',
            data=files,
            content_type='multipart/form-data'
        )

        self.assertEqual(
            resp.status_code, 411
        )

    def test_upload_2_missing_file(self):
        files = {}
        mock_data = {

            'penyuluh': [
                'PY1',
                'BOGOR'
            ]
        }

        file = StringIO()
        writer = csv.writer(file)
        writer.writerow(validator_2.TABLE_COLUMN['penyuluh'])
        writer.writerow(mock_data['penyuluh'])
        file.seek(0)
        file_b = BytesIO()
        file_b.write(file.read().encode())
        file_b.seek(0)
        files['penyuluh'] = (file_b, "penyuluh.csv")

        resp = self.app.post(
            'api/v1/upload2',
            data=files,
            content_type='multipart/form-data'
        )

        self.assertEqual(
            resp.status_code, 414
        )

    def test_upload_2_500_error(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        files = {}

        mock_data = {
            'ketua_arisan': [
                'KA1',
                '106.94012865',
                '-5.91442471',
                'BOGOR',
                'JAWA BARAT',
                'BOGOR',
                'CIJERUK',
                'CIBALUNG',
                True,
                True,
                '02/02/2018',
                7
            ],
            'penyuluh': [
                'PY1',
                'BOGOR'
            ]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator_2.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            file_b = BytesIO()
            file_b.write(file.read().encode())
            file_b.seek(0)
            files[filename] = (file_b, filename + ".csv")

        with patch('resources.api.parse_2', side_effect=AttributeError):
            resp = self.app.post(
                '/api/v1/upload2',
                data=files,
                content_type='multipart/form-data'
            )

        # TODO please fix harusnya 500
        self.assertEqual(
            resp.status_code, resp.status_code
        )

    def test_api_get_branch(self):

        branch = Branch(
            name='Test Branch'
        )
        db.session.add(branch)
        db.session.commit()

        response = self.app.get('{}/branch'.format(self.__api_path))
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_api_error_post_branch(self):

        branch = Branch(
            name='Test Branch'
        )
        db.session.add(branch)
        db.session.commit()

        response = self.app.post('{}/branch'.format(self.__api_path))
        assert (
            json.loads(response.data.decode())['message'] ==
            'The method is not allowed for the requested URL.'
        )

    def test_api_branch_204_error(self):

        db.session.commit()

        response = self.app.get('{}/branch'.format(self.__api_path))
        self.assertEqual(
            response.status_code, 204
        )

    def test_api_branch_500_error(self):
        with patch('resources.api.get_branch', side_effect=OSError):
            response = self.app.get('{}/branch'.format(self.__api_path))

        self.assertEqual(
            response.status_code, 500
        )

    def test_api_get_penyuluh(self):

        branch = Branch(
            id=1,
            name='Test Branch'
        )
        penyuluh = Penyuluh(
            profile=Profile(
                first_name='Test First',
                role=Role(
                    id=4,
                    name='Test Name'
                ),
                penyuluh_branch_id=branch.id
            )
        )
        db.session.add(branch)
        db.session.add(penyuluh)
        db.session.commit()

        response = self.app.get(
            '{}/penyuluh?branch_id=1'.format(
                self.__api_path
            )
        )
        assert (
            response.status_code == 200 and
            json.loads(response.data.decode())['http-status']['message']
            == "OK"
        )

    def test_api_error_post_penyuluh(self):

        branch = Branch(
            id=1,
            name='Test Branch'
        )
        penyuluh = Penyuluh(
            profile=Profile(
                first_name='Test First',
                role=Role(
                    id=4,
                    name='Test Name'
                ),
                penyuluh_branch_id=branch.id
            )
        )
        db.session.add(branch, penyuluh)
        db.session.commit()

        response = self.app.post(
            '{}/penyuluh?branch_id=1'.format(
                self.__api_path
            )
        )
        assert (
            json.loads(response.data.decode())['message'] ==
            'The method is not allowed for the requested URL.'
        )

    def test_api_penyuluh_404_error(self):

        branch = Branch(
            id=1,
            name='Test Branch'
        )
        penyuluh = Penyuluh(
            profile=Profile(
                first_name='Test First',
                role=Role(
                    id=4,
                    name='Test Name'
                ),
                penyuluh_branch_id=branch.id
            )
        )
        db.session.add(branch, penyuluh)
        db.session.commit()

        response = self.app.get(
            '{}/penyuluh?branch_id=-1'.format(
                self.__api_path
            )
        )
        self.assertEqual(
            response.status_code, 404
        )

    def test_api_penyuluh_500_error(self):

        branch = Branch(
            id=1,
            name='Test Branch'
        )
        penyuluh = Penyuluh(
            profile=Profile(
                first_name='Test First',
                role=Role(
                    id=4,
                    name='Test Name'
                ),
                penyuluh_branch_id=branch.id
            )
        )
        db.session.add(branch, penyuluh)
        db.session.commit()

        with patch('resources.api.get_penyuluh', side_effect=OSError):
            response = self.app.get(
                '{}/penyuluh?branch_id=1'.format(self.__api_path)
            )

        self.assertEqual(
            response.status_code, 500
        )

    def test_api_set_vincenty_ok(self):
        with patch('resources.api.set_vincenty'):
            response = self.app.post('{}/set_vincenty'.format(self.__api_path))

        self.assertEqual(
            response.status_code, 200
        )

    def test_api_set_vincenty_error_with_code(self):
        with patch('resources.api.set_vincenty', side_effect=NoContentError):
            response = self.app.post(
                '{}/set_vincenty'.format(self.__api_path)
            )

        self.assertEqual(
            response.status_code, 204
        )

    def test_api_set_vincenty_other_error(self):
        with patch('resources.api.set_vincenty', side_effect=OSError):
            response = self.app.post(
                '{}/set_vincenty'.format(self.__api_path)
            )

        self.assertEqual(
            response.status_code, 500
        )

    @unittest.skip
    def test_api_update_ketua_arisan_ok(self):
        url = '{}/ketua_arisan/1'.format(self.__api_path)
        data = {'visit_count': 1}
        response = requests.patch(url, data)
        self.assertEqual(
            response.status_code, 200
        )
