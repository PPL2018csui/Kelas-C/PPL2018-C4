import unittest
import mario
import csv

from io import StringIO, BytesIO
from common import validator
from PIL import Image
from copy import deepcopy

from error import (
    NoneValueError,
    InvalidFileFormatError,
    MissingColumnError
)


class TestValidator(unittest.TestCase):
    mock_data = {
        'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                   '08131651104', '[1]', '[1]'],
        'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
        'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                      'Duren Sawit', 'Malaka Jaya'],
        'ketua_arisan': [1, 'Bthari Smart', 1, 'Jl. Bunga Rampai',
                         '-6.198495', '106.837306', 3, '08/03/2018',
                         False, False]
    }

    def setUp(self):
        self.app = mario.app.test_client()
        self.app.testing = True

    def test_validator_ok(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = validator.TABLE_COLUMN
        mock_data = self.mock_data
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)
        self.assertTrue(validator.validate(files[0], files[1],
                                           files[2], files[3]))

    def test_validator_no_important_column_on_branch(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        table_column['branch'].pop(1)
        mock_data['branch'].pop(1)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_no_important_column_on_penyuluh(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data)
        files = []

        table_column['penyuluh'].pop(1)
        mock_data['penyuluh'].pop(1)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_no_important_column_on_territory(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data)
        files = []

        table_column['territory'].pop(0)
        mock_data['territory'].pop(0)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_no_important_column_on_ketua_arisan(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data)
        files = []

        table_column['ketua_arisan'].pop(1)
        mock_data['ketua_arisan'].pop(1)

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(MissingColumnError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_none_value_on_branch(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['branch'][1] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_branch_none_value_on_penyuluh(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['penyuluh'][1] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_branch_none_value_on_territory(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['territory'][1] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_branch_none_value_on_ketua_arisan(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        mock_data['ketua_arisan'][2] = None

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        with self.assertRaises(NoneValueError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_branch_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[0] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_ketua_arisan_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[1] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_penyuluh_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[2] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator.validate(files[0], files[1], files[2], files[3])

    def test_validator_territory_not_csv_file(self):
        file_img = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(file_img, 'png')
        file_img.name = 'test.png'
        file_img.seek(0)

        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        table_column = deepcopy(validator.TABLE_COLUMN)
        mock_data = deepcopy(self.mock_data.copy())
        files = []

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(table_column[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        files[3] = file_img

        with self.assertRaises(InvalidFileFormatError):
            validator.validate(files[0], files[1], files[2], files[3])
