import csv
import mario
import unittest

from models import db
from io import StringIO
from flask_testing import TestCase
from common import validator_2
from common import parser_2
from models import (
    Profile,
    Territory,
    Branch,
    KetuaArisan,
    Role,
    Penyuluh
)


class TestParser2(TestCase):

    def create_app(self):
        app = mario.app
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()
        self.app = mario.app.test_client()
        self.app.testing = True

        roles = {
            1: 'SUPER_USER',
            2: 'ADMIN',
            3: 'BRANCH_MANAGER',
            4: 'PENYULUH',
            5: 'KETUA_ARISAN'
        }

        for key, val in roles.items():
            db.session.add(
                Role(
                    id=key,
                    name=val
                )
            )

        bm = Profile(
            first_name="BM BOGOR",
            phone_number="81234567890",
            role_id=3,
        )
        db.session.add(
            Branch(
                name="BOGOR",
                branch_manager=bm,
            )
        )

        db.session.add(
            Territory(
                province="JAWA BARAT",
                city="BOGOR",
                subdistrict="NANGGUNG",
                village="BANTAR KARET",
            )
        )

        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    @unittest.skip('has to be revised')
    def test_parse_ok(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        files = []

        mock_data = {
            'ketua_arisan': [
                'KA1',
                '106.94012865',
                '-5.91442471',
                'BOGOR',
                'JAWA BARAT',
                'BOGOR',
                'CIJERUK',
                'CIBALUNG',
                True,
                True,
                '02/02/2018',
                7
            ],
            'penyuluh': [
                'PY1',
                'BOGOR'
            ]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator_2.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            file.seek(0)
            files.append(file)

        parser_2.parse_2(
            ka_file=files[0],
            py_file=files[1]
        )

        ka = Profile.query.join(
            KetuaArisan,
            Profile.ketua_arisan_id == KetuaArisan.id
        ).filter(
            Profile.first_name == "KA1",
            KetuaArisan.longitude == "106.94012865",
            KetuaArisan.latitude == "-5.91442471"
        )

        py = Profile.query.join(
            Penyuluh,
            Profile.penyuluh_id == Penyuluh.id
        )

        self.assertIsNotNone(ka)
        self.assertIsNotNone(py)

    @unittest.skip('has to be revised')
    def test_parse_no_territory(self):
        file_names = list(validator_2.TABLE_COLUMN.keys())
        file_names.sort()
        files = []

        mock_data = {
            'ketua_arisan': [
                'KA1',
                '106.94012865',
                '-5.91442471',
                'BOGOR',
                'JAWA BARAT',
                'BOGOR',
                'CIJERUK',
                'CIBALUNG',
                True,
                True,
                '02/02/2018',
                7
            ],
            'penyuluh': [
                'PY1',
                'BOGOR'
            ]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator_2.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])
            if filename == 'ketua_arisan':
                writer.writerow(
                    ['KA2', '106.293002', '-5.232134', 'BOGOR', 'BB',
                     'BOGOR', 'DD', 'EE', True, False, '01/28/2018', 4]
                )
            file.seek(0)
            files.append(file)

        parser_2.parse_2(
            ka_file=files[0],
            py_file=files[1]
        )

        ka = Profile.query.join(
            KetuaArisan,
            Profile.ketua_arisan_id == KetuaArisan.id
        ).filter(
            Profile.first_name == "KA1",
            KetuaArisan.longitude == "106.94012865",
            KetuaArisan.latitude == "-5.91442471"
        )

        py = Profile.query.join(
            Penyuluh,
            Profile.penyuluh_id == Penyuluh.id
        )

        self.assertIsNotNone(ka)
        self.assertIsNotNone(py)
