import unittest
import mario


class TestLogout(unittest.TestCase):

    def setUp(self):
        self.app = mario.app.test_client()
        self.app.testing = True

    def test_logout(self):
        response = self.app.get('/logout')
        self.assertEqual(response.status_code, 302)
