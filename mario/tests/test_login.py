import unittest
import mario


class TestLogin(unittest.TestCase):

    def setUp(self):
        self.app = mario.app.test_client()
        self.app.testing = True

    def test_login_success(self):
        response = self.app.get('/login')
        self.assertEqual(response.status_code, 302)
