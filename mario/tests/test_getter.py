from flask import Flask
from models import db
from flask_testing import TestCase
import mario

from common.getter import (
    get_branch,
    get_penyuluh
)

from models import (
    Profile,
    Branch,
    Role,
    Penyuluh
)


class TestValidator(TestCase):
    __api_path = '/api/v1'

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()
        self.app = mario.app.test_client()
        self.app.testing = True

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get_branch(self):
        branch = Branch(
            name='Test Branch'
        )
        db.session.add(branch)
        db.session.commit()

        res = get_branch()
        self.assertEqual(
            len(res), 1
        )

    def test_get_penyuluh(self):
        branch = Branch(
            id=1,
            name='Test Branch'
        )
        penyuluh = Penyuluh(
            profile=Profile(
                first_name='Test First',
                role=Role(
                    id=4,
                    name='Test Name'
                ),
                penyuluh_branch_id=branch.id
            )
        )
        db.session.add(branch)
        db.session.add(penyuluh)
        db.session.commit()

        res = get_penyuluh(1)
        self.assertEqual(
            len(res), 1
        )
