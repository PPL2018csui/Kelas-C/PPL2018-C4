from flask import Flask
from models import db
from flask_testing import TestCase
from sqlalchemy import exc

from models import (
    Profile,
    Territory,
    Branch,
    KetuaArisan,
    Role,
    Penyuluh,
    Assignment,
    Status,
    AssignmentRevision,
    RukunTetangga,
    Jarak
)


class TestDatabase(TestCase):

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_profile_ok(self):
        role = Role()
        role.name = 'Super Admin'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role

        db.session.add(profile)
        db.session.commit()

        self.assertIn(profile, db.session)

    def test_profile_no_name(self):
        role = Role()
        role.name = 'Super Admin'

        profile = Profile()
        profile.role = role
        db.session.add(profile)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_profile_no_role(self):
        profile = Profile()
        profile.first_name = 'Wicaksono'

        db.session.add(profile)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_branch_ok(self):

        branch = Branch()
        branch.name = 'Branch_A'
        db.session.add(branch)
        db.session.commit()

        self.assertIn(branch, db.session)

    def test_branch_no_name(self):
        branch = Branch()
        db.session.add(branch)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_territory_ok(self):

        territory = Territory()
        territory.code = 1
        territory.province = 'DKI Jakarta'
        territory.city = 'Jakarta Timur'
        territory.subdistrict = 'Duren Sawit'
        territory.village = 'Malaka Jaya'
        db.session.add(territory)
        db.session.commit()

        self.assertIn(territory, db.session)

    def test_territory_no_province(self):
        territory = Territory()
        territory.city = 'Jakarta Timur'
        territory.subdistrict = 'Duren Sawit'
        territory.village = 'Malaka Jaya'
        db.session.add(territory)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_territory_no_city(self):
        territory = Territory()
        territory.province = 'DKI Jakarta'
        territory.subdistrict = 'Duren Sawit'
        territory.village = 'Malaka Jaya'
        db.session.add(territory)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_territory_no_subdistrict(self):
        territory = Territory()
        territory.province = 'DKI Jakarta'
        territory.city = 'Jakarta Timur'
        territory.village = 'Malaka Jaya'
        db.session.add(territory)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_territory_no_village(self):
        territory = Territory()
        territory.province = 'DKI Jakarta'
        territory.city = 'Jakarta Timur'
        territory.subdistrict = 'Duren Sawit'
        db.session.add(territory)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_role_ok(self):
        role = Role()
        role.name = 'Super Admin'
        db.session.add(role)
        db.session.commit()

        self.assertIn(role, db.session)

    def test_role_no_name(self):
        role = Role()
        db.session.add(role)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_ketua_arisan_ok(self):
        ketua_arisan = KetuaArisan()
        ketua_arisan.visit_count = 5
        db.session.add(ketua_arisan)
        db.session.commit()

        self.assertIn(ketua_arisan, db.session)

    def test_penyuluh_ok(self):

        role = Role()
        role.name = 'Penyuluh'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        db.session.add(penyuluh)
        db.session.commit()

        self.assertIn(penyuluh, db.session)

    def test_assignment_ok(self):

        role = Role()
        role.name = 'Penyuluh'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role

        penyuluh = Penyuluh()
        penyuluh.profile = profile

        role = Role()
        role.name = 'Ketua Arisan'

        profile = Profile()
        profile.first_name = 'Wicaksono'
        profile.role = role

        ketua_arisan = KetuaArisan()
        ketua_arisan.profile = profile

        assignment_revision = AssignmentRevision()

        assignment = Assignment()
        assignment.ketua_arisan = ketua_arisan
        assignment.penyuluh = penyuluh
        assignment.assignment_revision = assignment_revision

        db.session.add(assignment)
        db.session.commit()

        self.assertIn(assignment, db.session)

    def test_status_ok(self):
        status = Status()
        status.name = 'cancelled'

        db.session.add(status)
        db.session.commit()

        self.assertIn(status, db.session)

    def test_assignment_revision_ok(self):
        branch = Branch()
        branch.name = 'Branch_A'

        assignment_revision = AssignmentRevision()
        assignment_revision.branch = branch

        db.session.add(assignment_revision)
        db.session.commit()

    def test_rukun_tetangga_ok(self):
        rukun_tetangga = RukunTetangga()
        rukun_tetangga.rw = '01'
        rukun_tetangga.rt = '01'

        db.session.add(rukun_tetangga)
        db.session.commit()

    def test_rukun_tetangga_no_rt(self):
        rukun_tetangga = RukunTetangga()
        rukun_tetangga.rw = '01'

        db.session.add(rukun_tetangga)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_rukun_tetangga_no_rw(self):
        rukun_tetangga = RukunTetangga()
        rukun_tetangga.rt = '01'

        db.session.add(rukun_tetangga)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_jarak_ok(self):
        rukun_tetangga = RukunTetangga(
            id=1,
            rw='01',
            rt='01'
        )

        db.session.add(rukun_tetangga)

        jarak = Jarak()
        jarak.rt_awal_id = 1
        jarak.rt_tujuan_id = 1

        db.session.add(jarak)
        db.session.commit()

    def test_jarak_no_tujuan(self):
        rukun_tetangga = RukunTetangga()
        rukun_tetangga.rw = '01'
        rukun_tetangga.rt = '01'

        jarak = Jarak()
        jarak.rt_tujuan = rukun_tetangga

        db.session.add(jarak)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()

    def test_jarak_no_awal(self):
        rukun_tetangga = RukunTetangga()
        rukun_tetangga.rw = '01'
        rukun_tetangga.rt = '01'

        jarak = Jarak()
        jarak.rt_awal = rukun_tetangga

        db.session.add(jarak)

        with self.assertRaises(exc.IntegrityError):
            db.session.commit()
