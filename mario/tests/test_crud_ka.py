from flask import Flask
from models import db
from flask_testing import TestCase
import mario
import datetime
'''
from common.crud_ka import (
    update_ka
)
'''
from models import (
    Profile,
    KetuaArisan,
    Role
)


class TestCrudKa(TestCase):
    __api_path = '/api/v1'

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()
        self.app = mario.app.test_client()
        self.app.testing = True

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def create_mock_data(self):
        r = Role(
            id=5,
            name='KETUA_ARISAN'
        )
        ka = KetuaArisan(
            id=1,
            visit_count=1,
            is_registered=False,
            is_signed=False,
            last_visited=datetime.datetime.today()
        )
        p = Profile(
            first_name='Test Name',
            role_id=r.id,
            ketua_arisan_id=ka.id
        )
        db.session.add(r)
        db.session.add(ka)
        db.session.add(p)
        db.session.commit()

    '''
    def test_update_visit_count(self):
        self.create_mock_data()
        res = update_ka(1, 'visit_count')
        self.assertEqual(
            res, 2
        )

    def test_update_is_registered(self):
        self.create_mock_data()
        res = update_ka(1, 'is_registered')
        self.assertEqual(
            res, True
        )

    def test_update_is_signed(self):
        self.create_mock_data()
        res = update_ka(1, 'is_signed')
        self.assertEqual(
            res, True
        )

    def test_update_invalid_attribute(self):
        self.create_mock_data()
        try:
            res = update_ka(1, 'invalid')
        except Exception as e:
            res = e
        assert(
            res.code == 427
        )
    '''
