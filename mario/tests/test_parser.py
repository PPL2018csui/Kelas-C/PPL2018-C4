import csv
import common.parser as p

from flask import Flask
from models import db
from io import StringIO
from common import validator
from flask_testing import TestCase
from unittest.mock import patch
from error import DuplicateTerritoryError
from models import (
    Profile,
    Territory,
    Branch,
    KetuaArisan,
    Role,
    Penyuluh
)


class TestParser(TestCase):

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config.TestingConfig')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

        roles = {
            1: 'SUPER_USER',
            2: 'ADMIN',
            3: 'BRANCH_MANAGER',
            4: 'PENYULUH',
            5: 'KETUA_ARISAN'
        }

        for key, val in roles.items():
            db.session.add(
                Role(
                    id=key,
                    name=val
                )
            )

        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_parse(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        files = []

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])

            if filename == 'ketua_arisan':
                writer.writerow(
                    [
                        1, 'Zahra', 'Ule', 'Jl. Bunga Rampai', 2,
                        "-6.198495", "106.837306", 3, "08/03/2018",
                        False, False
                    ]
                )

            if filename == 'territory':
                writer.writerow(
                    [
                        2, 'Jawa Barat', 'Depok',
                        'Cimanggis', 'Harjamukti'
                    ]
                )

            file.seek(0)
            files.append(file)

        with patch('common.parser.parse.update_state'):
            p.parse(
                branch_file=files[0].read().encode(),
                ka_file=files[1].read().encode(),
                py_file=files[2].read().encode(),
                territory_file=files[3].read().encode()
            )

        br = Branch.query.filter_by(
            name='Tebet',
            address='Jl. Tebet Raya'
        ).first()

        ter = Territory.query.filter_by(
            code=1,
            province='DKI Jakarta',
            city='Jakarta Timur',
            subdistrict='Duren Sawit',
            village='Malaka Jaya'
        ).first()

        py = Profile.query.join(
            Penyuluh,
            Profile.penyuluh_id == Penyuluh.id
        ).filter(
            Profile.first_name == 'Wicaksono',
            Profile.last_name == 'Wisnu',
            Profile.phone_number == '08111710107',
            Profile.role_id == 4
        )

        ka = Profile.query.join(
            KetuaArisan,
            Profile.ketua_arisan_id == KetuaArisan.id
        ).filter(
            Profile.first_name == 'Bthari',
            Profile.last_name == 'Smart',
            Profile.address == 'Jl. Bunga Rampai',
            KetuaArisan.latitude == '-6.198495',
            KetuaArisan.longitude == '106.837306'
        )

        self.assertIsNotNone(br)
        self.assertIsNotNone(ter)
        self.assertIsNotNone(py)
        self.assertIsNotNone(ka)

    def test_parse_duplicate_territory(self):
        file_names = list(validator.TABLE_COLUMN.keys())
        file_names.sort()
        files = []

        mock_data = {
            'branch': [1, 'Tebet', 'Jl. Tebet Raya', 'Rafiano Ruby',
                       '08131651104', [1], [1]],
            'penyuluh': [1, 'Wicaksono', 'Wisnu', '08111710107'],
            'territory': [1, 'DKI Jakarta', 'Jakarta Timur',
                          'Duren Sawit', 'Malaka Jaya'],
            'ketua_arisan': [1, 'Bthari', 'Smart', 'Jl. Bunga Rampai', 1,
                             "-6.198495", "106.837306", 3, "08/03/2018",
                             False, False]
        }

        for filename in file_names:
            file = StringIO()
            writer = csv.writer(file)
            writer.writerow(validator.TABLE_COLUMN[filename])
            writer.writerow(mock_data[filename])

            if filename == 'branch':
                writer.writerow(
                    [
                        1, 'Depok', 'Jl. Depok', 'Rubi Rafi',
                        '08131655633', [1], [1]
                    ]
                )

            file.seek(0)
            files.append(file)

        with self.assertRaises(DuplicateTerritoryError):
            with patch('common.parser.parse.update_state'):
                p.parse(
                    branch_file=files[0].read().encode(),
                    ka_file=files[1].read().encode(),
                    py_file=files[2].read().encode(),
                    territory_file=files[3].read().encode()
                )
