# *allocateam* - PPL C4

## TECHNOLOGY
 - Flask
 - React JS

## URLs

**PRODUCTION**
 - allocateam.com
 - mario.allocateam.com

**SIT/UAT**
 - sit-uat.allocateam.com
 - sit-uat.mario.allocateam.com

**COBA-COBA**
 - coba.allocateam.com
 - coba.mario.allocateam.com

## Developers
- Ahmad Yazid Harharah
- Bthari Smartnastiti
- Glory Finesse Valery
- Rafiano R. Rubiantoro
- Wisnu Teguh Wicaksono
- Zahra Auliaul Ummah
